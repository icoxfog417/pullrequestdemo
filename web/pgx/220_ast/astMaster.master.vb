﻿Public Enum AstUserKind
    PRM01
    MDP01
    MCI_ELA
    MCI_SWP
End Enum

Partial Class _220_ast_astMaster
    Inherits System.Web.UI.MasterPage

    Private _displayMenu As Boolean = True
    Public Property DisplayMenu() As Boolean
        Get
            Return _displayMenu
        End Get
        Set(ByVal value As Boolean)
            _displayMenu = value
        End Set
    End Property

    Private _loginUser As String
    Public Property LoginUser() As String
        Get
            Return _loginUser
        End Get
        Set(ByVal value As String)
            _loginUser = value
        End Set
    End Property

    Private _connectionName As String = ""
    Public Property ConnectionName() As String
        Get
            Return _connectionName
        End Get
        Set(ByVal value As String)
            _connectionName = value
        End Set
    End Property

    Private _dsNameSpace As String = "AstMaster"
    Public Property DsNameSpace() As String
        Get
            Return _dsNameSpace
        End Get
        Set(ByVal value As String)
            _dsNameSpace = value
        End Set
    End Property

    Private _colDefineTable As String = "AST_V_COLUMN"
    Public ReadOnly Property ColDefineTable() As String
        Get
            Return _colDefineTable
        End Get
    End Property


    Private _userRoleKind As New List(Of Boolean)
    Protected ReadOnly Property UserRoleKind As List(Of Boolean)
        Get
            Return _userRoleKind
        End Get
    End Property

    Public ReadOnly Property isPRM01 As Boolean
        Get
            Return _userRoleKind(AstUserKind.PRM01)
        End Get
    End Property
    Public ReadOnly Property isMDP01 As Boolean
        Get
            Return _userRoleKind(AstUserKind.MDP01)
        End Get
    End Property
    Public ReadOnly Property isMCI_ELA As Boolean
        Get
            Return _userRoleKind(AstUserKind.MCI_ELA)
        End Get
    End Property
    Public ReadOnly Property isMCI_SWP As Boolean
        Get
            Return _userRoleKind(AstUserKind.MCI_SWP)
        End Get
    End Property


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'ブラウザにキャッシュさせない
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        _loginUser = Membership.GetUser.UserName


        '条件分岐　※何度も行うことが想定されるため、モジュール化の可能性大
        _userRoleKind.Clear()
        Dim userKindCount As Integer = [Enum].GetNames(GetType(AstUserKind)).Count
        For i As Integer = 0 To userKindCount - 1
            _userRoleKind.Add(False)
        Next

        If Roles.IsUserInRole("AST_PRM01") Then
            ConnectionName = "ASTConnectPRM01"
            _userRoleKind(AstUserKind.PRM01) = True
        ElseIf Roles.IsUserInRole("AST_MDP01") Then
            ConnectionName = "ASTConnectMDP01"
            _userRoleKind(AstUserKind.MDP01) = True
        ElseIf Roles.IsUserInRole("AST_MCI01") Then
            ConnectionName = "ASTConnectMCI01"
            _userRoleKind(AstUserKind.MCI_ELA) = True
        ElseIf Roles.IsUserInRole("AST_MCI02") Then
            ConnectionName = "ASTConnectMCI02"
            _userRoleKind(AstUserKind.MCI_SWP) = True
        Else
            ConnectionName = "ASTConnectPRM01" 'テスト用
            _userRoleKind(AstUserKind.PRM01) = True
        End If

    End Sub

    Protected Function outUserKindStyle() As String
        Dim style As String = ""
        Dim displayOn As String = ""
        Dim displayOff As String = ""
        For i As Integer = 0 To _userRoleKind.Count - 1
            Dim cssClass As String = ""
            Select Case i
                Case AstUserKind.PRM01
                    cssClass = ".ast-prm01-only"
                Case AstUserKind.MDP01
                    cssClass = ".ast-mdp01-only"
                Case AstUserKind.MCI_ELA
                    cssClass = ".ast-mci-ela-only"
                Case AstUserKind.MCI_SWP
                    cssClass = ".ast-mci-swp-only"
            End Select

            If _userRoleKind(i) Then
                displayOn += cssClass + "{ display:inline; }"
                displayOn += cssClass + ".table-cell{ display:table-cell; }"
            Else
                displayOff += cssClass + "{ display:none; }"
            End If
        Next

        style = "<style>" + displayOff + displayOn + "</style>" 'CSSは前から適用されるので、表示するスタイルを後に持ってくる
        Return style

    End Function

End Class

