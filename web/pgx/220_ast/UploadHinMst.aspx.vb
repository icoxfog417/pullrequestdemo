﻿Imports Gears
Imports AstMaster
Imports System.Data
Imports System.Web.UI
Imports System.IO
Imports ExcelNameManager

''' -------------------------------------------------------------------------------
''' <summary>
''' スケジューラ品目マスタ 一括アップロード・ダウンロード
''' </summary>
''' <hisotry>
''' 2013/11/18  [TIS 坂本]    Created
''' </hisotry>
''' -------------------------------------------------------------------------------
Partial Class _220_ast_UploadHinMst
    Inherits GearsPage

    Private Const DTO_ERR_FIELD_ATTR As String = "dtoErr"

#Region "イベント"
#Region "ページ"
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Init
    ''' </summary>
    ''' <param name="e"></param>
    ''' <param name="sender"></param>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        registerMyControl(pnlGFilter)

        'イベントハンドラの登録
        AddHandler DHYO_MDL_KBN.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlMDL_KBN_SelectedIndexChanged

        '非同期更新処理の登録
        Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)
        srmOnPage.RegisterAsyncPostBackControl(DHYO_MDL_KBN.getControl(Of DropDownList))

    End Sub

    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Load
    ''' </summary>
    ''' <param name="e"></param>
    ''' <param name="sender"></param>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        setEscapesWhenSend(pnlGFilter, Nothing, SORT_KIND.ControlId)
        addRelation(DHYO_MDL_KBN.getControl(Of DropDownList), DHYO_SOUCHI_CD.getControl(Of DropDownList))

        'メッセージ消す用
        btnDownload.Attributes("onclick") = "return clearMsg();"
        btnUpload.Attributes("onclick") = "return clearMsg();"

        lblMsgDL.Text = ""
        lblMsgUL.Text = ""
        pnlResult.Visible = False
    End Sub
#End Region
#Region "ドロップダウン"
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' モデル区分の選択変更
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' ------------------------------------------------------------------------------
    Protected Sub ddlMDL_KBN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        executeBehavior(DHYO_MDL_KBN.getControl(Of DropDownList))
        udpSOUCHI_CD.Update()
    End Sub
#End Region
#Region "ボタン"
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' ダウンロードボタンクリック
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' ------------------------------------------------------------------------------
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        Try
            Using templateXls = New XlsTemplateSchedulerItem(GPageMediator.ConnectionName)
                Dim dtCount As Integer = templateXls.expandSchedulerItem(makeSelectingDto)
                If dtCount = 0 Then
                    SetMessage(lblMsgDL, "データがありません")
                Else
                    templateXls.Write("ＳＣ品目一覧.xlsx")
                End If
            End Using
        Catch ex As Exception
            SetMessage(lblMsgDL, ex.Message)
        End Try

    End Sub

    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' アップロードボタンクリック
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' ------------------------------------------------------------------------------
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        Dim dtXls As DataTable = Nothing    'EXCELからのデータ取得用テーブル

        '■ファイル形式チェック
        Dim postfile As HttpPostedFile = fupExcel.PostedFile
        Dim upldfileNm As String = fupExcel.PostedFile.FileName

        If String.IsNullOrEmpty(upldfileNm) Then
            SetMessage(lblMsgUL, "ファイルが指定されていません。")
            Exit Sub
        ElseIf UCase(postfile.ContentType) <> "APPLICATION/VND.OPENXMLFORMATS-OFFICEDOCUMENT.SPREADSHEETML.SHEET" And
                Path.GetExtension(upldfileNm).ToLower <> ".xlsx" Then
            SetMessage(lblMsgUL, "EXCELファイルではありません。")
            Exit Sub
        End If

        '■EXCELからDataTableにデータ取得し、1レコードずつ更新する
        Dim templateXls As XlsTemplateSchedulerItem = Nothing
        pnlResult.Visible = True
        Try
            '雛形EXCEL参照用のオブジェクト（取得データ列情報・入力規則設定情報取得などに使用）
            templateXls = New XlsTemplateSchedulerItem(GPageMediator.ConnectionName)

            'EXCELからデータ取得
            dtXls = getDatafromUploadfile(postfile.InputStream, templateXls)
            'DataTableがNothingのときは処理中止。エラー処理はgetDatafromUploadfileで実施済み
            If dtXls Is Nothing Then Exit Sub

            '更新に必要なものの準備
            Dim dsHin As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)

            '更新処理
            For Each xlsRow As DataRow In dtXls.Rows
                Dim errmsgs As String = ""
                Dim dtoHin As GearsDTO = Nothing
                Try
                    dtoHin = makeSaveDto(xlsRow, templateXls)
                    'DTO作成時エラーがない分だけバリデーション・更新対象
                    If String.IsNullOrEmpty(dtoHin.getAttrInfo(DTO_ERR_FIELD_ATTR)) Then
                        If rowDataValidation(dtoHin, templateXls) Then
                            dsHin.gSave(dtoHin)
                        End If
                    End If
                Catch ex As Exception
                    errmsgs = ex.Message
                End Try
                If (Not dtoHin Is Nothing AndAlso Not String.IsNullOrEmpty(dtoHin.getAttrInfo(DTO_ERR_FIELD_ATTR))) Or Not String.IsNullOrEmpty(errmsgs) Then
                    xlsRow.RowError = xlsRow(EPPlusNameManager.XLS_ROWNUM) & "行目:"
                    xlsRow.RowError += errmsgs
                    If Not dtoHin Is Nothing AndAlso Not String.IsNullOrEmpty(dtoHin.getAttrInfo(DTO_ERR_FIELD_ATTR)) Then
                        xlsRow.RowError += dtoHin.getAttrInfo(DTO_ERR_FIELD_ATTR)
                    End If
                End If
            Next
            '■結果をメッセージエリアに出力
            lblFile.Text = String.Format(hdnUlFile.Value, upldfileNm)
            lblCount.Text = String.Format(hdnUlCount.Value, dtXls.Rows.Count, dtXls.Rows.Count - dtXls.GetErrors().Count(), dtXls.GetErrors().Count())
            lblMsgArea.Text = ""
            If dtXls.HasErrors Then
                For Each r In dtXls.GetErrors()
                    lblMsgArea.Text += r.RowError + "<br />"
                Next
            Else
                lblMsgArea.Text += "エラーなし"
            End If
        Catch ex As Exception
            lblMsgArea.Text = ex.Message
        Finally
            If templateXls IsNot Nothing Then
                templateXls.Dispose()
            End If
        End Try
    End Sub
#End Region
#End Region
#Region "その他"
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' アップロードファイルからデータ取得
    ''' 取込対象スキーマ名が付いている名前定義(雛形EXCELから取得)の列が対象
    ''' </summary>
    ''' <param name="upfile">アップロードファイル</param>
    ''' <param name="templateXls">雛形EXCEL操作オブジェクト</param>
    ''' <returns>読込結果のDataTable（項目名からはスキーマ名を除外して返される）</returns>
    ''' ------------------------------------------------------------------------------
    Private Function getDatafromUploadfile(ByVal upfile As System.IO.Stream, ByRef templateXls As XlsTemplateSchedulerItem) As DataTable
        Dim dt As DataTable = Nothing
        Try
            Using enm As EPPlusNameManager = New EPPlusNameManager(upfile)
                If enm.Workbook.Worksheets(XlsTemplateSchedulerItem.SHEET_DATA) Is Nothing Then
                    SetMessage(lblMsgUL, "取込対象シート(" + XlsTemplateSchedulerItem.SHEET_DATA + ")が存在しないためアップロードできません。")
                Else
                    '「H.」が付いている名前定義の列データ取得
                    Try
                        dt = enm.ToDataTable(XlsTemplateSchedulerItem.SHEET_DATA, templateXls.DataStartRow, templateXls.HederNames.Keys.ToList(), True)
                    Catch ex As Exception
                        SetMessage(lblMsgUL, "以下の名前定義が削除されているためアップロードできません。<br/ >&nbsp;&nbsp;&nbsp;" + ex.Message)
                    End Try
                End If
            End Using
        Catch ex As Exception
            SetMessage(lblMsgUL, "アップロードできません。<br/ >&nbsp;&nbsp;&nbsp;" + ex.Message)
        End Try

        Return dt

    End Function
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' 検索用DTO作成
    ''' </summary>
    ''' <returns>検索用DTO</returns>
    ''' ------------------------------------------------------------------------------
    Private Function makeSelectingDto() As GearsDTO
        Dim ds As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
        Dim sendObj As GearsDTO = makeSendMessage(pnlGFilter)
        ds.setOrder(SORT_KIND.getValue, sendObj)
        Return sendObj
    End Function
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' 更新用DTO作成(必須項目チェックと入力規則名前とコードの変換も合わせて行う）
    ''' </summary>
    ''' <param name="row">EXCELから読込んだ1レコード</param>
    ''' <param name="templateXls">雛形EXCEL操作オブジェクト</param>
    ''' <returns>更新用DTO</returns>
    ''' ------------------------------------------------------------------------------
    Private Function makeSaveDto(ByVal row As DataRow, ByVal templateXls As XlsTemplateSchedulerItem) As GearsDTO

        Dim dto As GearsDTO = New GearsDTO(ActionType.SAVE)
        Dim errmsg As String = String.Empty
        Dim hin_kbn As String = String.Empty
        Dim exhin_kbn As String = String.Empty
        Dim vkorg As String = String.Empty
        Dim mdl As String = String.Empty
        Dim masterList As Dictionary(Of String, Dictionary(Of String, String)) = HinValidators.MasterList(templateXls)
        '品目区分と例外品目区分と販売組織とモデル区分をコードに変換する
        If masterList("HIN_KBN_TXT").ContainsValue(row("HIN_KBN_TXT").ToString) Then
            hin_kbn = masterList("HIN_KBN_TXT").FirstOrDefault(Function(x) x.Value = row("HIN_KBN_TXT").ToString).Key
        End If
        If masterList("EXHIN_KBN_TXT").ContainsValue(row("EXHIN_KBN_TXT").ToString) Then
            exhin_kbn = masterList("EXHIN_KBN_TXT").FirstOrDefault(Function(x) x.Value = row("EXHIN_KBN_TXT").ToString).Key
        End If
        If masterList("VKORG_TXT").ContainsValue(row("VKORG_TXT").ToString) Then
            vkorg = masterList("VKORG_TXT").FirstOrDefault(Function(x) x.Value = row("VKORG_TXT").ToString).Key
        End If
        If masterList("DHYO_MDL_KBN").ContainsValue(row("DHYO_MDL_KBN").ToString) Then
            mdl = masterList("DHYO_MDL_KBN").FirstOrDefault(Function(x) x.Value = row("DHYO_MDL_KBN").ToString).Key
        End If

        '必須項目のチェック（自消と無効フラグはバリデーションで行う）
        If String.IsNullOrWhiteSpace(row("HIN_CD").ToString) Then
            errmsg += "品目コードが設定されていません;"
        End If
        If AstMaster.HIN_KBN.GetHinKbn(hin_kbn) = HIN_KBN_LIST.NONE Then
            errmsg += "品目区分が設定されていません;"
        End If
        If AstMaster.EXHIN_KBN.GetExHinKbn(exhin_kbn) = EXHIN_KBN_LIST.NONE Then
            errmsg += "例外品目区分が設定されていません;"
        End If
        If String.IsNullOrEmpty(vkorg) And AstMaster.HIN_KBN.GetHinKbn(hin_kbn) <> HIN_KBN_LIST.NISUGATA Then
            errmsg += "販売組織が設定されていません;"
        End If
        If String.IsNullOrEmpty(mdl) And AstMaster.HIN_KBN.GetHinKbn(hin_kbn) <> HIN_KBN_LIST.NISUGATA Then
            errmsg += "モデル区分が設定されていません;"
        End If

        If String.IsNullOrEmpty(errmsg) Then

            'キー項目設定
            dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(row("HIN_CD").ToString).key)

            For i = 0 To row.ItemArray.Count - 1
                Dim columnName As String = row.Table.Columns(i).ColumnName
                Dim dataValue As String = row(i).ToString

                'ワーク項目は読み飛ばし
                If columnName = EPPlusNameManager.XLS_ROWNUM Or columnName = EPPlusNameManager.GROUP_NUM Or columnName = EPPlusNameManager.ROWNUM_IN_GROUP Then Continue For

                '品目区分と例外品目区分によって設定しなくていいところは読み飛ばし
                If templateXls.isExcludeCell(hin_kbn, exhin_kbn, XlsTemplateSchedulerItem.SCHEMA_DATA + "." + columnName) Then Continue For

                If Not String.IsNullOrEmpty(templateXls.HederNames(XlsTemplateSchedulerItem.SCHEMA_HEADER + "." + columnName)) And masterList.ContainsKey(columnName) Then

                    '変換先がありかつ値の設定があるものはコード変換が必要
                    If Not String.IsNullOrEmpty(dataValue) And Not masterList(columnName).ContainsValue(dataValue) Then
                        errmsg += templateXls.getColNameTxt(templateXls.HederNames(XlsTemplateSchedulerItem.SCHEMA_HEADER + "." + columnName)) + "が正しく選択されていません;"
                    Else
                        Dim setValue As String = ""
                        If Not String.IsNullOrEmpty(dataValue) Then
                            setValue = masterList(columnName).Where(Function(x) x.Value = dataValue).First.Key()
                        End If
                        dto.addSelection(SqlBuilder.newSelect(templateXls.HederNames(XlsTemplateSchedulerItem.SCHEMA_HEADER + "." + columnName)).setValue(setValue))
                    End If
                Else
                    'SqlServerでは空白がNullにならないため、挙動を同じにするため空白は明示的にNull(=Nothing)にする
                    If String.IsNullOrEmpty(dataValue) Then dataValue = Nothing
                    dto.addSelection(SqlBuilder.newSelect(columnName).setValue(dataValue))
                End If

            Next
            dto.addSelection(SqlBuilder.newSelect("UPD_YMD").setValue(Now.ToString("yyyyMMdd")))
            dto.addSelection(SqlBuilder.newSelect("UPD_HMS").setValue(Now.ToString("HHmmss")))
            dto.addSelection(SqlBuilder.newSelect("UPD_USR").setValue(Master.LoginUser))
        End If

        'その他付加情報にエラー情報を設定
        dto.addAttrInfo(DTO_ERR_FIELD_ATTR, If(errmsg.Length > 0, Left(errmsg, errmsg.Length - 1), errmsg))

        Return dto

    End Function

    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' バリデーション
    ''' </summary>
    ''' <param name="dto"></param>
    ''' <param name="templateXls">雛形EXCEL操作オブジェクト</param>
    ''' <returns>結果</returns>
    ''' ------------------------------------------------------------------------------
    Private Function rowDataValidation(ByRef dto As GearsDTO, ByRef templateXls As XlsTemplateSchedulerItem) As Boolean
        Dim errmsg As String = ""
        Dim ret As Boolean = False

        '定義の読み込み
        Dim validationTable As ValidationByTableDefine = HinValidators.TableDefine(GPageMediator.ConnectionName)
        Dim validationAstHin As AstMasterHinmokuValidator = HinValidators.HinValidator(GPageMediator.ConnectionName)


        '項目桁数と属性チェック
        Dim selectItem As List(Of SqlSelectItem) = dto.getSelection
        Dim param As Dictionary(Of String, String) = selectItem.ToDictionary(Function(item) item.Column, Function(item) item.Value)
        validationTable.isItemsValidByColName(param)

        If validationTable.getErrMsgList.Count > 0 Then
            'テーブル属性でエラーになったら内容のチェックはしない
            For Each e In validationTable.getErrMsgList
                errmsg += templateXls.getColNameTxt(e.Key) + ":" + e.Value + ";"
            Next
            errmsg = Left(errmsg, errmsg.Length - 1)
        Else
            '画面と同じバリデーション（必要な分だけ実行）
            Dim sb As SqlBuilder = dto.generateSqlBuilder
            Dim validates As New List(Of AbsModelValidator.ModelValidator)
            validates.Add(AddressOf validationAstHin.isHinCodeValid)
            validates.Add(AddressOf validationAstHin.isNisgataNew)
            validates.Add(AddressOf validationAstHin.isExHinKbnCannotChangePattern)
            'validates.Add(AddressOf validationAstHin.isDhyoSouchiValid)    'モデル区分変更時に品目装置チェックで変更できないためアラートに委譲
            validates.Add(AddressOf validationAstHin.IsJiShoDropValid)
            validates.Add(AddressOf validationAstHin.isPackRateValid)

            Dim result As ValidationResults = validationAstHin.Validate(sb, validates, True)
            errmsg += String.Join(";", (From x As ValidationResult In result.GetFails Select x.ErrorMessage).ToList)

        End If

        If String.IsNullOrEmpty(errmsg) Then
            ret = True
        Else
            dto.addAttrInfo(DTO_ERR_FIELD_ATTR, errmsg)    'その他付加情報にエラー情報を設定
        End If
        Return ret

    End Function
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' メッセージの設定
    ''' </summary>
    ''' <param name="strMsg">メッセージ文字列</param>
    ''' <param name="lblMsg">ラベル</param>
    ''' <param name="blnSuccess">True:成功,False:失敗</param>
    ''' <remarks>
    ''' 引数で指定された文字列をlblMsgに設定する。
    ''' blnSuccessがTrueの場合はcssにsuccessをFalseの場合はerrorを設定する。
    ''' </remarks>
    ''' ------------------------------------------------------------------------------
    Private Sub SetMessage(ByRef lblMsg As Label, ByVal strMsg As String, Optional ByVal blnSuccess As Boolean = False)
        lblMsg.Text = strMsg
        lblMsg.CssClass = If(blnSuccess, "ppp-msg success", "ppp-msg error")
    End Sub
#End Region

    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' Validationや値チェック用のオブジェクトを管理するクラス<br/>
    ''' これらは色々なFunctionで使いまわしたいが生成コストが高いため、インスタンスを管理するクラスを作成しそこから取得するようにする
    ''' </summary>
    ''' ------------------------------------------------------------------------------
    Public Class HinValidators
        Private Shared _tableDefine As ValidationByTableDefine = Nothing
        Public Shared Function TableDefine(ByVal conName As String) As ValidationByTableDefine
            If _tableDefine Is Nothing Then _tableDefine = New ValidationByTableDefine(conName, "AST_M_HIN")
            Return _tableDefine
        End Function

        Private Shared _hinValidator As AstMasterHinmokuValidator = Nothing
        Public Shared Function HinValidator(ByVal conName As String) As AstMasterHinmokuValidator
            If _hinValidator Is Nothing Then _hinValidator = New AstMasterHinmokuValidator(conName)
            Return _hinValidator
        End Function

        Private Shared _masterList As Dictionary(Of String, Dictionary(Of String, String)) = Nothing
        Public Shared Function MasterList(ByVal templateXls As XlsTemplateSchedulerItem) As Dictionary(Of String, Dictionary(Of String, String))
            If _masterList Is Nothing Then _masterList = templateXls.getHanyoMasterList
            Return _masterList
        End Function

    End Class

End Class
