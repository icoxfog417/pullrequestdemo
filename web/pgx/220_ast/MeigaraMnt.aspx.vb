﻿Imports Gears

Partial Class _220_ast_MeigaraMnt
    Inherits GearsPage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Dim scHingDS As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
        scHingDS.addLockCheckCol("UPD_YMD", LockType.UDATESTR) '楽観的ロックに使用する列とタイプを指定。
        scHingDS.addLockCheckCol("UPD_HMS", LockType.UTIMESTR)
        scHingDS.addLockCheckCol("UPD_USR", LockType.USER)

        'モデル検証クラスの設定
        Dim hingValidator As New AstMasterHinmokuValidator(Master.ConnectionName)
        scHingDS.ModelValidator = hingValidator

        registerMyControl(pnlMeigara__GFORM, scHingDS)

        'イベントハンドラの登録
        AddHandler HIN_CD__KEY.getControl(Of TextBox).TextChanged, AddressOf Me.txtHIN_CD_TextChanged
        AddHandler HIN_KBN.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlHIN_KBN_SelectedIndexChanged
        AddHandler DHYO_MDL_KBN.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlDHYO_MDL_KBN_SelectedIndexChanged

        '非同期更新処理の登録
        Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)
        'srmOnPage.RegisterAsyncPostBackControl(HIN_KBN.getControl(Of DropDownList))
        srmOnPage.RegisterAsyncPostBackControl(DHYO_MDL_KBN.getControl(Of DropDownList))
        srmOnPage.RegisterAsyncPostBackControl(HIN_CD__KEY.getControl(Of TextBox))

        'ユーザーコントロールの初期化
        SEARCH_CHILD.ConnectionName = Master.ConnectionName
        SEARCH_PARENT.ConnectionName = Master.ConnectionName
        ucHIN_SOUCHI.ConnectionName = Master.ConnectionName

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        resetEscapesWhenSend(pnlMeigara__GFORM, pnlMeigara__GFORM)

        addRelation(HIN_CD__KEY.getControl, pnlMeigara__GFORM)
        addRelation(pnlMeigara__GFORM, pnlMeigara__GFORM)
        setEscapesWhenSend(pnlMeigara__GFORM, pnlMeigara__GFORM, pnlHinSouchi.ID)
        addRelation(DHYO_MDL_KBN.getControl, DHYO_SOUCHI_CD.getControl)

        Dim hin As String = HIN_CD__KEY.getValue
        If Not IsPostBack AndAlso Not String.IsNullOrEmpty(getQueryString("hin_cd")) Then
            hin = getQueryString("hin_cd")
        End If

        If Not IsPostBack And Not String.IsNullOrEmpty(hin) Then '初回ロード
            HIN_CD__KEY.setValue(hin)
            refresh()
            changeHinKbn()
        ElseIf IsReload Then
            'refresh()
        ElseIf Request.Params("__EVENTTARGET") = udpSORT_CD.ID Then 'update panelによるソート順更新
            'ソート順の更新
            Dim sortItems As New GearsDTO(ActionType.SEL)
            sortItems.addSelection(SqlBuilder.newSelect("HIN_SORT"))
            sortItems.addSelection(SqlBuilder.newSelect("PRT_SSN_ORDER"))
            executeBehavior(HIN_CD__KEY.getControl, sortItems) 

        End If

    End Sub
    Private Sub refresh()
        executeBehavior(HIN_CD__KEY.getControl)
        refreshSearchControl()

    End Sub
    Private Sub changeHinKbn()
        '品目区分リストの設定
        Dim dto As New GearsDTO(ActionType.SEL)
        dto.addFilter(SqlBuilder.newFilter("HIN_KBN").eq(AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER)))
        dto.addFilter(SqlBuilder.newFilter("HIN_KBN").eq(AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.PELET)))
        getMyControl(HIN_KBN.ControlId).reload(dto)

        '品目区分リスト選択値による画面制御(複雑になってきたら別メソッドへ切り出し)
        Dim peletOnly As Boolean = False
        If AstMaster.HIN_KBN.GetHinKbn(HIN_KBN.getValue) = AstMaster.HIN_KBN_LIST.PELET Then
            peletOnly = True
        End If

        switchControl(PARENT_HIN_CD.getControl, peletOnly)
        switchControl(KAKUGAI_KBN.getControl, peletOnly)
        switchControl(IKOHIN_FLG.getControl, peletOnly)
        switchControl(PWD_BSH_FLG.getControl, peletOnly)
        switchControl(JISHO_DRP_FLG.getControl, peletOnly)
        switchControl(JISHO_DRP_NSU.getControl, peletOnly)

        '検索コントロールの切替
        refreshSearchControl()

    End Sub
    Private Sub switchControl(ByRef control As WebControl, ByVal enable As Boolean, Optional ByVal inVisible As Boolean = False)
        If Not inVisible Then
            control.Enabled = enable
            If Not enable And Not getMyControl(control.ID) Is Nothing Then
                getMyControl(control.ID).setValue(Nothing) '値初期化
            End If

        Else
            control.Visible = enable
        End If

        If Not enable Then
            setEscapesWhenSend(pnlMeigara__GFORM, pnlMeigara__GFORM, control.ID)
        End If

    End Sub


    Protected Sub txtHIN_CD_TextChanged(sender As Object, e As System.EventArgs)
        '新規品目でデータ更新(Updatepanel以外の箇所は更新されない)
        Dim targets As New Dictionary(Of String, UpdatePanel)
        Dim partialDto As New GearsDTO(ActionType.SEL)

        GPageMediator.fetchControls(Me, Sub(ByRef control As Control, ByRef dto As GearsDTO)
                                            Dim codes As String() = Split(control.ID, "__")
                                            If Not codes Is Nothing AndAlso codes.Length > 1 Then
                                                targets.Add(codes(1), CType(control, UpdatePanel))
                                            End If
                                        End Sub,
                                          Function(ByRef control As Control) As Boolean
                                              '品目デフォルト提案対象のものか確認
                                              If TypeOf control Is UpdatePanel AndAlso control.ID.ToUpper.StartsWith("udpDefault__".ToUpper) Then
                                                  Return True
                                              Else
                                                  Return False
                                              End If
                                          End Function)

        For Each t As String In targets.Keys
            partialDto.addSelection(SqlBuilder.newSelect(t))
        Next

        executeBehavior(HIN_CD__KEY.getControl, partialDto)

        For Each u As UpdatePanel In targets.Values
            u.Update()
        Next
        refreshControl()

    End Sub
    Protected Sub ddlHIN_KBN_SelectedIndexChanged(sender As Object, e As System.EventArgs)
        changeHinKbn()
        refreshSouchiControl()
    End Sub
    Protected Sub ddlDHYO_MDL_KBN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        executeBehavior(DHYO_MDL_KBN.getControl(Of DropDownList))
        refreshSouchiControl()

    End Sub
    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        getLogMsgDescription(executeBehavior(pnlMeigara__GFORM, ActionType.SAVE), lblMsg)
        refreshControl()

    End Sub

    Private Sub refreshControl()
        ucHIN_SOUCHI.Update(HIN_CD__KEY.getValue, HIN_KBN.getValue, DHYO_MDL_KBN.getValue)
        refreshSearchControl()
    End Sub

    Private Sub refreshSouchiControl()
        ucHIN_SOUCHI.Update(HIN_CD__KEY.getValue, HIN_KBN.getValue, DHYO_MDL_KBN.getValue)
        udpSOUCHI_CD.Update()
    End Sub

    Private Sub refreshSearchControl()

        If HIN_KBN.getValue = AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER) Then '重合Ｇ編集中
            '重合Ｇの場合の子品検索(しわ寄せ品目)はペレットとなる。制限は、"自身を親とするペレット"
            SEARCH_CHILD.HinKbn = AstMaster.HIN_KBN_LIST.PELET
            hdnPARENT_HIN_CD.Value = HIN_CD__KEY.getValue
            SEARCH_CHILD.RestrictionControls = hdnPARENT_HIN_CD.ID

        Else
            'ペレットの場合の子品検索(しわ寄せ品目)は荷姿となる。制限は、"自身を親とする荷姿"
            SEARCH_CHILD.HinKbn = AstMaster.HIN_KBN_LIST.NISUGATA
            hdnPARENT_HIN_CD.Value = HIN_CD__KEY.getValue
            SEARCH_CHILD.RestrictionControls = hdnPARENT_HIN_CD.ID
        End If

        udpSearchObj.Update()

    End Sub


End Class
