﻿<%@ Page Title="Ｓ-Ｎａｖｉ＋　一括ダウンロード・アップロード"  Language="VB" MasterPageFile="~/220_ast/astMaster.master" AutoEventWireup="false" CodeFile="UploadHinMst.aspx.vb" Inherits="_220_ast_UploadHinMst" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ MasterType VirtualPath="astMaster.master" %>

<asp:Content ID="SHeaderContent" ContentPlaceHolderID="pppHead" runat="Server" ClientIDMode="Static">
    <!-- headのスクリプト処理箇所。必要なJavaScript処理などがあればここに記載-->
    <meta name="DownloadOptions" content="noopen" />
    <script src="./lib/jquery.bgiframe.min.js" type="text/javascript"></script>
    <!-- スタイル関連 -->
    <style type="text/css">
        div.lblText
        {
            margin-bottom:10px;
            color:Green;
            font-weight:bold;
        }
        #lblMsgTitle
        {
            margin-left:60px;
            color:Green;
        }
        #pnlErrMessage
        {
            margin-left:20px;
            padding:10px;
            width:80%;
            height:300px;
            overflow-y:scroll;
            white-space:nowrap;
            color:blue;
            background-color:#CCFFFF;
            border:1px gray solid;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function clearMsg() {
            // アップロード、ダウンロードボタンをクリックされた際にエラーメッセージをクリアする。
            document.getElementById("lblMsgDL").innerHTML = "";
            document.getElementById("lblMsgUL").innerHTML = "";
            document.getElementById("lblMsgArea").innerHTML = "";
            document.getElementById("lblFile").innerHTML = "";
            document.getElementById("lblCount").innerHTML = "";
            document.getElementById("pnlResult").style.display = "none"
        }
    </script>
</asp:Content>

<asp:Content ID="SPreDeclare" ContentPlaceHolderID="pppPreDeclare" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="SPageTitle" ContentPlaceHolderID="pppPageTitle" Runat="Server" ClientIDMode=Static>
スケジューラ品目　一括ダウンロード・アップロード
</asp:Content>


<asp:Content ID="clientCenter" ContentPlaceHolderID="pppContent" runat="Server" ClientIDMode="Static">
    <asp:Panel ID="updArea" CssClass="ppp-switch-area" runat="server">
        <!--検索条件-->
        <asp:Panel ID="pnlGFilter" runat="server" Style="margin: 0px 0px 0px 10px;">
            <div>
                <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px;width:98%;">　ダウンロード</h3>
                <div class="ppp-indent">
                    <ui:unitItem ID="HIN_KBN" CtlKind="DDL" runat="server" LabelText="品目区分" IsNeedAll=True />
                    <ui:unitItem ID="DHYO_MDL_KBN" CtlKind="DDL" runat="server" LabelText="代表モデル区分" IsNeedAll=True AutoPostBack=True />
                    <asp:UpdatePanel id="udpSOUCHI_CD" runat="server" UpdateMode=Conditional>
                        <ContentTemplate>
                            <ui:unitItem ID="DHYO_SOUCHI_CD" CtlKind="DDL" runat="server" LabelText="代表装置" IsNeedAll=True />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <ui:unitItem ID="SCHEDULER_FLG" CtlKind="RBL" runat="server" LabelText="ＳＣ対象" IsNeedAll=True  DefaultCheckIndex=1/>
                    <ui:unitItem ID="MK_FLG" CtlKind="RBL" runat="server" LabelText="無効フラグ" IsNeedAll=True DefaultCheckIndex=1/>
                    <ui:unitItem ID="SORT_KIND" CtlKind="RBL" runat="server" LabelText="ソート順" Width="260" DefaultCheckIndex=0 />
                    <br style="clear: both" />
                    <br style="clear: both" />
                    <asp:Button ID="btnDownload" runat="server" Text="ダウンロード" style="width:95px;" />
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <asp:Label ID="Labelhelp" runat="server" Text="※使い方はこちらを参照下さい！ -->"></asp:Label>&nbsp&nbsp&nbsp
                    <asp:HyperLink ID="lnkTohelp" runat="server" Target="_blank" NavigateUrl="help/UploadHinMst.aspx-help.htm" Text="ヘルプ" Class="link-field"/>
                </div>
            </div>
            <asp:Label ID="lblMsgDL" runat="server" Text="" CssClass="ppp-msg error"></asp:Label>
            <br />
            <div>
                <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px;width:98%;">　アップロード</h3>
                <div class="ppp-indent">
                    <br style="clear: both" />
                    <asp:FileUpload ID="fupExcel" runat="server" width="700px" />
                    <br style="clear: both" />
                    <br style="clear: both" />
                    <asp:Button ID="btnUpload" runat="server" Text="アップロード" style="width:95px;" />
                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <asp:Label ID="Label1" runat="server" Text="※アップロード後は、アラートが出ていないか確認してください！ -->"></asp:Label>&nbsp&nbsp&nbsp
                    <asp:HyperLink ID="lnkTo" runat="server" Target="_blank" NavigateUrl="AlertList.aspx" Text="アラート確認画面" Class="link-field"/>
                </div>
            <asp:Label ID="lblMsgUL" runat="server" Text="" CssClass="ppp-msg error"></asp:Label>
            <br />
            </div>
        </asp:Panel>
    </asp:Panel>
    <!--表示-->
    <!--エラーメッセージ-->
    <asp:Label ID="lblLog" runat="server" Text="" CssClass="ppp-msg"></asp:Label>
    <!--アップロード用エラーメッセージ-->
    <div Style="margin-left:10px;">
        <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px;width:98%;">　メッセージエリア  </h3>
　      <div ID='pnlErrMessage'>
            <asp:Panel ID="pnlResult" runat="server" Style="margin: 0px 0px 0px 10px;">
                <asp:Label ID="lblResultTitle" runat="server" Text="【アップロード結果】" Font-Bold="true"></asp:Label><br /><br />
                <asp:Label ID="lblFile" runat="server" Text=" "></asp:Label><br /><br />
                <asp:Label ID="lblCount" runat="server" Text=" "></asp:Label><br /><br />
                <asp:Label ID="lblErrTitile" runat="server" Text="【エラー内容】" Font-Bold="true"></asp:Label><br /><br />
                <asp:Label ID="lblMsgArea" runat="server" Text=" "></asp:Label>
            </asp:Panel>
        </div>
    </div>
    <br /><br /><br />
    <asp:HiddenField ID="hdnUSER" runat="server" Value="" />
    <asp:HiddenField ID="hdnUlFile" runat="server" Value="取込ファイル:{0}" />
    <asp:HiddenField ID="hdnUlCount" runat="server" Value="取込件数 {0}件、　正常 {1}件、　エラー {2}件" />

</asp:Content>
