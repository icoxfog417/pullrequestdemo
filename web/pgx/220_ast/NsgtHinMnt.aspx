﻿<%@ Page Title="Ｓ-Ｎａｖｉ＋　荷姿詳細メンテナンス" Language="VB" MasterPageFile="~/220_ast/astSwitchItemMaster.master" AutoEventWireup="false" CodeFile="NsgtHinMnt.aspx.vb" Inherits="_220_ast_NsgtHinMnt" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ Register src="../parts/220_ast/SCItemSearch.ascx" tagname="sh" tagprefix="search" %>
<%@ Register src="../parts/220_ast/SCItemHinSouchi.ascx" tagname="hinSouchi" tagprefix="hs" %>

<%@ MasterType VirtualPath="astSwitchItemMaster.master" %>

<asp:Content ID="SHeader" ContentPlaceHolderID="pppSHeader" Runat="Server" ClientIDMode=Static>
    <link href="css/jquery.simple-color-picker.css" rel="stylesheet" type="text/css" />
    <script src="./lib/jquery.simple-color-picker.js" type="text/javascript" ></script>
    <script language="javascript">
        $(function () {
            $(".link-field").click(function () {
                window.open($(this).attr("href"), $(this).attr("target")).focus();
            })
        })
    </script>

</asp:Content>
<asp:Content ID="PreDeclare" ContentPlaceHolderID="pppSPreDeclare" Runat="Server" ClientIDMode=Static>
    <asp:ScriptManagerProxy runat="server">
            <Services>
                <asp:ServiceReference Path="~/service/000_common/DBDataService.asmx" />
            </Services>  
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="SPageTitle" ContentPlaceHolderID="pppSPageTitle" Runat="Server" ClientIDMode=Static>
荷姿詳細メンテナンス
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Label ID="lblMsg" runat="server" Text="" CssClass="ppp-msg success"></asp:Label>
</asp:Content>
<asp:Content ID="SHeaderContent" ContentPlaceHolderID="pppSHeaderContent" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="UpperAreaTitle" ContentPlaceHolderID="pppUpperAreaTitle" Runat="Server" ClientIDMode=Static>
荷姿詳細メンテナンス
</asp:Content>

<asp:Content ID="UpperPanel" ContentPlaceHolderID="pppUpperPanel" Runat="Server" ClientIDMode=Static>

		<!-- SC品目マスタ詳細画面 -->
		<asp:Panel id="pnlGFORM__HIN" runat="server">
				<ui:unitItem ID="HIN_CD__KEY" CtlKind="TXT" runat="server" LabelText="品目コード" AutoPostBack=True CssClass="gears-GRequired gears-GByteLengthBetween_MinLength_0_Length_18" SearchAction="SEARCH_NSGT_1.openDialog()" />
                <asp:Panel ID="pnlHIN_DSP" runat="server">
                    <asp:UpdatePanel id="udpHin" runat="server" UpdateMode=Conditional>
                        <ContentTemplate>
                            <ui:unitItem ID="HIN_TXT" CtlKind="TXT" runat="server" LabelText="品目テキスト" Width="330" CssClass="gears-GByteLengthBetween_MinLength_0_Length_40"/>
							<ui:unitItem ID="SCHEDULER_FLG" CtlKind="CHB" runat="server" LabelText="SC対象フラグ" Width="80"/>
							<ui:unitItem ID="MK_FLG" CtlKind="RBL" runat="server" LabelText="無効フラグ" />
				            <br style="clear:both">
                            <ui:unitItem ID="PARENT_HIN_CD" CtlKind="TXT" runat="server" LabelText="親銘柄コード" SearchAction="SEARCH_PELET_1.openDialog()" AutoPostBack=True CssClass="gears-GByteLengthBetween_MinLength_0_Length_18" />
                            <ui:unitItem ID="PARENT_HIN_TXT__GCON" CtlKind="LBL" runat="server" LabelText="親銘柄名称" IsEditable=False Width="400"/>
				            <br style="clear:both">
				            <ui:unitItem ID="HIN_KBN_TXT__GCON" CtlKind="LBL" runat="server" LabelText="品目区分" />
				            <ui:unitItem ID="VKORG_TXT__GCON" CtlKind="LBL" runat="server" LabelText="販売組織"  />
				            <ui:unitItem ID="DHYO_MDL_KBN_TXT__GCON" CtlKind="LBL" runat="server" LabelText="代表モデル区分" />
				            <ui:unitItem ID="DHYO_SOUCHI_CD__GCON" CtlKind="LBL" runat="server" LabelText="代表装置コード" />
				            <ui:unitItem ID="EXHIN_KBN_TXT__GCON" CtlKind="LBL" runat="server" LabelText="例外品目区分" />
				            <ui:unitItem ID="IKOHIN_FLG" CtlKind="CHB" runat="server" LabelText="移行品銘柄" IsEditable="false" />
				            <br style="clear:both">
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
				<ui:unitItem ID="PIM_OUT_CD" CtlKind="TXT" runat="server" LabelText="PIM-Aid 品目コード" CssClass="gears-GByteLengthBetween_MinLength_0_Length_18"/>
				<ui:unitItem ID="SCP_OUT_CD" CtlKind="TXT" runat="server" LabelText="SAP 品目コード" CssClass="gears-GByteLengthBetween_MinLength_0_Length_18"/>
				<br style="clear:both">
				<hr class="dot" width="99%" />

				<ui:unitItem ID="MIN_HOJU_SRY" CtlKind="TXT" runat="server" LabelText="最小補充生産量" CssClass="gears-GRequired gears-GNumeric gears-GPeriodPositionOk_BeforeP_4_AfterP_2 gears-GComp_CompType_>_TgtValue_0 " />
				<ui:unitItem ID="ADD_HOJU_SRY" CtlKind="TXT" runat="server" LabelText="増分補充生産量" CssClass="gears-GRequired gears-GNumeric gears-GPeriodPositionOk_BeforeP_4_AfterP_2 gears-GComp_CompType_>_TgtValue_0 " />
				<ui:unitItem ID="NSGT_TNI_NUM" CtlKind="TXT" runat="server" LabelText="荷姿単位" Width="60" CssClass="gears-GNumeric gears-GNumeric gears-GPeriodPositionOk_BeforeP_5_AfterP_3" />
				<ui:unitItem ID="PALT_TNI_NUM" CtlKind="TXT" runat="server" LabelText="パレット単位" Width="60" CssClass="gears-GNumeric gears-GNumeric gears-GPeriodPositionOk_BeforeP_5_AfterP_3" />
				<ui:unitItem ID="SCP_PLANOM" CtlKind="TXT" runat="server" LabelText="計画単位"  Width="60" CssClass="gears-GByteLengthBetween_MinLength_0_Length_3" />
				<ui:unitItem ID="PACK_RATE" CtlKind="TXT" runat="server" LabelText="充填計画比率"  Width="60" CssClass="gears-GNumeric gears-GNumeric gears-GPeriodPositionOk_BeforeP_1_AfterP_2" />
				<br style="clear:both">
                <ui:unitItem ID="DISP_COLOR" CtlKind="TXT" runat="server" LabelText="色" CssClass="cpicker" Width="60" />
				<ui:unitItem ID="CMNT" CtlKind="TXT" runat="server" LabelText="コメント" Width="420" CssClass="gears-GByteLengthBetween_MinLength_0_Length_100" />
			    <asp:HiddenField ID="hdnHING_CD" runat="server" />
			    <asp:HiddenField ID="hdnDHYO_MDL_KBN" runat="server" />
                <asp:HiddenField ID="hdnHIN_KBN" runat="server" />
        </asp:Panel>
				<br style="clear:both">
				<hr class="dot" width="99%" />
        <asp:Panel id="pnlHinSouchi" runat="server" >
            <hs:hinSouchi ID="ucHIN_SOUCHI" runat="server"  />
        </asp:Panel>
				<br style="clear:both">
			    <asp:Button ID="btnUpdate" runat="server" Text="　更 新　" />
                <br style="clear:both">
</asp:Content>

<asp:Content ID="LowerAreaTitle" ContentPlaceHolderID="pppLowerAreaTitle" Runat="Server" ClientIDMode=Static>
検索条件
</asp:Content>

<asp:Content ID="LowerPanel" ContentPlaceHolderID="pppLowerPanel" Runat="Server" ClientIDMode=Static >
     <asp:Panel id="pnlGFilter" runat="server"  Width="600" > <%-- "GFILTER"をIDに含むこと。 --%>
	   <ui:unitItem ID="HIN_CD__2" CtlKind="TXT" runat="server" LabelText="品目コード" Operator="START_WITH" SearchAction="SEARCH_NSGT_2.openDialog()" />
	   <ui:unitItem ID="HIN_TXT__2" CtlKind="TXT" runat="server" LabelText="品目テキスト" Operator="LIKE" Width="300"/>
        <ui:unitItem ID="PARENT_HIN_CD__2" CtlKind="TXT" runat="server" LabelText="親銘柄コード" SearchAction="SEARCH_PELET_2.openDialog()"　Operator="START_WITH" />
        <br style="clear:both"/>

        <div width="100%" style="text-align:right">
            <asp:Button id="btnSearch" runat="server" Text=" 検 索 " />
        </div>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Footer" ContentPlaceHolderID="pppSFooter" Runat="Server" ClientIDMode=Static>
	<!-- 品目一覧画面 -->
	<asp:Panel id="pnlListArea" runat="server"  style="margin-left:28px;">
        <asp:GridView id="grvHin" runat="server" CssClass=ppp-table HeaderStyle-CssClass=ppp-table-head 
        EnableViewState=False DataSourceID=odsHin　DataKeyNames="HIN_CD"
        PageSize=20 AllowPaging=True AutoGenerateColumns=False >
            <Columns>
                <asp:CommandField ShowSelectButton=True HeaderText="選択" HeaderStyle-Width="40" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="HIN_CD" HeaderText="品目コード">
                </asp:BoundField>
                <asp:BoundField DataField="NSGT_CD" HeaderText="荷姿" HeaderStyle-Width="40">
                </asp:BoundField>
                <asp:BoundField DataField="HIN_TXT" HeaderText="品目名称">
                </asp:BoundField>
                <asp:TemplateField HeaderText="親銘柄コード">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkTo" runat="server" Target="scParent" 
                            NavigateUrl=<%# AstMaster.Common.AstMasterNavigation.makeLink(DataBinder.Eval(container.DataItem, "PARENT_HIN_CD"),AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.PELET),DataBinder.Eval(container.DataItem, "EXHIN_KBN")) %>
                            Text=<%# DataBinder.Eval(Container.DataItem, "PARENT_HIN_CD")%>
                            ToolTip=<%# DataBinder.Eval(Container.DataItem, "PARENT_HIN_TXT")%>
                         CssClass="link-field" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="VKORG_TXT" HeaderText="販売組織"  HeaderStyle-Width="60" ItemStyle-Width="60" ItemStyle-HorizontalAlign="Center">
                </asp:BoundField>
                <asp:BoundField DataField="DHYO_MDL_KBN_TXT" HeaderText="代表モデル" HeaderStyle-Width="70" ItemStyle-Width="70" ItemStyle-HorizontalAlign="Center">
                </asp:BoundField>
                <asp:BoundField DataField="MIN_HOJU_SRY" HeaderText="最小補充"  HeaderStyle-Width="70" ItemStyle-Width="70" ItemStyle-HorizontalAlign="right">
                </asp:BoundField>
                <asp:BoundField DataField="ADD_HOJU_SRY" HeaderText="増分補充"  HeaderStyle-Width="70" ItemStyle-Width="70" ItemStyle-HorizontalAlign="right">
                </asp:BoundField>
                <asp:TemplateField HeaderText="SC対象" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chbSCHEDULER_FLG" Checked='<%# Bind("SCHEDULER_FLG") %>' Enabled=false />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="無効" HeaderStyle-Width="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chbMK_FLG" Checked='<%# Bind("MK_FLG") %>' Enabled=false />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
    </asp:GridView>
    <asp:ObjectDataSource id="odsHin" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount" typename="AstMaster.AST_M_HIN" EnablePaging=True >
    </asp:ObjectDataSource>

		<asp:Label ID="lblCount" runat="server" Text="" CssClass="ppp-msg success"></asp:Label>
		<asp:Label ID="lblLog" runat="server" Text="" CssClass="ppp-msg success"></asp:Label>
	</asp:Panel>
    <br style="clear:both">

</asp:Content>

<asp:Content ID="SEndDeclare" ContentPlaceHolderID="pppSEndDeclare" Runat="Server" ClientIDMode=Static>
    <search:sh ID="SEARCH_NSGT_1" runat="server" TgtControl=txtHIN_CD__KEY HinKbn=NISUGATA AutoPostBack=true />
    <search:sh ID="SEARCH_NSGT_2" runat="server" TgtControl=txtHIN_CD__2    HinKbn=NISUGATA />
    <search:sh ID="SEARCH_PELET_1" runat="server" TgtControl=txtPARENT_HIN_CD    HinKbn=PELET AutoPostBack=true />
    <search:sh ID="SEARCH_PELET_2" runat="server" TgtControl=txtPARENT_HIN_CD__2 HinKbn=PELET />
    <asp:HiddenField ID="hdnEXHIN_KBN" runat="server" Value="0" />
</asp:Content>
