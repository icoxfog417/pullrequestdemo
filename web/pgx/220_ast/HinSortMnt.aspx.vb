﻿Imports Gears

''' ------------------------------------------------------------------------------
''' Project     : 220_ast
''' Class       : _220_ast_HinSortMnt
''' 
''' -------------------------------------------------------------------------------
''' <summary>
''' AST銘柄ソート順メンテ画面
''' </summary>
''' <remarks>
''' </remarks>
''' <hisotry>
'''  [K.Takahashi] 2012/05/24 Created
''' </hisotry>
''' -------------------------------------------------------------------------------
Partial Class _220_ast_HinSortMnt
    Inherits GearsPage

#Region "定数"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 例外品目区分：通常
    ''' </summary>
    ''' <remarks>
    ''' 例外品目区分：通常
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Const strHIN_KBN_USUALLY As String = "0"

#End Region

#Region "変数"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' dsHinSouchi
    ''' </summary>
    ''' <remarks>
    ''' 品目マスタ Gears Data Souce
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private dsHin As AstMaster.AST_M_HIN = Nothing

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' gvUtil
    ''' </summary>
    ''' <remarks>
    ''' GridViewユーティリティ
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private gvUtil As GridViewUtility = Nothing

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' updList
    ''' </summary>
    ''' <remarks>
    ''' 更新用リスト
    ''' GridViewに表示されているデータで更新対象を保存する。
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private updList As New List(Of GearsDTO)

#End Region

#Region "イベント"

#Region "ページ"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Init
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        dsHin = New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)

        '詳細表示用のコントロール/データソースを登録
        registerMyControl(pnlGFilter, dsHin)
        registerMyControl(grvHin, dsHin)

        'GridViewユーティリティ初期化
        gvUtil = New GridViewUtility(grvHin)
        gvUtil.addHideColumn("UPD_YMD")
        gvUtil.addHideColumn("UPD_HMS")
        gvUtil.addHideColumn("PREV_HIN_SORT")
        gvUtil.addHideColumn("PRT_SSN_ORDER")

        '親品目検索画面設定
        SEARCH_POWDER.ConnectionName = GPageMediator.ConnectionName
        SEARCH_POWDER.RestrictionControls = hdnMK_FLG.ID + "," + hdnEXHIN_KBN.ID + "," + hdnSCHEDULER_FLG.ID

        'ペレットのみ対象
        hdnHIN_KBN.Value = AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.PELET)

        'ヘッダー出力(thead,tbody)
        grvHin.UseAccessibleHeader = True

        '保存ボタン不可視化
        btnUpdate.Visible = False

        '閉じるイベント
        btnClose.OnClientClick = "fncCloseWindow()"

        'イベントハンドラの登録
        AddHandler PARENT_HIN_CD.getControl(Of TextBox).TextChanged, AddressOf Me.txtPARENT_HIN_CD_TextChanged

        '非同期更新処理の登録
        Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)
        srmOnPage.RegisterAsyncPostBackControl(PARENT_HIN_CD.getControl(Of TextBox))

    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Load
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then
        Else
            hdnUpdFunc.Value = getQueryString("upd_func")
            PARENT_HIN_CD.setValue(getQueryString("parent_hin_cd"))
            SetParentInfo()
            refresh()
        End If
        'データが存在しない場合もヘッダーを出力する。
        grvHin.ShowHeaderWhenEmpty = True

        ''javascriptを定義
        Dim csm As ClientScriptManager = Page.ClientScript
        Dim sb As New StringBuilder(String.Empty)

        sb.Append("<script language=""javascript"">")
        sb.Append("function fncParentUpdate(){")
        If Not String.IsNullOrEmpty(hdnUpdFunc.Value) Then
            sb.Append("if (window.opener && !window.opener.closed) { window.opener." + hdnUpdFunc.Value + "(); }")
        End If
        sb.Append("}")
        sb.Append("</script>")
        If Not csm.IsClientScriptBlockRegistered("fncParentUpdate") Then
            csm.RegisterClientScriptBlock(Me.GetType(), "fncParentUpdate", sb.ToString(), False)
        End If

    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_LoadComplete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If grvHin.HeaderRow IsNot Nothing Then
            'theadタグとtbodyタグを出力
            'Page_Load時だとHeaderRowがまだ作成されていない。またRowDataBound()時には、grvHin.DataBound()を行わないと呼び出されないのでここで行う。
            grvHin.HeaderRow.TableSection = TableRowSection.TableHeader

            'GridViewが1行でもあれば保存ボタンを可視化する。
            If grvHin.Rows.Count > 0 Then
                btnUpdate.Visible = True
            End If
        End If

    End Sub

#End Region

#Region "GridView"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' grvHinSouchi_RowDataBound
    ''' </summary>
    ''' <remarks>
    ''' GridViewにデータがBoundされる時に呼ばれる
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/08 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub grvHinSouchi_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvHin.RowDataBound

    End Sub

#End Region

#Region "Button"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' btnUpdate_Click
    ''' </summary>
    ''' <remarks>
    ''' 更新ボタン押下イベント
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim blnRslt As Boolean = True
        If IsReload Then
            Log.Add(grvHin.ID, New GearsException("リロードと判定されました。更新処理" + GearsDTO.getAtypeString(ActionType.UPD) + "は実行せず、データのロードのみ行います"))
        Else
            updList.Clear()
            Dim result As String = gvUtil.fetchEachRow(AddressOf Me.fetchHinSouchiRow)
            If result = "" Then
                '品目ソート順更新処理
                result = executeHinSortUpd()
                If result = "" Then
                    refresh()
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "key", "fncParentUpdate()", True)
                End If
            End If
            If result <> "" Then
                Log.Add(grvHin.ID, New GearsDataValidationException(result))
                blnRslt = False
            End If
        End If
        getLogMsgDescription(blnRslt, lblMsg)

    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' btnSearch_Click
    ''' </summary>
    ''' <remarks>
    ''' 検索ボタン押下イベント
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMsg.Text = ""
        refresh()
    End Sub

#End Region

#Region "PARENT_HIN_CD"
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' txtPARENT_HIN_CD_TextChanged
    ''' </summary>
    ''' <remarks>
    ''' 親銘柄コードが変更された際の処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/21 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub txtPARENT_HIN_CD_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SetParentInfo()
    End Sub

#End Region

#End Region

#Region "その他関数"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' fetchHinSouchiRow
    ''' </summary>
    ''' <remarks>
    ''' 銘柄ソート順更新処理(行単位)
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/24 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function fetchHinSouchiRow(ByVal index As Integer, ByRef rowInfo As Dictionary(Of String, TableCell)) As String
        Dim dto As New GearsDTO(ActionType.UPD)
        Dim result As String = ""
        Dim strHinCd As String = String.Empty

        Dim txtSort As TextBox = rowInfo("ソート順").FindControl("txtHIN_SORT")
        Dim txtPrevSort As TextBox = rowInfo("PREV_HIN_SORT").FindControl("txtPREV_HIN_SORT")
        Dim txtSsnOrder As TextBox = rowInfo("生産順").FindControl("txtPRT_SSN_ORDER")
        Dim txtPrevSsnOrder As TextBox = rowInfo("PREV_PRT_SSN_ORDER").FindControl("txtPREV_PRT_SSN_ORDER")

        If Not ((txtSort IsNot Nothing AndAlso txtPrevSort IsNot Nothing AndAlso txtSort.Text <> txtPrevSort.Text) OrElse _
            (txtSsnOrder IsNot Nothing AndAlso txtPrevSsnOrder IsNot Nothing AndAlso txtSsnOrder.Text <> txtPrevSsnOrder.Text)) Then
            '処理対象外
            Return result
        End If

        GPageMediator.fetchControls(grvHin.Rows(index), AddressOf Me.fetchInRowControl, AddressOf isValidTarget, dto)

        For Each item As KeyValuePair(Of String, TableCell) In rowInfo
            Select Case item.Key
                Case "品目コード"
                    dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(item.Value.Text).key)
                    strHinCd = item.Value.Text
                Case "UPD_YMD"
                    dto.addFilter(SqlBuilder.newFilter(item.Key).eq(item.Value.Text).key)
                    dto.addSelection(SqlBuilder.newSelect(item.Key).setValue(dsHin.getLockTypeValue(LockType.UDATESTR)))
                Case "UPD_HMS"
                    dto.addFilter(SqlBuilder.newFilter(item.Key).eq(item.Value.Text).key)
                    dto.addSelection(SqlBuilder.newSelect(item.Key).setValue(dsHin.getLockTypeValue(LockType.UTIMESTR)))
            End Select
        Next
        'ログインユーザ
        dto.addSelection(SqlBuilder.newSelect("UPD_USR").setValue(Master.LoginUser))
        If String.IsNullOrEmpty(dto.getAttrInfo("VALIDATION_ERR")) Then
            updList.Add(dto)
        Else
            result = "品目コード：" + strHinCd + "のデータにエラーがあります：" + dto.getAttrInfo("VALIDATION_ERR") + "<br/>"
        End If

        Return result

    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' fetchInRowControl
    ''' </summary>
    ''' <remarks>
    ''' 銘柄ソート順更新処理(行単位)
    ''' 行から更新対象の項目を取得し、更新対象として設定する。
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/24 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub fetchInRowControl(ByRef control As Control, ByRef dto As GearsDTO)
        Dim inRowCon As New GearsControl(control, GPageMediator.ConnectionName, GPageMediator.DsNameSpace)
        Dim conInfo As New GearsControlInfo(inRowCon)
        conInfo.IsFormAttribute = True '各コントロールは、更新用のフォームコントロールとして認識させる
        dto.addControlInfo(conInfo)
        'バリデーション
        If Not inRowCon.isValidateOk And String.IsNullOrEmpty(dto.getAttrInfo("VALIDATION_ERR")) Then
            dto.addAttrInfo("VALIDATION_ERR", inRowCon.getValidatedMsg)
        End If
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' isValidTarget
    ''' </summary>
    ''' <remarks>
    ''' データチェック処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/24 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function isValidTarget(ByRef control As Control) As Boolean
        If control.Visible = True Then
            Return GPageMediator.isTargetControl(control)
        Else
            Return False 'Visible Falseの場合対象外
        End If
    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' executeHinSortUpd
    ''' </summary>
    ''' <remarks>
    ''' 更新処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/24 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Function executeHinSortUpd() As String
        Dim result As String = ""
        '各行更新
        For index As Integer = 0 To updList.Count - 1
            Dim preStr As String = ""
            If updList.Count > 1 Then
                preStr = (index + 1).ToString + "行目："
            End If
            Try
                dsHin.gUpdate(updList(index))
            Catch ex As GearsRequestedActionInvalid
                result += preStr + "データは他のユーザーにより更新されています。再度更新してください。<br/>"
            Catch ex As Exception
                Dim msg As String = ""
                If Not ex.InnerException Is Nothing Then
                    msg = ex.InnerException.Message
                Else
                    msg = ex.Message
                End If
                result += preStr + "データは更新されませんでした：" + msg + "<br/>"
            End Try
        Next

        Return result

    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 再表示
    ''' </summary>
    ''' <remarks>
    ''' フィルタの値でロード
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/24 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub refresh()
        grvHin.DataSource = dsHin.gSelect(makeSendMessage(pnlGFilter))
        grvHin.DataBind()
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' SetParentInfo
    ''' </summary>
    ''' <remarks>
    ''' 親品目の情報を取得して画面項目に設定する。
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/21 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub SetParentInfo()

        If Not String.IsNullOrEmpty(PARENT_HIN_CD.getValue) Then
            Dim dsParent As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
            Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)
            dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(PARENT_HIN_CD.getValue))
            dto.addFilter(SqlBuilder.newFilter("MK_FLG").eq("0"))

            '親銘柄の情報を取得
            Dim dt As System.Data.DataTable = dsParent.gSelect(dto)
            If dt.Rows.Count > 0 Then
                'If Not IsDBNull(dt.Rows(0).Item("HIN_TXT")) Then
                '    PARENT_HIN_TXT.setValue(dt.Rows(0).Item("HIN_TXT"))
                'End If
                If Not IsDBNull(dt.Rows(0).Item("DHYO_MDL_KBN")) Then
                    Dim strValue = dt.Rows(0).Item("DHYO_MDL_KBN")
                    If DHYO_MDL_KBN.getControl(Of DropDownList).Items.FindByValue(strValue) IsNot Nothing Then
                        DHYO_MDL_KBN.setValue(strValue)
                    End If
                End If
            End If

        End If
    End Sub

#End Region


End Class

