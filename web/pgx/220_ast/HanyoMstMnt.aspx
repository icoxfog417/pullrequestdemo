﻿<%@ Page Title="汎用テーブル参照" Language="VB" MasterPageFile="~/220_ast/astSwitchItemMaster.master" AutoEventWireup="false" CodeFile="HanyoMstMnt.aspx.vb" Inherits="_220_ast_HanyoMstMnt" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ MasterType VirtualPath="astSwitchItemMaster.master" %>

<asp:Content ID="SHeader" ContentPlaceHolderID="pppSHeader" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="SPageTitle" ContentPlaceHolderID="pppSPageTitle" Runat="Server" ClientIDMode=Static>
汎用テーブル参照画面
</asp:Content>
<asp:Content ID="SHeaderContent" ContentPlaceHolderID="pppSHeaderContent" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="UpperAreaTitle" ContentPlaceHolderID="pppUpperAreaTitle" Runat="Server" ClientIDMode=Static>
汎用テーブル詳細画面
</asp:Content>
<asp:Content ID="UpperPanel" ContentPlaceHolderID="pppUpperPanel" Runat="Server" ClientIDMode=Static>
    <asp:Panel id="pnlHanyo__GFORM" runat="server">
            <ui:unitItem ID="HANYO_ID__FORM" CtlKind="TXT" runat="server" LabelText="マスタID"  IsEditable="false" />
            <ui:unitItem ID="HYMST_ID_TXT__GCON" CtlKind="LBL" runat="server" LabelText="マスタ名称" Width="280" />
            <br style="clear:both">
            <ui:unitItem ID="HYMST3_KEY" CtlKind="TXT" runat="server" LabelText="KEY3"  IsEditable="false" />
            <ui:unitItem ID="HYMST4_KEY" CtlKind="TXT" runat="server" LabelText="KEY4" IsEditable="false" />
            <ui:unitItem ID="HYMST5_KEY" CtlKind="TXT" runat="server" LabelText="KEY5" IsEditable="false" />
            <ui:unitItem ID="HYMST_SORTJUN" CtlKind="TXT" runat="server" LabelText="ソート順"  CssClass="" IsEditable="false" />
            <br style="clear:both">
            <ui:unitItem ID="HYMST1_TXT" CtlKind="TXT" runat="server" LabelText="テキスト1" Width="280" IsEditable="false" />
            <ui:unitItem ID="HYMST2_TXT" CtlKind="TXT" runat="server" LabelText="テキスト2" Width="280" IsEditable="false" />
            <ui:unitItem ID="HYMST3_TXT" CtlKind="TXT" runat="server" LabelText="テキスト3" Width="280" IsEditable="false" />
            <br style="clear:both">
            <ui:unitItem ID="HYMSTL_TXT" CtlKind="TXT" runat="server" LabelText="テキスト長" Width="900" IsEditable="false" />
            <br style="clear:both">
            <ui:unitItem ID="HYMST1_SRY" CtlKind="TXT" runat="server" LabelText="汎用マスタ数量1" IsEditable="false" />
            <ui:unitItem ID="HYMST2_SRY" CtlKind="TXT" runat="server" LabelText="汎用マスタ数量2" IsEditable="false" />
            <ui:unitItem ID="HYMST3_SRY" CtlKind="TXT" runat="server" LabelText="汎用マスタ数量3" IsEditable="false" />
            <ui:unitItem ID="HYMST1_DATE" CtlKind="TXT" runat="server" LabelText="汎用マスタ日付1" IsEditable="false" />
            <ui:unitItem ID="HYMST2_DATE" CtlKind="TXT" runat="server" LabelText="汎用マスタ日付2" IsEditable="false" />
            <br style="clear:both">
    </asp:Panel>
</asp:Content>

<asp:Content ID="LowerAreaTitle" ContentPlaceHolderID="pppLowerAreaTitle" Runat="Server" ClientIDMode=Static>
検索エリア
</asp:Content>
<asp:Content ID="LowerPanel" ContentPlaceHolderID="pppLowerPanel" Runat="Server" ClientIDMode=Static>

    <asp:Panel id="pnlHanyo__GFilter" runat="server" Width="720px" >

        <ui:unitItem ID="HANYO_ID" CtlKind="DDL" runat="server" LabelText="マスタ名" IsNeedAll=True  Width="250" AutoPostBack=True />
        <ui:unitItem ID="HYMST1_TXT__SEARCH" CtlKind="TXT" runat="server" LabelText="テキスト１" Operator="LIKE"  />
        <ui:unitItem ID="HYMST2_TXT__SEARCH" CtlKind="TXT" runat="server" LabelText="テキスト２"  Operator="LIKE" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnSearch" runat="server" Text="　検索　" />

    </asp:Panel>

</asp:Content>

<asp:Content ID="SFooter" ContentPlaceHolderID="pppSFooter" Runat="Server" ClientIDMode=Static>
    <asp:Panel runat="server" style="margin-left:27px;">
        <asp:GridView id="grvHanyo" runat="server" CssClass="ppp-table" HeaderStyle-CssClass=ppp-table-head 
        EnableViewState=False DataSourceID=odsHanyo AutoGenerateSelectButton=false DataKeyNames="HANYO_ID,HYMST3_KEY,HYMST4_KEY,HYMST5_KEY"
        PageSize=20 AllowPaging=True AutoGenerateColumns=False>
            <Columns>
                <asp:CommandField ShowSelectButton=True HeaderText="選択" />
                <asp:BoundField DataField="HANYO_ID" HeaderText="マスタID" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST_ID_TXT" HeaderText="マスタ名" ItemStyle-Width="180">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST3_KEY" HeaderText="KEY3" ItemStyle-Width="120">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST4_KEY" HeaderText="KEY4" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST5_KEY" HeaderText="KEY5" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST_SORTJUN" HeaderText="ソート順" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST1_TXT" HeaderText="テキスト1" ItemStyle-Width="100">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST2_TXT" HeaderText="テキスト2" ItemStyle-Width="100">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST1_SRY" HeaderText="数量1" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="HYMST1_DATE" HeaderText="日付1" ItemStyle-Width="60">
                </asp:BoundField>
            </Columns>
    </asp:GridView>
    <asp:ObjectDataSource id="odsHanyo" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount" typename="AstMaster.AST_M_HANYO" EnablePaging=True >
    </asp:ObjectDataSource>
    <asp:Label runat="server" ID="lblState" CssClass="ppp-msg success"></asp:Label>
    </asp:Panel>
    <br style="clear:both"><br/><br/>
</asp:Content>
<asp:Content ID="SEndDeclare" ContentPlaceHolderID="pppSEndDeclare" Runat="Server" ClientIDMode=Static>
</asp:Content>

