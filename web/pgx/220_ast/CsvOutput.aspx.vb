﻿Imports Gears
Imports System.Data
Imports System.Data.Common
Imports System.Web.UI
Imports System.IO
Imports Ionic.Zip
Imports AstMaster

''' ------------------------------------------------------------------------------
''' Project     : 220_ast
''' Class       : _220_ast_CsvOutput
''' 
''' -------------------------------------------------------------------------------
''' <summary>
''' データ取り込み指示画面
''' </summary>
''' <remarks>
''' 指定されたＣＳＶファイルを出力する
''' </remarks>
''' <hisotry>
'''  [K.Sakamoto] 2012/05/16 Created
'''  [K.Sakamoto] 2013/06/24 販売CSV(出力区分)に月次(計画のみ）最新月次(計画のみ）を追加
'''  [K.Sakamoto] 2013/12/03 予算バージョンは選択させず、常に最新バージョンを出力する
'''  [K.Sakamoto] 2013/12/13 OrderByを指定できるようにする
''' </hisotry>
''' -------------------------------------------------------------------------------

Partial Class _220_ast_CsvOutput
    Inherits GearsPage

    Private objDB As DBExecution = Nothing      'DBアクセス
    Private objZip As ZipFile = Nothing
    Private ms As MemoryStream
    Private execTimeOut As Integer = 180
    Private CSVTYPE_COUNT As Integer = 5

    'プロパティオーバーライド
    Private _connectionName As String = ""
    Public Property ConnectionName As String
        Get
            Return Master.ConnectionName
        End Get
        Set(ByVal value As String)
            Master.ConnectionName = value
        End Set
    End Property

#Region "イベント"
#Region "ページ"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' ページ初期化
    ''' </summary>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <remarks></remarks>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        '使用するデータベーススキーマ設定
        'DBExecution(データベース種別(OracleとかSqlServerとか), DB接続用の文字列)
        objDB = New DBExecution(ConnectionName)

        registerMyControl(pnlGFilter)

        '存在する場合、ダウンロード時間を取得
        Dim downloadTime As String = Trim(ConfigurationManager.AppSettings("DownloadTimeout"))
        If Not downloadTime Is Nothing AndAlso Not String.IsNullOrEmpty(downloadTime) Then
            Integer.TryParse(downloadTime, execTimeOut)
        End If

    End Sub
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' ページロード(ここでは主にコントロール間の関連を登録しておく)
    ''' </summary>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <remarks></remarks>
    ''' -------------------------------------------------------------------------------   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMsg.Text = ""

        '関連を定義（プラント→モデル区分）
        addRelation(ddlPLNT_CD, MDL_KBN.getControl(Of DropDownList))

        If Not IsPostBack Then
            'デフォルトセット
            txtNENDO.Text = DateTime.Now.Year   '年度は当年
            rblPLAN_YM.SelectedIndex = 0

            For i = 1 To CSVTYPE_COUNT
                Dim objRdb As RadioButton = CType(pnlCsvType.FindControl("rdbCSVTYPE_" & i.ToString), RadioButton)
                objRdb.Enabled = False
            Next

            Dim dsCsvType As New CSV_TYPE(ConnectionName)
            Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)
            Dim dtCsvType As DataTable = dsCsvType.gSelect(dto)

            If dtCsvType.Rows.Count <> 0 Then
                For i = 0 To dtCsvType.Rows.Count - 1
                    Dim objRdb As RadioButton = CType(pnlCsvType.FindControl("rdbCSVTYPE_" & dtCsvType.Rows(i).Item("CSV_TYPE").ToString), RadioButton)
                    objRdb.Text = dtCsvType.Rows(i).Item("HYMST1_TXT")
                    objRdb.Enabled = True
                Next
            Else
                pnlCsvType.Visible = False
            End If

        End If

    End Sub
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_LoadComplete
    ''' </summary>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <remarks></remarks>
    ''' ------------------------------------------------------------------------------- 
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        setMsg(executeBehavior(ddlPLNT_CD))

    End Sub
#End Region
#Region "ドロップダウン"
    ''' ------------------------------------------------------------------------------- 
    ''' <summary>
    ''' プラントコードSelectedIndexChanged
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' ------------------------------------------------------------------------------- 
    Protected Sub ddlPLNT_CD_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPLNT_CD.SelectedIndexChanged
        setMsg(executeBehavior(ddlPLNT_CD))
    End Sub
#End Region
#Region "ボタン"
    ''' ------------------------------------------------------------------------------- 
    ''' <summary>
    ''' ダウンロードボタンクリック
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' ------------------------------------------------------------------------------- 
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click

        Dim strMdlKbn As String = String.Empty
        Dim strCsvType As String = String.Empty '出力区分(1:通年予算,2:下期予算,3:月次(計画のみ),4:最新月次(計画のみ),5:最新月次(受注込み))
        Dim strCsvTypeNm As String = String.Empty
        Dim strNendo As String = String.Empty
        Dim strGetsujiYm As String = String.Empty
        Dim strYosanVer As String = String.Empty
        Dim strDataSbt As String = String.Empty

        lblMsg.Text = ""
        If Not isValidateOk(pnlGFilter) Then
            setMsg(False)
            Exit Sub
        End If

        '条件を抽出
        'モデル区分
        If CType(MDL_KBN.getControl(), DropDownList).SelectedIndex <> -1 Then
            strMdlKbn = MDL_KBN.getValue
        Else
            lblMsg.Text = "モデル区分を選択してください"
            Exit Sub
        End If

        '出力区分(1:通年予算,2:下期予算,3:月次(計画のみ),4:最新月次(計画のみ),5:最新月次(受注込み))
        If pnlCsvType.Visible Then

            Dim dsCsvType As New CSV_TYPE(ConnectionName)
            Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)

            For i = 1 To CSVTYPE_COUNT
                Dim objRdb As RadioButton = CType(pnlCsvType.FindControl("rdbCSVTYPE_" & i.ToString), RadioButton)
                If objRdb.Checked = True Then
                    strCsvType = i.ToString
                    strCsvTypeNm = objRdb.Text

                    'データ種別を取得
                    dto.addFilter(SqlBuilder.newFilter("CSV_TYPE").eq(strCsvType))
                    Dim dtCsvType As DataTable = dsCsvType.gSelect(dto)
                    Try
                        strDataSbt = dtCsvType.Rows(0).Item("DATA_SBT").ToString()
                    Catch ex As Exception
                        lblMsg.Text = "汎用マスタの設定が誤っています"
                        Exit Sub
                    End Try
                End If
            Next
            If strCsvType = "" Then
                lblMsg.Text = "出力区分を選択してください"
                Exit Sub
            End If
        End If

        '年度
        If txtNENDO.Visible Then
            strNendo = txtNENDO.Text
            strYosanVer = ddlYOSAN_VER.SelectedValue
            If (strCsvType = "1" Or strCsvType = "2") And strNendo = "" Then
                lblMsg.Text = "予算の場合、年度を入力してください"
                Exit Sub
            End If
        End If

        If pnlCsvType.Visible Then
            '月次開始年月
            Dim dbExe As New DBExecution(GearsSqlExecutor.getDBType(ConnectionName), GearsSqlExecutor.getConnection(ConnectionName))
            dbExe.addResultParam()
            Dim dteToday As DateTime = dbExe.executeDBFunction(Of DateTime)("AST_GET_TODAY")

            If rblPLAN_YM.SelectedValue = "1" Then
                strGetsujiYm = dteToday.AddMonths(-1).ToString("yyyyMM")     '前月
            Else
                strGetsujiYm = dteToday.ToString("yyyyMM")                   '当月
            End If
        End If

        '対象データ(HNB:販売,JSY:自消,MST:マスタ,ZAI:在庫)
        If CType(CSV_KBN.getControl(), CheckBoxList).SelectedIndex = -1 Then
            lblMsg.Text = "対象データを選択してください"
            Exit Sub
        End If

        'ZIPFileを準備
        objZip = New ZipFile(Encoding.GetEncoding("shift_jis"))

        Try
            For Each litem As ListItem In CType(CSV_KBN.getControl(), CheckBoxList).Items

                If litem.Selected Then
                    '出力処理
                    OutputCsv(strMdlKbn, strCsvType, litem.Value, strNendo, strGetsujiYm, strYosanVer, strDataSbt)

                End If
            Next

            Dim strZipNm As String = strMdlKbn & "_" + strCsvTypeNm + "_" + Now.ToString("yyyyMMdd") + "-" + Now.ToString("HHmmss") + ".zip"

            Response.ContentType = "application/zip"
            Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}", strZipNm))
            Response.Clear()
            objZip.Save(Response.OutputStream)
            Response.End()

        Catch ex As Exception
            lblMsg.Text = ex.Message

        Finally
            If objZip IsNot Nothing Then objZip = Nothing

        End Try

    End Sub
#End Region
#Region "その他関数"
    ''' ------------------------------------------------------------------------------- 
    ''' <summary>
    ''' CSVへデータを出力する
    ''' </summary>
    ''' <param name="reader"></param>
    ''' <param name="counter"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' ------------------------------------------------------------------------------- 
    Private Function fetchCsv(ByVal reader As DbDataReader, ByVal counter As Long) As String

        Dim field As String = String.Empty
        Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")

        For i = 0 To reader.FieldCount - 1
            field += Replace(DBExecution.readItemSafe(reader, i), ",", ";") 'カンマを置換
            field += ","
        Next
        field = Left(field, field.Length - 1)       '最後のカンマを除去

        Dim bs As Byte() = enc.GetBytes(field)
        ms.Write(bs, 0, bs.Length)

        ms.Write(enc.GetBytes(ControlChars.Cr + ControlChars.Lf), 0, (ControlChars.Cr + ControlChars.Lf).Length)        '改行

        Return ""

    End Function

    ''' ------------------------------------------------------------------------------- 
    ''' <summary>
    ''' CSV出力処理
    ''' </summary>
    ''' <param name="argMdl">モデル区分</param>
    ''' <param name="argType">出力区分(1:通年予算,2:下期予算,3:月次(計画のみ),4:最新月次(計画のみ),5:最新月次(受注込み))</param>
    ''' <param name="argKbn">対象データ(HNB:販売,JSY:自消,MST:マスタ,ZAI:在庫)</param>
    ''' <param name="argNendo">年度</param>
    ''' <param name="argGetsujiYm">月次開始年月</param>
    ''' <param name="argYosanVer">予算バージョン</param>
    ''' <param name="argDataSbt">データ種別(1:通年予算,2:下期予算,3:月次,4:最新月次)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' ------------------------------------------------------------------------------- 
    Private Function OutputCsv(ByVal argMdl As String, ByVal argType As String, ByVal argKbn As String,
                                ByVal argNendo As String, ByVal argGetsujiYm As String, ByVal argYosanVer As String, ByVal argDataSbt As String) As Boolean

        Dim strCsvNm As String = ""     'CSVファイル名
        Dim strHeader As String = ""    'ヘッダー
        Dim strSql As String = ""       'SQL文
        Dim strProc As String = ""      '前処理ファンクション
        Dim strProcId As String = ""    '前処理ファンクションから取得した処理ID
        Dim sqlb As SqlBuilder = Nothing
        Dim rdm As New Random

        '◆CSV情報をテーブルから取得
        Dim dsCsv As New AST_V_CSVINFO(ConnectionName)
        Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)

        dto.addFilter(SqlBuilder.newFilter("CSV_KBN").eq(argKbn))
        dto.addFilter(SqlBuilder.newFilter("CSV_TYPE").eq(argType))
        dto.addFilter(SqlBuilder.newFilter("CSV_TYPE").eq("-"))

        Dim dtCsv As DataTable = dsCsv.gSelect(dto)

        '取得したデータを元にCSV出力
        If dtCsv.Rows.Count <> 0 Then

            For i = 0 To dtCsv.Rows.Count - 1

                '■初回および前回とファイル名が異なる場合は、新しいファイル出力の準備
                If strCsvNm <> dtCsv.Rows(i).Item("CSV_NM") Then

                    '--該当のCSVファイル用のSQLBuilderを作成
                    Dim localDto As GearsDTO = New GearsDTO(ActionType.SEL)
                    sqlb = MakeSqlb(dtCsv.Rows(i).Item("CSV_NM"), argType, localDto)

                    '--変数初期化
                    strCsvNm = dtCsv.Rows(i).Item("CSV_NM")
                    strProc = dtCsv.Rows(i).Item("PRE_PROC").ToString
                    strHeader = ""
                    strSql = ""
                    strProcId = Now.ToString("dd") + Now.ToString("HHmmss") + rdm.Next(10, 99).ToString '10桁の数値
                End If

                '■ファイル出力用のヘッダーとSELECT項目を作る
                If dtCsv.Rows(i).Item("SORT_NO") > 0 Then
                    'SORT_NOが指定されている項目だけ抽出項目
                    strHeader += dtCsv.Rows(i).Item("HEADER_NM") & ","
                    sqlb.addSelection(SqlBuilder.newSelect(dtCsv.Rows(i).Item("COL_NM")))
                Else
                    sqlb.addSelection(SqlBuilder.newSelect(dtCsv.Rows(i).Item("COL_NM")).isNoSelect)
                End If


                '■CSV情報の次回読み込みファイル名が異なる or CSV情報の最後 の場合は、SQLを実行し、CSVを出力する
                If (i + 1 < dtCsv.Rows.Count - 1 AndAlso dtCsv.Rows(i + 1).Item("CSV_NM") <> dtCsv.Rows(i).Item("CSV_NM")) Or i = dtCsv.Rows.Count - 1 Then

                    '--前処理ファンクションがある場合は実行
                    If strProc <> "" Then

                        Dim dbExe As New DBExecution(ConnectionName)
                        dbExe.CommandTimeout = execTimeOut

                        Dim strParam As String() = Split(dtCsv.Rows(i).Item("PRE_PROC_PARM").ToString, ";")
                        If strParam.Length > 0 Then
                            For k = 0 To strParam.Length - 1
                                Select Case strParam(k)
                                    Case "MDL_KBN"
                                        dbExe.addFilter("IN_MDLKBN", argMdl)
                                    Case "DATASBT"
                                        dbExe.addFilter("IN_DATASBT", argDataSbt)
                                    Case "NENDO"
                                        dbExe.addFilter("IN_YEAR", argNendo)
                                    Case "YOSAN_VER"
                                        dbExe.addFilter("IN_VERSION", argYosanVer)  '予算バージョンは渡すが使わない。常に最新を出力する
                                    Case "YM"
                                        dbExe.addFilter("IN_STARTYM", argGetsujiYm)
                                    Case "PROC_ID"
                                        dbExe.addFilter("IN_PROC_ID", strProcId)
                                End Select
                            Next
                        End If

                        dbExe.executeDBFunction(Of String)(strProc)
                        If dbExe.getErrorMsg <> "" Then
                            'ファンクション内で何かエラーが起きている場合
                            Throw New Exception(strProc + "(" + argMdl + "," + argDataSbt + "," + argNendo + "," + argYosanVer + "," + argGetsujiYm + "," + strProcId + ")実行時エラー " + dbExe.getErrorMsg)
                            Return False
                        End If

                    End If

                    '--WHERE句の生成
                    Dim strWhereCols As String() = Split(dtCsv.Rows(i).Item("WHERE_COLS").ToString, ";")

                    If strWhereCols.Length > 0 Then
                        For k = 0 To strWhereCols.Length - 1
                            Select Case strWhereCols(k)
                                Case "MDL_KBN"
                                    sqlb.addFilter(SqlBuilder.newFilter("MDL_KBN").eq(argMdl))
                                    sqlb.addFilter(SqlBuilder.newFilter("MDL_KBN").eq(Nothing))       'NULLも追加
                                Case "DATASBT"
                                    sqlb.addFilter(SqlBuilder.newFilter("DATASBT").eq(argDataSbt))
                                Case "NENDO"
                                    sqlb.addFilter(SqlBuilder.newFilter("NENDO").eq(argNendo))
                                Case "YOSAN_VER"
                                    sqlb.addFilter(SqlBuilder.newFilter("YOSAN_VER").eq(argYosanVer))
                                Case "YM"
                                    sqlb.addFilter(SqlBuilder.newFilter("YM").eq(argGetsujiYm))
                                Case "PROC_ID"
                                    sqlb.addFilter(SqlBuilder.newFilter("PROC_ID").eq(strProcId))
                            End Select
                        Next
                    End If

                    '--MemoryStream準備
                    ms = New MemoryStream

                    '--ヘッダー出力
                    Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("Shift_JIS")
                    Dim bs_h As Byte() = enc.GetBytes(Left(strHeader, strHeader.Length - 1))    '最後のカンマを除去
                    ms.Write(bs_h, 0, bs_h.Length)
                    ms.Write(enc.GetBytes(ControlChars.Cr + ControlChars.Lf), 0, (ControlChars.Cr + ControlChars.Lf).Length)

                    '--明細出力
                    Try
                        Dim params As New Dictionary(Of String, String)
                        strSql = sqlb.makeSql(params, ActionType.SEL)   'SQL文を取得

                        '--Orderの生成
                        Dim orderList As List(Of String) = New List(Of String)
                        For Each q In From dr In dtCsv.AsEnumerable() _
                                      Select dr
                                      Where dr.Item("ORDER_NO") > 0 And dr.Item("CSV_NM") = strCsvNm
                                      Order By dr.Item("ORDER_NO") Ascending
                            orderList.Add(q.Field(Of String)("COL_NM"))
                        Next
                        If Not IsNothing(orderList) Then
                            strSql += " ORDER BY " + String.Join(",", orderList)
                        End If

                        objDB.addFilter(params)     'パラメータをセットしてフィルタ実行
                        objDB.sqlReadToString(strSql, AddressOf Me.fetchCsv)

                    Catch ex As Exception
                        lblMsg.Text = ex.Message

                    Finally
                        '閉じる
                        objZip.AddEntry(strCsvNm & ".csv", ms.ToArray)
                        ms.Close()
                        sqlb = Nothing
                    End Try

                End If

            Next

        End If

        Return True

    End Function

    ''' ------------------------------------------------------------------------------- 
    ''' <summary>
    ''' 各ファイル用のSqlBuilder生成
    ''' </summary>
    ''' <param name="argFileNm">ファイル名</param>
    ''' <param name="argType">出力区分(月次とか予算とか)</param>
    ''' <param name="dto">GearsDTO</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' ------------------------------------------------------------------------------- 
    Private Function MakeSqlb(ByVal argFileNm As String, ByVal argType As String, ByVal dto As GearsDTO) As SqlBuilder

        Dim sqlb As SqlBuilder = Nothing

        Select Case argFileNm
            Case "demand"       '販売実績
                If argType = "1" Or argType = "2" Then
                    sqlb = New AST_V_CSV_DEMAND_Y(ConnectionName).makeSqlBuilder(dto)       '通年予算・下期予算
                ElseIf argType = "5" Then
                    sqlb = New AST_V_CSV_DEMAND_G(ConnectionName).makeSqlBuilder(dto)       '最新月次(実績込み) 
                Else
                    sqlb = New AST_V_CSV_DEMAND_GK(ConnectionName).makeSqlBuilder(dto)      '月次・最新月次(計画のみ) 
                End If
            Case "inventory"    '在庫操作
                If argType = "1" Or argType = "2" Then
                    sqlb = New AST_V_CSV_INVENTORY_Y(ConnectionName).makeSqlBuilder(dto)    '通年予算・下期予算
                Else
                    sqlb = New AST_V_CSV_INVENTORY_G(ConnectionName).makeSqlBuilder(dto)    '月次・最新月次
                End If
            Case "jisyo"        '自消
                If argType = "1" Or argType = "2" Then
                    sqlb = New AST_V_CSV_JISHO_Y(ConnectionName).makeSqlBuilder(dto)        '通年予算・下期予算
                Else
                    sqlb = New AST_V_CSV_JISHO_G(ConnectionName).makeSqlBuilder(dto)        '月次・最新月次
                End If
            Case "item"         '品目
                sqlb = New AST_V_CSV_ITEM(ConnectionName).makeSqlBuilder(dto)
            Case "proc"         '工程
                sqlb = New AST_V_CSV_PROC(ConnectionName).makeSqlBuilder(dto)
            Case "graph"        '工程グラフ
                sqlb = New AST_V_CSV_GRAPH(ConnectionName).makeSqlBuilder(dto)
            Case "link"         '工程リンク
                sqlb = New AST_V_CSV_LINK(ConnectionName).makeSqlBuilder(dto)
            Case "resource"     '資源
                sqlb = New AST_V_CSV_RESOURCE(ConnectionName).makeSqlBuilder(dto)
            Case "bor"          '資源表
                sqlb = New AST_V_CSV_BOR(ConnectionName).makeSqlBuilder(dto)
        End Select

        Return sqlb

    End Function

#End Region
#End Region
    ''' ------------------------------------------------------------------------------- 
    ''' <summary>
    ''' メッセージ出力
    ''' </summary>
    ''' <param name="success"></param>
    ''' <remarks></remarks>
    ''' ------------------------------------------------------------------------------- 
    Private Sub setMsg(ByVal success As Boolean)
        If Not success And getLogCount() > 0 Then
            If getLogMsgFirst.getMsgDebug = "" Then
                lblMsg.Text = getLogMsgFirst.Message
            Else
                lblMsg.Text = getLogMsgFirst.Message + " 詳細：" + getLogMsgFirst.getMsgDebug
            End If
        End If

    End Sub

End Class
