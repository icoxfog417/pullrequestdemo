﻿<%@ Page Language="VB" MasterPageFile="astMaster.master" AutoEventWireup="false" CodeFile="CsvOutput.aspx.vb" Inherits="_220_ast_CsvOutput" %>
<%@ MasterType VirtualPath="astMaster.master" %>
<%@ Register Src="../parts/UnitItem.ascx" TagName="unitItem" TagPrefix="ui" %>

<asp:Content ID="clientPageTitle" ContentPlaceHolderID="pppPageTitle" runat="Server" ClientIDMode="Static">
   ＣＳＶファイル出力
</asp:Content>
<asp:Content ID="clientHead" ContentPlaceHolderID="pppHead" runat="Server" ClientIDMode="Static">
    <!-- headのスクリプト処理箇所。必要なJavaScript処理などがあればここに記載-->
    <title>Ｓ-Ｎａｖｉ＋　ＣＳＶファイル出力</title>
    <!-- 2013/08/23 K.Takahashi ダウンロードのポップアップに「開く」ボタンを表示しないようにする -->
    <meta name="DownloadOptions" content="noopen" />
    <script type="text/javascript" language="javascript">
    </script>
</asp:Content>


<asp:Content ID="clientCenter" ContentPlaceHolderID="pppContent" runat="Server" ClientIDMode="Static">
    <!--メイン画面-->
    <asp:Panel ID="updArea" class="ppp-switch-area" runat="server">
        <!--検索条件-->
        <asp:Panel ID="pnlGFilter" runat="server" CssClass="ppp-include-combo-area" Style="margin: 0px 0px 0px 30px;">
           
            <ui:unitItem ID="PLNT_CD" runat="server" LabelText="プラント"/>
            <asp:DropDownList id="ddlPLNT_CD" runat="server" AutoPostBack=True CssClass="gs-layout-ctrl" ></asp:DropDownList>
            <%= UnitItem.closing%>
            <ui:unitItem ID="MDL_KBN" CtlKind="DDL" runat="server" LabelText="モデル区分" />
            <br style="clear: both" />
            <br style="clear: both" />
            <ui:unitItem ID="CSV_KBN" CtlKind="CBL" runat="server" LabelText="対象データ" DefaultCheckIndex=0 Width="300" />
            <br style="clear: both" />
            <br style="clear: both" />
            <asp:Panel ID="pnlCsvType" runat="server" >
                <table class="ppp-table" cellspacing="0" border="1" style="border-collapse: collapse;">
                    <tr class="ppp-table-head">
                        <th style="font-weight: bold;">出力区分</th>
                        <th style="font-weight: bold;">範囲</th>
                    </tr>
                    <tr>
                        <td height="25"><asp:RadioButton ID="rdbCSVTYPE_3" runat="server" GroupName="CSV_TYPE" Text="月次(計画のみ)" Width="180" Checked=true></asp:RadioButton></td>
                        <td style="text-align: center;border-bottom:none;">
                            <asp:RadioButtonList id="rblPLAN_YM" RepeatDirection="Horizontal" runat="server" CssClass="gs-layout-ctrl" Width="400" ></asp:RadioButtonList>
                        </td>

                    </tr>
                    <tr>
                        <td height="25"><asp:RadioButton ID="rdbCSVTYPE_4" runat="server" GroupName="CSV_TYPE" Text="最新月次(計画のみ)" Width="180" ></asp:RadioButton></td>
                        <td rowspan="2" style="border-top:none;">
                            <asp:Label style="margin: 10px;" ID="Label3" runat="server" Text="※「当月」選択時、計画入力期間中の場合は入力中の値が出力されます。" /><br />
                            <asp:Label style="margin: 10px;" ID="Label4" runat="server" Text="※「受注込み」は、受注＞計画の場合は受注数量が出力されます。" />
                        </td>
                    </tr>
                    <tr>
                        <td height="25"><asp:RadioButton ID="rdbCSVTYPE_5" runat="server" GroupName="CSV_TYPE" Text="最新月次(受注込み)" Width="180" ></asp:RadioButton></td>
                    </tr>
                    <tr>
                        <td height="25"><asp:RadioButton ID="rdbCSVTYPE_1" runat="server" GroupName="CSV_TYPE" Text="年初予算" Width="180" ></asp:RadioButton></td>
                        <td height="25" style="text-align: center;">
                            <asp:Label ID="Label1" runat="server" Text="予算年度" />
                            <asp:TextBox id="txtNENDO" runat="server" MaxLength=4 Width=40 CssClass="gears-GDate_Format_yyyy" ></asp:TextBox>
                            <asp:Label style="margin: 10px;" ID="Label6" runat="server" Text="※予算入力期間中は入力中の値が出力されます。" /><br />
                            <asp:Label ID="Label2" runat="server" Text="バージョン" Visible="false" />
                            <asp:DropDownList id="ddlYOSAN_VER" runat="server" CssClass="gs-layout-ctrl" Width="100" Visible="false" ></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td height="25"><asp:RadioButton ID="rdbCSVTYPE_2" runat="server" GroupName="CSV_TYPE" Text="下期計画" Width="180" ></asp:RadioButton></td>
                        <td height="25"><asp:Label style="margin: 10px;" ID="Label5" runat="server" Text="※当年度分です。入力期間中は入力中の値が出力されます。" /></td>
                    </tr>
                </table>
            </asp:Panel>
            <br style="clear: both" />
            <br style="clear: both" />
            <asp:Button ID="btnDownload" runat="server" Text="ダウンロード" />
            <br />
            <br />
        </asp:Panel>
    </asp:Panel>
    <!--表示-->
    <!--エラーメッセージ-->
    <asp:Label ID="lblMsg" runat="server" Text="" CssClass="ppp-msg error"></asp:Label>
    <asp:Label ID="lblLog" runat="server" Text="" CssClass="ppp-msg"></asp:Label>

</asp:Content>
