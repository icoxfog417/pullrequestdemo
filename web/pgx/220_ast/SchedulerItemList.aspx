﻿<%@ Page Title="Ｓ-Ｎａｖｉ＋　品目一覧" Language="VB" MasterPageFile="~/220_ast/astMaster.master" AutoEventWireup="false" CodeFile="SchedulerItemList.aspx.vb" Inherits="_220_ast_SchedulerItemList" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ MasterType VirtualPath="astMaster.master" %>

<asp:Content ID="SHeader" ContentPlaceHolderID="pppHead" Runat="Server" ClientIDMode=Static>
    <script>
        $(function(){
            //adjustCombobox();
            //adjustComboboxが終わるまで待ってから処理

            $(".ppp-switch-area").hide(); //画面ゆれ(処理が終わるまで上が出たり消えたりする)を防ぐために、一旦隠す
            <%If IsDisplayUpper Then %>
                makePPPSwitchArea(0);
            <%Else %>
                makePPPSwitchArea(-1);
            <%End IF %>
            $(".ppp-switch-area").show();

            $(".link-field").click(function(){ 
                window.open($(this).attr("href"),$(this).attr("target")).focus();
             })
        })
    </script>
    <script type="text/javascript" language="javascript">
        function clearMsg() {
            // ボタンをクリックされた際にエラーメッセージをクリアする。
            document.getElementById("lblMsg").innerHTML = "";
        }
    </script>

     <style>
       .liquid-list-area
      {
       min-width:750px;
       max-width:1250px;
       width: expression(this.width > 1024 ? this.width : '1150px'); /* IE用 */
      }
      
      .liquid-list-area .lq-float
      {
        padding:0px 0px 10px 0px;
        overflow:auto;   
      }
     </style>
</asp:Content>

<asp:Content ID="SPreDeclare" ContentPlaceHolderID="pppPreDeclare" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="SPageTitle" ContentPlaceHolderID="pppPageTitle" Runat="Server" ClientIDMode=Static>
スケジューラ品目一覧
</asp:Content>
<asp:Content ID="SHeaderContent" ContentPlaceHolderID="pppHeader" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="pppSContent" ContentPlaceHolderID="pppContent" Runat="Server" ClientIDMode=Static>
    <asp:Panel id="SwitchingArea" class="ppp-switch-area" runat="server">
    <h3 class="ppp-switch-header">
        検索条件
    </h3>
    <asp:Panel id="UpperPanel" runat="server" CssClass="ppp-include-combo-area" >
       <asp:Panel id="pnlGFILTER" runat="server" Width=970>

            <ui:unitItem ID="HIN_CD" CtlKind="TXT" runat="server" LabelText="品目コード" Operator="START_WITH" style="text-transform:uppercase"/>
            <ui:unitItem ID="HIN_TXT" CtlKind="TXT" runat="server" LabelText="品目テキスト" Operator="LIKE" />
            <ui:unitItem ID="SCHEDULER_FLG" CtlKind="RBL" runat="server" LabelText="ＳＣ対象" IsNeedAll=True  DefaultCheckIndex=1/>
            <ui:unitItem ID="CHUI_FLG" CtlKind="RBL" runat="server" LabelText="注意銘柄" IsNeedAll=True DefaultCheckIndex=0/>
            <ui:unitItem ID="MK_FLG" CtlKind="RBL" runat="server" LabelText="無効フラグ" IsNeedAll=True DefaultCheckIndex=1/>
            <br style="clear:both">

            <ui:unitItem ID="VKORG" CtlKind="DDL" runat="server" LabelText="販売組織" IsNeedAll=True AutoPostBack=True/>
            <ui:unitItem ID="HIN_KBN" CtlKind="DDL" runat="server" LabelText="品目区分" IsNeedAll=True />
            <ui:unitItem ID="DHYO_MDL_KBN" CtlKind="DDL" runat="server" LabelText="代表モデル区分" IsNeedAll=True AutoPostBack=True />

            <asp:UpdatePanel id="udpSOUCHI_CD" runat="server" UpdateMode=Conditional>
                <ContentTemplate>
                    <ui:unitItem ID="DHYO_SOUCHI_CD" CtlKind="DDL" runat="server" LabelText="代表装置" IsNeedAll=True />
                </ContentTemplate>
            </asp:UpdatePanel>

            <br style="clear:both">
            <ui:unitItem ID="EXHIN_KBN" CtlKind="DDL" runat="server" LabelText="例外品目区分" IsNeedAll=True />
            <ui:unitItem ID="HBAN_KBN" CtlKind="DDL" runat="server" LabelText="廃盤区分" IsNeedAll=True />
            <ui:unitItem ID="JYAKUSHO_KBN" CtlKind="DDL" runat="server" LabelText="弱小銘柄区分" IsNeedAll=True />
            <ui:unitItem ID="KAKUGAI_KBN" CtlKind="DDL" runat="server" LabelText="格外銘柄区分" IsNeedAll=True />
            <br style="clear:both">

            <ui:unitItem ID="HIN_CD_OLD" CtlKind="TXT" runat="server" LabelText="旧品目コード" Operator="LIKE" AuthorizationAllow="AST_MDP01" RoleEvalAction="VISIBLE" />
            <ui:unitItem ID="HINKAISO_CD" CtlKind="TXT" runat="server" LabelText="品目階層" Operator="LIKE" AuthorizationAllow="AST_MDP01" RoleEvalAction="VISIBLE"/>
            <ui:unitItem ID="HING_TXT" CtlKind="TXT" runat="server" LabelText="銘柄名称" Operator="LIKE" AuthorizationAllow="AST_PRM01" RoleEvalAction="VISIBLE"/>

            <asp:UpdatePanel id="udpHING1_CD" runat="server" UpdateMode=Conditional>
                <ContentTemplate>
                    <ui:unitItem ID="HING1_CD" CtlKind="DDL" runat="server" LabelText="品目グループ１" IsNeedAll=True AuthorizationAllow="AST_PRM01" RoleEvalAction="VISIBLE"/>
                </ContentTemplate>
            </asp:UpdatePanel>
            <ui:unitItem ID="DOK_ZOK5" CtlKind="TXT" runat="server" LabelText="独自属性５" Operator="LIKE" />
       
            <%-- 
           <ui:unitItem ID="SORT_KIND" runat="server" LabelText="ソート順" />
            <asp:RadioButtonList ID="dSORT_KIND" runat="server" RepeatDirection=Horizontal>
                <asp:ListItem Value=0 Text="品目コード順" Selected />
                <asp:ListItem Value=1 Text="品目コード・親子関係順" />
            </asp:RadioButtonList>
           <%= UnitItem.closing%>
           --%>
           <ui:unitItem ID="SORT_KIND" CtlKind="RBL" runat="server" LabelText="ソート順" Width="260" DefaultCheckIndex=0 />

            <br style="clear:both">    
        </asp:Panel>
	    <br/>
        <asp:Button id="btnSearch" runat="server" Width="105" Text="検索" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button id="btnListOut" runat="server" Width="105" Text="Excel出力" />
    </asp:Panel>
  </asp:Panel>

    <div class="liquid-list-area" style="margin:10px 0px 0px 10px;"> 
    <asp:Panel ID="pnlFloat" runat="server" CssClass="lq-float" Style="display:none"> <%-- 初回非表示 --%>
    <asp:GridView id="grvHin" runat="server" CssClass="ppp-table" HeaderStyle-CssClass=ppp-table-head 
        EnableViewState=False DataSourceID=odsHin
        PageSize=20 AllowPaging=True AutoGenerateColumns=False>
            <Columns>
                <asp:HyperLinkField Text="詳細" DataNavigateUrlFields="HIN_CD,HIN_KBN" DataNavigateUrlFormatString="SchedulerItemDashBoard.aspx?hin_cd={0}&hin_kbn={1}" Target="scDetail" ControlStyle-CssClass="link-field">
                </asp:HyperLinkField>
                <asp:BoundField DataField="HIN_KBN_TXT" HeaderText="品目区分" ItemStyle-Width="60">
                </asp:BoundField>

                <asp:TemplateField HeaderText="品目コード">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkTo" runat="server" Target="scEdit" 
                            NavigateUrl=<%# AstMaster.Common.AstMasterNavigation.makeLink(Container) %>
                            Text=<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>
                            ToolTip=<%# DataBinder.Eval(Container.DataItem, "HIN_TXT")%>
                         CssClass="link-field" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="HING1_TXT" HeaderText="品目グループ１略称" ItemStyle-CssClass="ast-prm01-only table-cell" HeaderStyle-CssClass="ast-prm01-only table-cell" ItemStyle-Width="80">
                </asp:BoundField>

                <asp:BoundField DataField="HIN_CD_OLD" HeaderText="旧品目コード" ItemStyle-Width="150" ItemStyle-CssClass="ast-mdp01-only table-cell" HeaderStyle-CssClass="ast-mdp01-only table-cell" >
                </asp:BoundField>
                <asp:BoundField DataField="HINKAISO_CD" HeaderText="品目階層"  ItemStyle-Width="120" ItemStyle-CssClass="ast-mdp01-only table-cell" HeaderStyle-CssClass="ast-mdp01-only table-cell" >
                </asp:BoundField>

                <asp:TemplateField HeaderText="ＳＣ対象" ItemStyle-HorizontalAlign=Center ItemStyle-Width="40">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSCHEDULER_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "SCHEDULER_FLG")="1",True,False) %>  Enabled=False/>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="DHYO_MDL_KBN_TXT" HeaderText="代表モデル" ItemStyle-Width="40">
                </asp:BoundField>
                <asp:BoundField DataField="SPART" HeaderText="製品部門" ItemStyle-Width="40">
                </asp:BoundField>
                <asp:BoundField DataField="PARENT_HIN_CD" HeaderText="親銘柄">
                </asp:BoundField>
                <asp:BoundField DataField="PARENT_HIN_TXT" HeaderText="親銘柄名称">
                </asp:BoundField>
                <asp:BoundField DataField="DHYO_SOUCHI_CD" HeaderText="代表生産装置" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="MIN_HOJU_SRY" HeaderText="最小補充" ItemStyle-Width="40" DataFormatString="{0:#}" ItemStyle-HorizontalAlign=Right>
                </asp:BoundField>
                <asp:BoundField DataField="ADD_HOJU_SRY" HeaderText="増分補充" ItemStyle-Width="40" DataFormatString="{0:#}" ItemStyle-HorizontalAlign=Right>
                </asp:BoundField>
                <asp:BoundField DataField="HIN_SORT" HeaderText="ソート順" ItemStyle-Width="40">
                </asp:BoundField>
                <asp:BoundField DataField="KAKUGAI_KBN_TXT" HeaderText="格外銘柄" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="HBAN_KBN_TXT" HeaderText="廃盤銘柄" ItemStyle-Width="60">
                </asp:BoundField>

                <asp:TemplateField HeaderText="無効フラグ" ItemStyle-Width="40" ItemStyle-HorizontalAlign=Center>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkMK_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "MK_FLG")="1",True,False) %> Enabled=False/>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
    </asp:GridView>
    <asp:ObjectDataSource id="odsHin" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount" typename="AstMaster.AST_M_HIN" EnablePaging=True >
    </asp:ObjectDataSource>
    <asp:Label runat="server" ID="lblState" CssClass="ppp-msg success"></asp:Label><br/><br/>

    </asp:Panel>
    </div>
    <asp:Label runat="server" ID="lblMsg" Text="" CssClass="ppp-msg error"></asp:Label>

</asp:Content>



