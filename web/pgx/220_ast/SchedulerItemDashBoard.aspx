﻿<%@ Page Title="Ｓ-Ｎａｖｉ＋　ダッシュボード" Language="VB" MasterPageFile="~/220_ast/astMaster.master" AutoEventWireup="false" CodeFile="SchedulerItemDashBoard.aspx.vb" Inherits="_220_ast_SchedulerItemDashBoard" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ Register src="../parts/220_ast/SCItemSearch.ascx" tagname="ms" tagprefix="search" %>
<%@ MasterType VirtualPath="astMaster.master" %>

<asp:Content ID="Head" ContentPlaceHolderID="pppHead" Runat="Server" ClientIDMode=Static>
    <script>
        $(function () {
            //absolute指定を行うと裏側にコンテンツが回りこんでしまうので、高さを補正するdivを配置する
            //なお、floatを使うと画面を縮めた時に回りこんでしまう。「確実に横に並べ、なおかつコンテンツ領域を確保する」ためにこの方法を使う
            var contentSize = $("#pnlContent").height();
            if ($("#pnlMenu").height() > contentSize) {
                contentSize = $("#pnlMenu").height();
            }

            $("#overAbsoluteContent").css("height", contentSize + "px");
        })
    </script>

</asp:Content>

<asp:Content ID="PreDeclare" ContentPlaceHolderID="pppPreDeclare" Runat="Server" ClientIDMode=Static>
</asp:Content>
<asp:Content ID="PageTitle" ContentPlaceHolderID="pppPageTitle" Runat="Server" ClientIDMode=Static>
スケジューラー品目ダッシュボード
</asp:Content>

<asp:Content ID="Header" ContentPlaceHolderID="pppHeader" Runat="Server" ClientIDMode=Static>

</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="pppContent" Runat="Server" ClientIDMode=Static>
  <%-- ページトップ --%>
  <asp:Panel id="pnlDashBoard" runat="server" style="margin:0px 0px 0px 15px">
    <%-- <asp:Label id="test" runat="server" Text="Label"></asp:Label>--%>
    <asp:Panel id="pnlKey" runat="server" style="width:800px;">
            <ui:unitItem ID="HIN_CD" CtlKind="TXT" runat="server" LabelText="品目コード" AutoPostBack=True SearchAction="SEARCH_HING.openDialog()" style="text-transform:uppercase"/>
        
            <asp:Panel id="pnlKeyAttr" runat="server">
                <ui:unitItem ID="HIN_TXT__GCON" CtlKind="LBL" runat="server" LabelText="品目テキスト" IsEditable=False Width="250" />
                <ui:unitItem ID="HIN_KBN" CtlKind="DDL" runat="server" LabelText="品目区分" IsEditable=False />
                <asp:HiddenField ID="hdnHING_CD" runat="server"  />
                <asp:HiddenField ID="hdnPARENT_HIN_CD" runat="server" />
                <asp:HiddenField ID="hdnJYUGO_GRP_CD" runat="server" />
            </asp:Panel>

            <%-- 
            <asp:UpdatePanel id="udpKeyAttr" runat="server" UpdateMode=Conditional>
                <ContentTemplate>
                </ContentTemplate>
            </asp:UpdatePanel>
           

            <asp:Panel id="pnlJumpButtons" runat="server" style="padding-top:25px">
                    <asp:Button id="btnSortMnt" runat="server"  OnClientClick="window.open('HinSortMnt.aspx?parent_hin_cd=' + escape(getElementById('hdnJYUGO_GRP_CD').value) ,'scSort').focus(); return false;" UseSubmitBehavior=False Text="　ソート順編集　" />
            </asp:Panel>
            --%>
            <asp:Panel id="pnlSearchPanel" runat="server">
                <asp:HiddenField ID="hdnHIN_CD__SEARCH" runat="server" />
                <asp:HiddenField ID="hdnPARENT_HIN_CD__SEARCH" runat="server" />
                <asp:HiddenField ID="hdnHING_CD__SEARCH" runat="server"  />
            </asp:Panel>
            
            <div style="clear:both">
    </asp:Panel>
    

    <%-- メニューカラム --%>
    <div id="contentArea" style="position:relative;">
    <asp:Panel id="pnlMenu" runat="server" style="width:190px;position:absolute;">
        <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">銘柄</h3>

        <asp:GridView id="grvPeletList" runat="server" CssClass=ppp-table HeaderStyle-CssClass=ppp-table-head 
        EnableViewState=False DataSourceID=odsPelet
        PageSize=20 AllowPaging=True AutoGenerateColumns=False DataKeyNames="HIN_CD">
        <AlternatingRowStyle CssClass="ppp-table-even" />
            <Columns>
                <asp:ButtonField CommandName="Select" HeaderText="品目コード" DataTextField=HIN_CD />
                <asp:TemplateField HeaderText="ＳＣ対象" ItemStyle-HorizontalAlign=Center ItemStyle-Width="40">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSCHEDULER_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "SCHEDULER_FLG")="1",True,False) %>  OnClick="return false;"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="無効フラグ" ItemStyle-Width="40" ItemStyle-HorizontalAlign=Center>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkMK_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "MK_FLG")="1",True,False) %> OnClick="return false;"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
			<pagersettings pagebuttoncount="5"/>
        </asp:GridView>
        <asp:ObjectDataSource id="odsPelet" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount" typename="AstMaster.AST_M_HIN" EnablePaging=True >
        </asp:ObjectDataSource>
    </asp:Panel>

     <%-- コンテンツエリア --%>
    <asp:Panel id="pnlContent" runat="server" style="position:absolute;left:200px;">
        
        <asp:Panel id="pnlSeisan" runat="server" >
            <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">生産装置情報</h3>
            <div class="lq-float">
                <asp:GridView id="grvSeisan" runat="server" CssClass=ppp-table HeaderStyle-CssClass=ppp-table-head
                EnableViewState=False AutoGenerateColumns=True>
                <AlternatingRowStyle CssClass="ppp-table-even" />
                </asp:GridView>
            </div>
        </asp:Panel>
        <br/>
        <br/>

        <asp:Panel id="pnlPowder" runat="server" >
            <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">重合グループ情報</h3>

           <asp:GridView id="grvPowder" runat="server" CssClass=ppp-table HeaderStyle-CssClass=ppp-table-head 
            EnableViewState=False AutoGenerateColumns=False>
            <AlternatingRowStyle CssClass="ppp-table-even" />
                <Columns>
                    <%-- 
                    <asp:HyperLinkField Text="詳細" DataNavigateUrlFields="HIN_CD,HIN_KBN" DataNavigateUrlFormatString="SchedulerItemDashBoard.aspx?hin_cd={0}&hin_kbn={1}" Target="scDetail" >
                    </asp:HyperLinkField>
                    --%>
                    <asp:TemplateField HeaderText="品目コード">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkTo" runat="server" Target="_blank" 
                                NavigateUrl=<%# AstMaster.Common.AstMasterNavigation.makeLink(Container) %>
                                Text=<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>
                                ToolTip=<%# DataBinder.Eval(Container.DataItem, "HIN_TXT")%>
                             CssClass=tooltip-on />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ＳＣ対象" ItemStyle-HorizontalAlign=Center ItemStyle-Width="40">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSCHEDULER_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "SCHEDULER_FLG")="1",True,False) %>  OnClick="return false;"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- 
                    <asp:BoundField DataField="DHYO_MDL_KBN_TXT" HeaderText="代表モデル" ItemStyle-Width="40">
                    </asp:BoundField>
                    --%>
                    <asp:BoundField DataField="SPART" HeaderText="製品部門" ItemStyle-Width="40">
                    </asp:BoundField>
                    <%-- 
                    <asp:BoundField DataField="DHYO_SOUCHI_CD" HeaderText="代表生産装置" ItemStyle-Width="60">
                    </asp:BoundField>
                    --%>
                    <asp:BoundField DataField="MIN_HOJU_SRY" HeaderText="最小補充" ItemStyle-Width="40" DataFormatString="{0:#,#}">
                    </asp:BoundField>
                    <asp:BoundField DataField="ADD_HOJU_SRY" HeaderText="増分補充" ItemStyle-Width="40" DataFormatString="{0:#,#}">
                    </asp:BoundField>
                    <asp:BoundField DataField="KAKUGAI_KBN_TXT" HeaderText="格外銘柄" ItemStyle-Width="60">
                    </asp:BoundField>
                    <asp:BoundField DataField="HBAN_KBN_TXT" HeaderText="廃盤銘柄" ItemStyle-Width="60">
                    </asp:BoundField>

                    <asp:TemplateField HeaderText="無効フラグ" ItemStyle-Width="40" ItemStyle-HorizontalAlign=Center>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkMK_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "MK_FLG")="1",True,False) %> OnClick="return false;"/>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        
        </asp:Panel>
        <br/>

        <asp:Panel id="pnlMeigara" runat="server" >
            <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">銘柄情報</h3>

            <div style="width:100%;overflow-x:auto">
                <asp:GridView id="grvPeletDetail" runat="server" CssClass=ppp-table HeaderStyle-CssClass=ppp-table-head 
                EnableViewState=False AutoGenerateColumns=False DataSourceID=odsPelet PageSize=10 AllowPaging=True Width="1050px">
                <AlternatingRowStyle CssClass="ppp-table-even" />
                    <Columns>
                        <asp:TemplateField HeaderText="品目コード">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkTo" runat="server" Target="_blank" 
                                    NavigateUrl=<%# AstMaster.Common.AstMasterNavigation.makeLink(Container) %>
                                    Text=<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>
                                    ToolTip=<%# DataBinder.Eval(Container.DataItem, "HIN_TXT")%>
                                    CssClass=tooltip-on />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="HING1_TXT" HeaderText="品目グループ１略称" ItemStyle-CssClass="ast-prm01-only table-cell" HeaderStyle-CssClass="ast-prm01-only table-cell" ItemStyle-Width="80">
                        </asp:BoundField>

                        <asp:BoundField DataField="HIN_CD_OLD" HeaderText="旧品目コード" ItemStyle-Width="150" ItemStyle-CssClass="ast-mdp01-only table-cell" HeaderStyle-CssClass="ast-mdp01-only table-cell" >
                        </asp:BoundField>
                        <asp:BoundField DataField="HINKAISO_CD" HeaderText="品目階層"  ItemStyle-Width="120" ItemStyle-CssClass="ast-mdp01-only table-cell" HeaderStyle-CssClass="ast-mdp01-only table-cell" >
                        </asp:BoundField>

                        <asp:TemplateField HeaderText="ＳＣ対象" ItemStyle-HorizontalAlign=Center ItemStyle-Width="40">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSCHEDULER_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "SCHEDULER_FLG")="1",True,False) %>  OnClick="return false;"/>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="しわ寄せ" ItemStyle-HorizontalAlign=Center ItemStyle-Width="40">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkINFL_HIN_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "INFL_HIN_FLG")="1",True,False) %>  OnClick="return false;"/>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%-- 
                        <asp:BoundField DataField="DHYO_MDL_KBN_TXT" HeaderText="代表モデル" ItemStyle-Width="40">
                        </asp:BoundField>
                        <asp:BoundField DataField="SPART" HeaderText="製品部門" ItemStyle-Width="40">
                        </asp:BoundField>

                        <asp:BoundField DataField="PARENT_HIN_CD" HeaderText="親銘柄">
                        </asp:BoundField>
                        <asp:BoundField DataField="PARENT_HIN_TXT" HeaderText="親銘柄名称" >
                        </asp:BoundField>
                        <asp:BoundField DataField="DHYO_SOUCHI_CD" HeaderText="代表生産装置" ItemStyle-Width="60">
                        </asp:BoundField>                        
                        --%>
                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhSSN_CYC_NSU" runat="server" Text="生サ<br/>日数" ToolTip="生産サイクル日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSSN_CYC_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "SSN_CYC_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhAZZK_RATE" runat="server" Text="安全在庫率"/>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAZZK_RATE" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "AZZK_RATE","{0:N}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhCYC_HOSEI_NSU" runat="server" Text="サ長" ToolTip="サイクル長周期補正日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCYC_HOSEI_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "CYC_HOSEI_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhHBAI_HDZK_NSU" runat="server" Text="販変" ToolTip="販売変動在庫日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblHBAI_HDZK_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "HBAI_HDZK_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhKNTLDT_HS_NSU" runat="server" Text="検定" ToolTip="検定リードタイム在庫日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblKNTLDT_HS_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "KNTLDT_HS_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhKPRISK_ZK1_NSU" runat="server" Text="欠１" ToolTip="欠品リスク在庫１日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblKPRISK_ZK1_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "KPRISK_ZK1_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhKPRISK_ZK2_NSU" runat="server" Text="欠２" ToolTip="欠品リスク在庫２日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblKPRISK_ZK2_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "KPRISK_ZK2_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right>
                            <HeaderTemplate>
                                <asp:Label ID="lblhHSRSK_ZK_NSU" runat="server" Text="品リ" ToolTip="品質リスク在庫日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblHSRSK_ZK_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "HSRSK_ZK_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign=Right >
                            <HeaderTemplate>
                                <asp:Label ID="lblhOTHER_ZK_NSU" runat="server" Text="そ他" ToolTip="その他在庫補正日数" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblOTHER_ZK_NSU" runat="server" Text=<%# DataBinder.Eval(Container.DataItem, "OTHER_ZK_NSU","{0:#}") %> />
                            </ItemTemplate>
                        </asp:TemplateField>                
                
                        <asp:BoundField DataField="HIN_SORT" HeaderText="ソート順" ItemStyle-Width="40">
                        </asp:BoundField>
                        <asp:BoundField DataField="PRT_SSN_ORDER" HeaderText="生産順" ItemStyle-Width="40">
                        </asp:BoundField>                
                        <asp:BoundField DataField="KAKUGAI_KBN_TXT" HeaderText="格外銘柄" ItemStyle-Width="60">
                        </asp:BoundField>
                        <asp:BoundField DataField="HBAN_KBN_TXT" HeaderText="廃盤銘柄" ItemStyle-Width="60">
                        </asp:BoundField>

                        <asp:TemplateField HeaderText="無効フラグ" ItemStyle-Width="40" ItemStyle-HorizontalAlign=Center>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkMK_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "MK_FLG")="1",True,False) %> OnClick="return false;"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>        
                <br/> 
            </div>


        </asp:Panel>

        <br/>
        <asp:Panel id="pnlNisgata" runat="server" >
            <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">荷姿情報</h3>

            <asp:GridView id="grvNisugata" runat="server" CssClass=ppp-table HeaderStyle-CssClass=ppp-table-head 
            EnableViewState=False AutoGenerateColumns=False>
            <AlternatingRowStyle CssClass="ppp-table-even" />
                <Columns>
                    <asp:TemplateField HeaderText="品目コード">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkTo" runat="server" Target="_blank" 
                                NavigateUrl=<%# AstMaster.Common.AstMasterNavigation.makeLink(Container) %>
                                Text=<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>
                                ToolTip=<%# DataBinder.Eval(Container.DataItem, "HIN_TXT")%>
                             CssClass=tooltip-on />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="HING1_TXT" HeaderText="品目グループ１略称" ItemStyle-CssClass="ast-prm01-only table-cell" HeaderStyle-CssClass="ast-prm01-only table-cell" ItemStyle-Width="80">
                    </asp:BoundField>

                    <asp:BoundField DataField="HIN_CD_OLD" HeaderText="旧品目コード" ItemStyle-Width="150" ItemStyle-CssClass="ast-mdp01-only table-cell" HeaderStyle-CssClass="ast-mdp01-only table-cell" >
                    </asp:BoundField>
                    <asp:BoundField DataField="HINKAISO_CD" HeaderText="品目階層"  ItemStyle-Width="120" ItemStyle-CssClass="ast-mdp01-only table-cell" HeaderStyle-CssClass="ast-mdp01-only table-cell" >
                    </asp:BoundField>

                    <asp:TemplateField HeaderText="ＳＣ対象" ItemStyle-HorizontalAlign=Center ItemStyle-Width="40">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSCHEDULER_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "SCHEDULER_FLG")="1",True,False) %>  OnClick="return false;"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="しわ寄せ" ItemStyle-HorizontalAlign=Center ItemStyle-Width="40">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkINFL_HIN_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "INFL_HIN_FLG")="1",True,False) %>  OnClick="return false;"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%-- 
                    <asp:BoundField DataField="DHYO_MDL_KBN_TXT" HeaderText="代表モデル" ItemStyle-Width="40">
                    </asp:BoundField>
                    <asp:BoundField DataField="SPART" HeaderText="製品部門" ItemStyle-Width="40">
                    </asp:BoundField>
                    <asp:BoundField DataField="PARENT_HIN_CD" HeaderText="親銘柄">
                    </asp:BoundField>
                    <asp:BoundField DataField="PARENT_HIN_TXT" HeaderText="親銘柄名称">
                    </asp:BoundField>
                    --%>
                    <asp:BoundField DataField="HIN_SORT" HeaderText="ソート順" ItemStyle-Width="40">
                    </asp:BoundField>
                    <asp:BoundField DataField="KAKUGAI_KBN_TXT" HeaderText="格外銘柄" ItemStyle-Width="60">
                    </asp:BoundField>
                    <asp:BoundField DataField="HBAN_KBN_TXT" HeaderText="廃盤銘柄" ItemStyle-Width="60">
                    </asp:BoundField>

                    <asp:TemplateField HeaderText="無効フラグ" ItemStyle-Width="40" ItemStyle-HorizontalAlign=Center>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkMK_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "MK_FLG")="1",True,False) %> OnClick="return false;"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:Panel>
        <br/>
        <br/>
    </asp:Panel>
    </div>

    <div id="overAbsoluteContent">
    </div><br/>

</asp:Panel>

</asp:Content>


<asp:Content ID="Footer" ContentPlaceHolderID="pppFooter" Runat="Server" ClientIDMode=Static>
    <search:ms ID="SEARCH_HING" runat="server" TgtControl=txtHIN_CD AutoPostBack=True />
</asp:Content>

<asp:Content ID="EndDeclare" ContentPlaceHolderID="pppEndDeclare" Runat="Server" ClientIDMode=Static>
</asp:Content>

