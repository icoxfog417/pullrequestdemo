﻿
Partial Class _220_ast_Default
    Inherits System.Web.UI.Page

    'ページ初期処理
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'ブラウザにキャッシュさせない()
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

    End Sub

    'ページロード
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        hdLoginUser.Value = RequestManager.getCookieValue("bizsyslogin", "usrcd")

        'ページタイトルを消去
        Dim pnl As Panel = CType(Me.Page.Master.FindControl("pnlPageTitle"), Panel)
        pnl.Visible = False

    End Sub

    'ページエラー
    Protected Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error

        '例外のErrorイベントはすべてこのPage_Errorイベントで処理する
        AddHandler [Error], AddressOf Me.Page_Error

        Server.Transfer("../home/error.aspx")

    End Sub

    'Protected Sub btnDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDocument.Click

    '    Dim strWk As String
    '    strWk = "<script type='text/javascript'>"
    '    strWk += "var win;"
    '    strWk += "win=window.open('documentExplorer.aspx','Document',"
    '    strWk += "'top=' + (window.screen.height - 650) / 5 + ',left=' + (window.screen.width - 900) / 2 + ',width=900,height=650,menubar=no,toolbar=no,scrollbars=no,location=no');"
    '    strWk += "win.focus();"
    '    strWk += "</script>"
    '    ClientScript.RegisterStartupScript(Me.GetType(), "ドキュメント", strWk)

    'End Sub

End Class
