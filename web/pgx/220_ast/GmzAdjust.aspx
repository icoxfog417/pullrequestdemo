﻿<%@ Page Language="VB" MasterPageFile="astMaster.master" AutoEventWireup="false"
    CodeFile="GmzAdjust.aspx.vb" Inherits="_220_ast_GmzAdjust" %>

<%@ Register Src="../parts/UnitItem.ascx" TagName="unitItem" TagPrefix="ui" %>
<%@ MasterType VirtualPath="astMaster.master" %>
<asp:Content ID="clientHead" ContentPlaceHolderID="pppHead" runat="Server" ClientIDMode="Static">
    <!-- headのスクリプト処理箇所。必要なJavaScript処理などがあればここに記載-->
    <title>月末在庫数量意思入れ</title>
    <script type="text/javascript" language="javascript">
        $(function () {
            if ('<%=hdnMode.Value%>' == "S") {
                setTimeout(function () { makePPPSwitchArea(1) }, 100);
            } else {
                setTimeout(function () { makePPPSwitchArea(0) }, 100);
            }
        })
    </script>
</asp:Content>
<asp:Content ID="clientPageTitle" ContentPlaceHolderID="pppPageTitle" runat="Server"
    ClientIDMode="Static">
    月末在庫数量意思入れ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lblMsg" runat="server" Text="" CssClass="ppp-msg error" />
</asp:Content>
<asp:Content ID="clientPreDeclare" ContentPlaceHolderID="pppPreDeclare" runat="Server"
    ClientIDMode="Static">
    <asp:ScriptManagerProxy runat="server">
        <Services>
            <asp:ServiceReference Path="~/service/000_common/DBDataService.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="clientCenter" ContentPlaceHolderID="pppContent" runat="Server" ClientIDMode="Static">
    <!--新規登録/編集-->
    <asp:Panel ID="updArea" class="ppp-switch-area" runat="server" Width="100%">
        <h3 class="ppp-switch-header">
            調整値入力</h3>
        <asp:Panel ID="pnlGFORM" runat="server">
            <ui:unitItem ID="HIN_CD__KEY__GCON" CtlKind="LBL" runat="server" LabelText="品目コード"
                CssClass="gears-GRequired" />
            <ui:unitItem ID="HIN_TXT__GCON" CtlKind="LBL" runat="server" LabelText="品目テキスト" Width="325" />
            <ui:unitItem ID="HIN_CMT" CtlKind="TXT" runat="server" LabelText="在庫意思入れコメント" Width="450"
                CssClass="gears-GByteLengthBetween_MinLength_0_Length_256" />
            <br style="clear: both" />
            <asp:Label ID="lblAdjustDataHelp" runat="server" Text="＜在庫データ＞　単位：kg" />
            <asp:Table ID="tblADJUST" CssClass="ppp-table" CellSpacing="0" border="1" Style="border-collapse: collapse;"
                runat="server">
                <asp:TableHeaderRow ID="tblADJUST_trHeader01" runat="server" CssClass="ppp-table-head">
                    <asp:TableHeaderCell ID="tblJISHO_trHeader01_td01" runat="server" Text="対象月" RowSpan="2" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader01_td02" runat="server" Text="品質ランク内訳"
                        ColumnSpan="5" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader01_td03" runat="server" Text="SCP<br />対象"
                        RowSpan="2" BackColor="#668C99" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader01_td04" runat="server" Text="SCP<br />対象外"
                        RowSpan="2" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader01_td05" runat="server" Text="品目計" RowSpan="2" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader01_td06" runat="server" Text="SCP対象<br />調整値"
                        RowSpan="2" BackColor="#668C99" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader01_td07" runat="server" Text="調整後" RowSpan="2"
                        BackColor="#668C99" />
                </asp:TableHeaderRow>
                <asp:TableHeaderRow ID="tblADJUST_trHeader02" runat="server" CssClass="ppp-table-head">
                    <asp:TableHeaderCell ID="tblJISHO_trHeader02_td01" runat="server" Text="Ａ" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader02_td02" runat="server" Text="Ｂ" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader02_td03" runat="server" Text="Ｃ" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader02_td04" runat="server" Text="Ｄ" />
                    <asp:TableHeaderCell ID="tblJISHO_trHeader02_td05" runat="server" Text="なし" />
                </asp:TableHeaderRow>
                <asp:TableRow ID="tblJISHO_tr01" runat="server">
                    <asp:TableCell ID="tblJISHO_tr01_td01" runat="server" Width="50px" HorizontalAlign="Right">
                        <asp:Label ID="lblYM__KEY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td02" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblQRANK_A_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td03" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblQRANK_B_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td04" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblQRANK_C_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td05" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblQRANK_D_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td06" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblQRANK_Z_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td07" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblSCPTAISHO_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td08" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblSCPTAISHOGAI_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td09" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:Label ID="lblTOTAL_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td10" runat="server" Width="75px" HorizontalAlign="Right">
                        <asp:TextBox ID="txtADJUST_SRY" runat="server" Style="text-align: right;" Width="70px"
                            CssClass="gears-GRequired gears-GPeriodPositionOk_BeforeP_13_AfterP_0 gs-number" />
                    </asp:TableCell>
                    <asp:TableCell ID="tblJISHO_tr01_td11" runat="server" Width="75px" HorizontalAlign="Right"
                        BackColor="#FFFFAA" Font-Bold="True">
                        <asp:Label ID="lblADJUSTED_TOTAL_SRY__GCON" runat="server" CssClass="gs-number" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br style="clear: both" />
            <asp:Label ID="lblDetailDataHelp" runat="server" Text="＜プラント・ロット別内訳＞　単位：kg" />
            <br style="clear: both" />
            <asp:GridView ID="grvGmzDetail" runat="server" AutoGenerateDeleteButton="False" AutoGenerateEditButton="False"
                AutoGenerateSelectButton="False" CssClass="ppp-apl ppp-table" HeaderStyle-CssClass="ppp-table-head"
                AutoGenerateColumns="False" RowStyle-CssClass="ppp-table-odd" DataKeyNames="YM,HIN_CD,PLNT_CD" HeaderStyle-Wrap="False"
                RowStyle-Wrap="False">
                <AlternatingRowStyle CssClass="ppp-table-even" />
                <Columns>
                    <asp:BoundField DataField="YM" Visible="false" />
                    <asp:BoundField DataField="HIN_CD" Visible="false" />
                    <asp:BoundField DataField="PLNT_CD" HeaderText="プラント">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PLNT_TXT" HeaderText="プラントテキスト">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LOT_NO" HeaderText="ロット">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="QRANK" HeaderText="品質ﾗﾝｸ">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RIYOKA_SRY" HeaderText="利用可" DataFormatString="{0:N0}">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TENSOCHU_SRY" HeaderText="転送中" DataFormatString="{0:N0}">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="KENSACHU_SRY" HeaderText="品検中" DataFormatString="{0:N0}">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="HIRIKA_SRY" HeaderText="非利可" DataFormatString="{0:N0}">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="HORYU_SRY" HeaderText="保留" DataFormatString="{0:N0}">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="HENPIN_SRY" HeaderText="返品保留" DataFormatString="{0:N0}">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TOKUSHU_SRY" HeaderText="特殊" DataFormatString="{0:N0}">
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TOTAL_SRY" HeaderText="在庫合計" DataFormatString="{0:N0}" >
                        <HeaderStyle Wrap="False" />
                        <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:HiddenField ID="hdnUPD_USR" runat="server" />
            <br style="clear: both" />
            <asp:Button ID="btnUpdate" runat="server" Text="　更新　" />
            <div style="clear: both">
            </div>
        </asp:Panel>
        <!--検索条件-->
        <h3 class="ppp-switch-header">
            検索条件</h3>
        <asp:Panel ID="pnlGFilter" runat="server">
            <ui:unitItem ID="HIN_CD__2" CtlKind="TXT" runat="server" LabelText="品目コード" Operator="START_WITH" />
            <ui:unitItem ID="HIN_TXT__2" CtlKind="TXT" runat="server" LabelText="品目テキスト" Operator="LIKE" />
            <ui:unitItem ID="ZK_CMT_EXIST" CtlKind="RBL" runat="server" LabelText="在庫コメント" IsNeedAll="True"
                DefaultCheckIndex="0" />
            <ui:unitItem ID="HIN_CD_OLD__2" CtlKind="TXT" runat="server" LabelText="旧品目コード" Operator="LIKE"
                AuthorizationAllow="AST_MDP01" RoleEvalAction="VISIBLE" />
            <ui:unitItem ID="HINKAISO_CD__2" CtlKind="TXT" runat="server" LabelText="品目階層" Operator="START_WITH"
                AuthorizationAllow="AST_MDP01" RoleEvalAction="VISIBLE" />
            <ui:unitItem ID="HING1_CD__2" CtlKind="DDL" runat="server" LabelText="品目グループ１" IsNeedAll="True"
                AuthorizationAllow="AST_PRM01" RoleEvalAction="VISIBLE" />
            <br style="clear: both" />
            <asp:Button ID="btnSearch" runat="server" Text="　検索　" />
        </asp:Panel>
    </asp:Panel>
    <br />
    <!--表示-->
    <asp:Label ID="lblSummaryDataHelp" runat="server" Style="margin-left: 23px;" Text="　　単位：kg　※背景が灰色の行はスケジューラ対象外の品目です" />
    <br />
    <asp:GridView ID="grvGmzSummary" runat="server" AutoGenerateDeleteButton="False" AutoGenerateEditButton="False"
        AutoGenerateSelectButton="False" CssClass="ppp-table" HeaderStyle-CssClass="ppp-table-head"
        EnableViewState="True" DataSourceID="odsData" PageSize="20" AllowPaging="True"
        AutoGenerateColumns="False" RowStyle-CssClass="ppp-table-odd" DataKeyNames="YM,HIN_CD"
        HeaderStyle-Wrap="False" RowStyle-Wrap="False" Style="margin-left: 33px;">
        <AlternatingRowStyle CssClass="ppp-table-even" />
        <Columns>
            <asp:CommandField ShowSelectButton="True">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
            </asp:CommandField>
            <asp:BoundField DataField="YM" HeaderText="対象月">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="品目コード">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:HyperLink ID="lnkTo" runat="server" Target="dashBoard" NavigateUrl='<%# "SchedulerItemDashBoard.aspx?hin_cd=" & DataBinder.Eval(Container.DataItem, "HIN_CD")%>'
                        Text='<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="HIN_TXT" HeaderText="品目テキスト">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="QRANK_A_SRY" HeaderText="Ａﾗﾝｸ" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="QRANK_B_SRY" HeaderText="Ｂﾗﾝｸ" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="QRANK_C_SRY" HeaderText="Ｃﾗﾝｸ" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="QRANK_D_SRY" HeaderText="Ｄﾗﾝｸ" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="QRANK_Z_SRY" HeaderText="ﾗﾝｸなし" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="SCPTAISHO_SRY" HeaderText="SCP<br />対象" DataFormatString="{0:N0}"
                HtmlEncode="False">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="SCPTAISHOGAI_SRY" HeaderText="SCP<br />対象外" DataFormatString="{0:N0}"
                HtmlEncode="False">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="TOTAL_SRY" HeaderText="品目計" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUST_SRY" HeaderText="SCP対象<br />調整値" DataFormatString="{0:N0}"
                HtmlEncode="False">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_TOTAL_SRY" HeaderText="調整後" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Font-Bold="True" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="HIN_CMT" HeaderText="在庫意思入れコメント">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="SCHEDULER_FLG" Visible="False" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsData" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount"
        TypeName="Gears.GDSTemplate" EnablePaging="True"></asp:ObjectDataSource>
    <asp:Label ID="lblCount" runat="server" Text="" CssClass="ppp-msg success" Style="margin-left: 33px;"></asp:Label>
    <br />
    <br />
    <asp:Panel ID="pnlKeyItem" runat="server">
        <asp:HiddenField ID="hdnHIN_CD" runat="server" />
        <asp:HiddenField ID="hdnYM" runat="server" />
    </asp:Panel>
    <asp:HiddenField ID="hdnMode" runat="server" />
</asp:Content>
