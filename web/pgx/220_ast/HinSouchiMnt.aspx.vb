﻿Imports Gears

''' ------------------------------------------------------------------------------
''' Project     : 220_ast
''' Class       : _220_ast_HinSouchiMnt
''' 
''' -------------------------------------------------------------------------------
''' <summary>
''' AST品目生産装置マスタメンテ画面
''' </summary>
''' <remarks>
''' </remarks>
''' <hisotry>
'''  [K.Takahashi] 2012/04/16 Created
''' </hisotry>
''' -------------------------------------------------------------------------------
Partial Class _220_ast_HinSouchiMnt
    Inherits GearsPage

#Region "定数"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 例外品目区分：通常
    ''' </summary>
    ''' <remarks>
    ''' 例外品目区分：通常
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Const strHIN_KBN_USUALLY As String = "0"

#End Region

#Region "変数"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' dsHinSouchi
    ''' </summary>
    ''' <remarks>
    ''' 品目装置マスタ Gears Data Souce
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private dsHinSouchi As AstMaster.AST_M_HIN_SOUCHI = Nothing

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' dsHinSouchi
    ''' </summary>
    ''' <remarks>
    ''' 品目マスタ Gears Data Souce
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private dsHin As AstMaster.AST_M_HIN = Nothing

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' gvUtil
    ''' </summary>
    ''' <remarks>
    ''' GridViewユーティリティ
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private gvUtil As GridViewUtility = Nothing

    Private updList As New List(Of GearsDTO)

#End Region

#Region "イベント"

#Region "ページ"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Init
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Master.DisplayMenu = False

        dsHinSouchi = New AstMaster.AST_M_HIN_SOUCHI(GPageMediator.ConnectionName)
        dsHin = New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)

        '詳細表示用のコントロール/データソースを登録
        registerMyControl(pnlGFORM__HIN, dsHin)
        registerMyControl(hdnDHYO_MDL_KBN)
        registerMyControl(ddlSOUCHI_CD)
        registerMyControl(hdnHIN_CD)

        'GridViewユーティリティ初期化
        gvUtil = New GridViewUtility(grvHinSouchi)

        btnClose.OnClientClick = "fncCloseWindow()"

    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Load
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        addRelation(hdnHIN_CD, pnlGFORM__HIN)
        addRelation(hdnDHYO_MDL_KBN, ddlSOUCHI_CD)
        addRelation(hdnDHYO_MDL_KBN, lblCHARGE_UNIT__GCON)
        'for debug
        '        GearsLogStack.traceOn()
        If IsPostBack Then
        Else
            hdnHIN_CD.Value = getQueryString("hin_cd")
            hdnUpdFunc.Value = getQueryString("upd_func")
            If String.IsNullOrEmpty(hdnHIN_CD.Value) Then
                btnUpdate.Visible = False
                btnAddNew.Visible = False
                Log.Add(pnlGFORM__HIN.ID, New GearsDataValidationException("品目コードが指定されていません。"))
                getLogMsgDescription(False, lblMsg)
                Exit Sub
            End If
            'モデル区分を設定
            executeBehavior(hdnHIN_CD)
            If Not String.IsNullOrEmpty(getQueryString("mdl_kbn")) Then
                'モデル区分が指定された場合は、モデル区分から装置ドロップダウンリストを作成
                hdnDHYO_MDL_KBN.Value = getQueryString("mdl_kbn")
            End If
            executeBehavior(hdnDHYO_MDL_KBN)
            If Not String.IsNullOrEmpty(getQueryString("hin_kbn")) Then
                hdnHIN_KBN.Value = getQueryString("hin_kbn")
            End If
            getMyControl(lblCHARGE_UNIT__GCON).dataAttach()
            If hdnHIN_KBN.Value = AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA) OrElse hdnHIN_KBN.Value = AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NONE) Then
                '品目区分は、銘柄または重合Gである必要があります
                btnUpdate.Visible = False
                btnAddNew.Visible = False
                Log.Add(pnlGFORM__HIN.ID, New GearsDataValidationException("品目区分が正しくありません。"))
                getLogMsgDescription(False, lblMsg)
                Exit Sub
            End If
            refresh()
        End If

        ''javascriptを定義
        Dim csm As ClientScriptManager = Page.ClientScript
        Dim sb As New StringBuilder(String.Empty)

        sb.Append("<script language=""javascript"">")
        sb.Append("function fncParentUpdate(){")
        If Not String.IsNullOrEmpty(hdnUpdFunc.Value) Then
            sb.Append("if (window.opener && !window.opener.closed) { window.opener." + hdnUpdFunc.Value + "(); }")
        End If
        sb.Append("}")
        sb.Append("</script>")
        If Not csm.IsClientScriptBlockRegistered("fncParentUpdate") Then
            csm.RegisterClientScriptBlock(Me.GetType(), "fncParentUpdate", sb.ToString(), False)
        End If

    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_LoadComplete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If IsPostBack Then
        End If

        '開始～終了までの間に実行された処理のログを出力する
        '        lblLog.Text = GearsLogStack.makeDisplayString
        ''ログ記録を終了する。'
        '        GearsLogStack.traceEnd()

    End Sub

#End Region

#Region "GridView"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' grvHinSouchi_RowDataBound
    ''' </summary>
    ''' <remarks>
    ''' GridViewにデータがBoundされる時に呼ばれる
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/08 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub grvHinSouchi_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvHinSouchi.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            '無効フラグと代表装置フラグを取得
            Dim chbMK_FLG As CheckBox = CType(e.Row.FindControl("chbMK_FLG"), CheckBox)
            Dim lblDHYO_SOUCHI_FLG As Label = CType(e.Row.FindControl("lblDHYO_SOUCHI_FLG"), Label)
            If chbMK_FLG IsNot Nothing AndAlso lblDHYO_SOUCHI_FLG IsNot Nothing AndAlso Not chbMK_FLG.Checked AndAlso lblDHYO_SOUCHI_FLG.Text = "1" Then
                '代表装置で無効フラグがOFFの場合は編集不可とする
                chbMK_FLG.Enabled = False
            End If
            Dim lblCHARGE_UNIT As Label = CType(e.Row.FindControl("lblCHARGE_UNIT"), Label)
            If lblCHARGE_UNIT IsNot Nothing Then
                lblCHARGE_UNIT.Text = lblCHARGE_UNIT__GCON.Text
            End If
        End If

    End Sub

#End Region

#Region "Button"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' btnAddNew_Click
    ''' </summary>
    ''' <remarks>
    ''' 追加ボタン押下イベント
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim blnRslt As Boolean = True
        If IsReload Then
            Log.Add(grvHinSouchi.ID, New GearsException("リロードと判定されました。更新処理" + GearsDTO.getAtypeString(ActionType.INS) + "は実行せず、データのロードのみ行います"))
        Else
            blnRslt = InsertHinSouchi()
            If blnRslt Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "key", "fncParentUpdate()", True)
            End If
        End If
        refresh()
        getLogMsgDescription(blnRslt, lblMsg)
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' btnUpdate_Click
    ''' </summary>
    ''' <remarks>
    ''' 更新ボタン押下イベント
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim blnRslt As Boolean = True
        If IsReload Then
            Log.Add(grvHinSouchi.ID, New GearsException("リロードと判定されました。更新処理" + GearsDTO.getAtypeString(ActionType.UPD) + "は実行せず、データのロードのみ行います"))
            refresh()
        Else
            updList.Clear()
            Dim result As String = gvUtil.fetchEachRow(AddressOf Me.fetchHinSouchiRow)
            If result = "" Then
                '品目装置更新処理
                result = executeHinSouchiUpd()
                If result = "" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "key", "fncParentUpdate()", True)
                    refresh()
                End If
            End If
            If result <> "" Then
                Log.Add(grvHinSouchi.ID, New GearsDataValidationException(result))
                blnRslt = False
            End If
        End If
        getLogMsgDescription(blnRslt, lblMsg)
    End Sub

#End Region

#End Region

#Region "その他関数"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' fetchHinSouchiRow
    ''' </summary>
    ''' <remarks>
    ''' 品目装置更新処理(行単位)
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function fetchHinSouchiRow(ByVal index As Integer, ByRef rowInfo As Dictionary(Of String, TableCell)) As String
        Dim dto As New GearsDTO(ActionType.UPD)
        Dim result As String = ""

        GPageMediator.fetchControls(grvHinSouchi.Rows(index), AddressOf Me.fetchInRowControl, AddressOf isValidTarget, dto)

        For Each item As KeyValuePair(Of String, TableCell) In rowInfo
            Select Case item.Key
                Case "HIN_CD"
                    dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(item.Value.Text).key)
                Case "装置"
                    dto.addFilter(SqlBuilder.newFilter("SOUCHI_CD").eq(item.Value.Text).key)
                Case "CHARGE_UNIT"
                    If hdnHIN_KBN.Value = AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER) Then
                        '重合Gの場合のみ更新対象
                        dto.addSelection(SqlBuilder.newSelect(item.Key).setValue(lblCHARGE_UNIT__GCON.Text))
                    End If
                Case "UPD_YMD"
                    dto.addFilter(SqlBuilder.newFilter(item.Key).eq(item.Value.Text).key)
                    dto.addSelection(SqlBuilder.newSelect(item.Key).setValue(dsHinSouchi.getLockTypeValue(LockType.UDATESTR)))
                Case "UPD_HMS"
                    dto.addFilter(SqlBuilder.newFilter(item.Key).eq(item.Value.Text).key)
                    dto.addSelection(SqlBuilder.newSelect(item.Key).setValue(dsHinSouchi.getLockTypeValue(LockType.UTIMESTR)))
            End Select
        Next
        dto.addSelection(SqlBuilder.newSelect("UPD_USR").setValue(Master.LoginUser))
        If String.IsNullOrEmpty(dto.getAttrInfo("VALIDATION_ERR")) Then
            updList.Add(dto)
        Else
            result = (index + 1).ToString + "行目のデータにエラーがあります：" + dto.getAttrInfo("VALIDATION_ERR") + "<br/>"
        End If

        Return result

    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' fetchInRowControl
    ''' </summary>
    ''' <remarks>
    ''' 品目装置更新処理(行単位)
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub fetchInRowControl(ByRef control As Control, ByRef dto As GearsDTO)
        Dim inRowCon As New GearsControl(control, GPageMediator.ConnectionName, GPageMediator.DsNameSpace)
        Dim conInfo As New GearsControlInfo(inRowCon)
        conInfo.IsFormAttribute = True '各コントロールは、更新用のフォームコントロールとして認識させる
        dto.addControlInfo(conInfo)
        'バリデーション
        If Not inRowCon.isValidateOk And String.IsNullOrEmpty(dto.getAttrInfo("VALIDATION_ERR")) Then
            dto.addAttrInfo("VALIDATION_ERR", inRowCon.getValidatedMsg)
        End If
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' isValidTarget
    ''' </summary>
    ''' <remarks>
    ''' データチェック処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function isValidTarget(ByRef control As Control) As Boolean
        If control.Visible = True Then
            Return GPageMediator.isTargetControl(control)
        Else
            Return False 'Visible Falseの場合対象外
        End If
    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' executeHinSouchiUpd
    ''' </summary>
    ''' <remarks>
    ''' 更新処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Function executeHinSouchiUpd() As String
        Dim result As String = ""
        '各行更新
        For index As Integer = 0 To updList.Count - 1
            Dim preStr As String = ""
            If updList.Count > 1 Then
                preStr = (index + 1).ToString + "行目："
            End If
            Try
                dsHinSouchi.gUpdate(updList(index))
            Catch ex As GearsRequestedActionInvalid
                result += preStr + "データは他のユーザーにより更新されています。再度更新してください。<br/>"
            Catch ex As Exception
                Dim msg As String = ""
                If Not ex.InnerException Is Nothing Then
                    msg = ex.InnerException.Message
                Else
                    msg = ex.Message
                End If
                result += preStr + "データは更新されませんでした：" + msg + "<br/>"
            End Try
        Next

        Return result

    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' InsertHinSouchi
    ''' </summary>
    ''' <remarks>
    ''' 品目装置マスタ更新処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function InsertHinSouchi() As Boolean
        Dim blnRet As Boolean = False
        Dim dto As New GearsDTO(ActionType.INS)
        Dim strSOUCHI_CD As String = String.Empty

        '存在チェック
        If ExistsHinSouchi(hdnHIN_CD.Value, ddlSOUCHI_CD.SelectedValue.ToString) Then
            Log.Add(hdnHIN_CD.ID, New GearsDataValidationException("品目コード=[" + hdnHIN_CD.Value + "],装置コード=[" + ddlSOUCHI_CD.SelectedValue.ToString + "]は既に存在します。"))
            Return False
        End If
        If txtCHARGE__NEW.Visible = True Then
            If Not isValidateOk(txtCHARGE__NEW) Then
                Return False
            End If
        End If
        '品目コード
        dto.addSelection(SqlBuilder.newSelect("HIN_CD").setValue(hdnHIN_CD.Value).key)
        '装置コード
        dto.addSelection(SqlBuilder.newSelect("SOUCHI_CD").setValue(ddlSOUCHI_CD.SelectedValue.ToString).key)
        'チャージ
        If txtCHARGE__NEW.Visible Then
            dto.addSelection(SqlBuilder.newSelect("CHARGE").setValue(txtCHARGE__NEW.Text))
            dto.addSelection(SqlBuilder.newSelect("CHARGE_UNIT").setValue(lblCHARGE_UNIT__GCON.Text))
        End If
        'ログインユーザ
        dto.addSelection(SqlBuilder.newSelect("UPD_USR").setValue(Master.LoginUser))
        Try
            dsHinSouchi.gInsert(dto)
            blnRet = True
        Catch ex As GearsException
            Log.Add(grvHinSouchi.ID, New GearsDataValidationException(ex.Message))
            blnRet = False
        End Try
        Return blnRet
    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' ExistsHinSouchi
    ''' </summary>
    ''' <remarks>
    ''' 品目装置存在チェック
    ''' </remarks>
    ''' <param name="strHinCD">品目コード</param>
    ''' <param name="strSouchiCD">装置コード</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function ExistsHinSouchi(ByVal strHinCD As String, ByVal strSouchiCD As String) As Boolean
        Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)

        dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(strHinCD))
        dto.addFilter(SqlBuilder.newFilter("SOUCHI_CD").eq(strSouchiCD))
        If dsHinSouchi.gSelectCount(dto) > 0 Then
            Return True
        End If
        Return False

    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 再表示
    ''' </summary>
    ''' <remarks>
    ''' フィルタの値でロード
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub refresh()

        'パウダー以外の場合はロード、ロード単位を非表示にする。
        If hdnHIN_KBN.Value <> AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER) Then
            lblCHARGE.Visible = False
            lblCHARGE_UNIT__GCON.Visible = False
            txtCHARGE__NEW.Visible = False
            gvUtil.addHideColumn("ロード")
            gvUtil.addHideColumn("ロード単位")
            tblAddNew.Rows(0).FindControl("tdCHARGE_TITLE").Visible = False
            tblAddNew.Rows(0).FindControl("tdCHARGE").Visible = False
            tblAddNew.Rows(0).FindControl("tdCHARGE_UNIT").Visible = False
        Else
            lblCHARGE.Visible = True
            lblCHARGE_UNIT__GCON.Visible = True
            txtCHARGE__NEW.Visible = True
            '初期化
            txtCHARGE__NEW.Text = String.Empty
        End If
        gvUtil.addHideColumn("DHYO_SOUCHI_FLG")
        gvUtil.addHideColumn("CHARGE_UNIT")
        gvUtil.addHideColumn("HIN_CD")
        gvUtil.addHideColumn("UPD_YMD")
        gvUtil.addHideColumn("UPD_HMS")

        Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)

        dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(hdnHIN_CD.Value))

        Dim dt As System.Data.DataTable = dsHinSouchi.gSelect(dto)
        grvHinSouchi.DataSource = dt
        grvHinSouchi.DataBind()
        'データ件数が0件の場合は、保存ボタンを押せなくする。
        If dt.Rows.Count = 0 Then
            btnUpdate.Enabled = False
        Else
            btnUpdate.Enabled = True
        End If

    End Sub

#End Region




End Class

