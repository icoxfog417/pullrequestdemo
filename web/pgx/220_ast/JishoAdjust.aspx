﻿<%@ Page Language="VB" MasterPageFile="astMaster.master" AutoEventWireup="false"
    CodeFile="JishoAdjust.aspx.vb" Inherits="_220_ast_JishoAdjust" %>

<%@ Register Src="../parts/UnitItem.ascx" TagName="unitItem" TagPrefix="ui" %>
<%@ Register src="../parts/220_ast/SCItemSearch.ascx" tagname="hinSearch" tagprefix="hs" %>
<%@ MasterType VirtualPath="astMaster.master" %>
<asp:Content ID="clientHead" ContentPlaceHolderID="pppHead" runat="Server" ClientIDMode="Static">
    <!-- headのスクリプト処理箇所。必要なJavaScript処理などがあればここに記載-->
    <title>自消計画数量意思入れ</title>
    <script>
        var codeSearchPlnt = new CodeSearcher("pnlForCodeSearch__PLNT", "lstPlntBox", "");

        $(function () {
            codeSearchPlnt.initDialog("プラント検索", 450, 400);

            if ('<%=hdnMode.Value%>' == "S") {
                setTimeout(function () { makePPPSwitchArea(1) }, 100);
            } else {
                setTimeout(function () { makePPPSwitchArea(0) }, 100);
            }

        })

        function itemCodeSearchPlnt(target) {
            codeSearchPlnt.setSelectedCallback(function () {
                __doPostBack(target, '');
            });
            codeSearchPlnt.setTarget(target);
            codeSearchPlnt.openDialog()
        }

        function synchro(defaultTB, diffTB, adjustTB, adjustedTB) {
            var txt1 = document.getElementById(defaultTB);
            var txt2 = document.getElementById(diffTB);
            var txt3 = document.getElementById(adjustTB);
            var txt4 = document.getElementById(adjustedTB);
            var x = txt1.innerHTML;
            x = x.replace(",", "");
            if (isNaN(x) == true || x == "") {
                x = 0;
            }
            if (isNaN(txt3.value) == false) {
                if (txt3.value == null || txt3.value == "") {
                    txt2.value = 0;
                    txt4.value = x;
                } else {
                    txt2.value = txt3.value - x;
                    txt4.value = txt3.value;
                }
            } else {
                txt2.value = "";
                txt4.value = "";
            }
        }

    </script>
</asp:Content>
<asp:Content ID="clientPageTitle" ContentPlaceHolderID="pppPageTitle" runat="Server"
    ClientIDMode="Static">
    自消計画数量意思入れ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lblMsg" runat="server" Text="" CssClass="ppp-msg error" />
</asp:Content>
<asp:Content ID="clientPreDeclare" ContentPlaceHolderID="pppPreDeclare" runat="Server"
    ClientIDMode="Static">
    <asp:ScriptManagerProxy runat="server">
        <Services>
            <asp:ServiceReference Path="~/service/000_common/DBDataService.asmx" />
        </Services>
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="clientCenter" ContentPlaceHolderID="pppContent" runat="Server" ClientIDMode="Static">
    <!--新規登録/編集-->
    <asp:Panel ID="updArea" class="ppp-switch-area" runat="server" Width="100%">
        <h3 class="ppp-switch-header">
            調整値入力</h3>
        <asp:Panel ID="pnlGFORM" runat="server">
            <asp:UpdatePanel ID="udpJisho" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlKeyItem" runat="server">
                        <asp:HiddenField ID="hdnSAKUTEI_YM__KEY" runat="server" />
                        <asp:Panel ID="pnlHin" runat="server">
                            <ui:unitItem ID="HIN_CD__KEY" runat="server" LabelText="品目コード" SearchAction="SEARCH_NSGT_1.openDialog()"
                                CtlKind="TXT" CssClass="gears-GRequired gears-GByteLengthBetween_MinLength_0_Length_18" />
                            <ui:unitItem ID="HIN_TXT__GCON" runat="server" CtlKind="LBL" LabelText="品目テキスト" Width="400" />
                        </asp:Panel>
                        <asp:Panel ID="pnlPlnt" runat="server">
                            <ui:unitItem ID="PLNT_CD__KEY" runat="server" SearchAction="itemCodeSearchPlnt('txtPLNT_CD__KEY')"
                                CtlKind="TXT" LabelText="プラントコード" Width="50" CssClass="gears-GRequired gears-GByteLengthBetween_MinLength_4_Length_4" />
                            <ui:unitItem ID="PLNT_TXT__GCON" runat="server" CtlKind="LBL" LabelText="プラントテキスト"
                                Width="200" />
                        </asp:Panel>
                    </asp:Panel>
                    <br style="clear: both" />
                    <asp:UpdateProgress ID="upgJisho" runat="server" AssociatedUpdatePanelID="udpJisho"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="prgTemp" runat="server" style="text-align: center; background-color: #ffffff;">
                                <div>
                                    <img src="images/progress.gif" />
                                    処理中です・・・
                                </div>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:Label ID="lblAdjustDataHelp" runat="server" Text="＜自消計画データ＞　単位：kg" />
                    <br style="clear: both" />
                    <asp:Table ID="tblJISHO" class="ppp-table" CellSpacing="0" border="1" Style="border-collapse: collapse;"
                        runat="server">
                        <asp:TableHeaderRow ID="tblJISHO_trHeader" runat="server" CssClass="ppp-table-head">
                            <asp:TableHeaderCell ID="tblJISHO_trHeader_td01" runat="server" Style="width: 90px;" />
                            <asp:TableHeaderCell ID="tblJISHO_trHeader_td02" runat="server" Style="width: 90px;"
                                Text="計画値" />
                            <asp:TableHeaderCell ID="tblJISHO_trHeader_td03" runat="server" Style="width: 90px;"
                                Text="増減" />
                            <asp:TableHeaderCell ID="tblJISHO_trHeader_td04" runat="server" Style="width: 90px;"
                                Text="調整数" />
                            <asp:TableHeaderCell ID="tblJISHO_trHeader_td05" runat="server" Style="width: 90px;"
                                Text="調整後" />
                        </asp:TableHeaderRow>
                        <asp:TableRow ID="tblJISHO_tr01" runat="server">
                            <asp:TableCell ID="tblJISHO_tr01_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM01" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr01_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY01__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr01_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY01" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr01_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY01" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY01__GCON','txtDIFF_SRY01','txtADJUST_SRY01','txtADJUSTED_SRY01')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr01_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY01" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr02" runat="server">
                            <asp:TableCell ID="tblJISHO_tr02_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM02" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr02_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY02__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr02_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY02" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr02_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY02" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY02__GCON','txtDIFF_SRY02','txtADJUST_SRY02','txtADJUSTED_SRY02')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr02_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY02" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr03" runat="server">
                            <asp:TableCell ID="tblJISHO_tr03_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM03" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr03_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY03__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr03_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY03" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr03_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY03" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY03__GCON','txtDIFF_SRY03','txtADJUST_SRY03','txtADJUSTED_SRY03')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr03_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY03" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr04" runat="server">
                            <asp:TableCell ID="tblJISHO_tr04_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM04" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr04_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY04__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr04_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY04" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr04_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY04" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY04__GCON','txtDIFF_SRY04','txtADJUST_SRY04','txtADJUSTED_SRY04')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr04_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY04" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr05" runat="server">
                            <asp:TableCell ID="tblJISHO_tr05_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM05" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr05_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY05__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr05_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY05" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr05_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY05" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY05__GCON','txtDIFF_SRY05','txtADJUST_SRY05','txtADJUSTED_SRY05')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr05_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY05" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr06" runat="server">
                            <asp:TableCell ID="tblJISHO_tr06_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM06" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr06_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY06__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr06_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY06" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr06_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY06" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY06__GCON','txtDIFF_SRY06','txtADJUST_SRY06','txtADJUSTED_SRY06')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr06_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY06" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr07" runat="server">
                            <asp:TableCell ID="tblJISHO_tr07_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM07" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr07_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY07__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr07_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY07" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr07_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY07" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY07__GCON','txtDIFF_SRY07','txtADJUST_SRY07','txtADJUSTED_SRY07')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr07_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY07" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr08" runat="server">
                            <asp:TableCell ID="tblJISHO_tr08_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM08" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr08_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY08__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr08_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY08" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr08_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY08" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY08__GCON','txtDIFF_SRY08','txtADJUST_SRY08','txtADJUSTED_SRY08')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr08_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY08" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr09" runat="server">
                            <asp:TableCell ID="tblJISHO_tr09_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM09" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr09_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY09__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr09_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY09" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr09_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY09" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY09__GCON','txtDIFF_SRY09','txtADJUST_SRY09','txtADJUSTED_SRY09')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr09_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY09" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr10" runat="server">
                            <asp:TableCell ID="tblJISHO_tr10_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM10" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr10_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY10__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr10_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY10" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr10_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY10" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY10__GCON','txtDIFF_SRY10','txtADJUST_SRY10','txtADJUSTED_SRY10')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr10_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY10" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr11" runat="server">
                            <asp:TableCell ID="tblJISHO_tr11_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM11" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr11_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY11__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr11_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY11" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr11_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY11" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY11__GCON','txtDIFF_SRY11','txtADJUST_SRY11','txtADJUSTED_SRY11')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr11_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY11" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr12" runat="server">
                            <asp:TableCell ID="tblJISHO_tr12_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM12" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr12_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY12__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr12_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY12" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr12_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY12" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY12__GCON','txtDIFF_SRY12','txtADJUST_SRY12','txtADJUSTED_SRY12')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr12_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY12" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr13" runat="server">
                            <asp:TableCell ID="tblJISHO_tr13_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM13" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr13_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY13__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr13_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY13" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr13_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY13" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY13__GCON','txtDIFF_SRY13','txtADJUST_SRY13','txtADJUSTED_SRY13')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr13_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY13" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr14" runat="server">
                            <asp:TableCell ID="tblJISHO_tr14_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM14" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr14_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY14__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr14_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY14" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr14_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY14" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY14__GCON','txtDIFF_SRY14','txtADJUST_SRY14','txtADJUSTED_SRY14')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr14_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY14" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="tblJISHO_tr15" runat="server">
                            <asp:TableCell ID="tblJISHO_tr15_td01" runat="server" Style="width: 90px; font-weight: bold;">
                                <asp:Label ID="lblJISHO_YM15" runat="server" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr15_td02" runat="server" Style="width: 90px; text-align: right;">
                                <asp:Label ID="lblSRY15__GCON" runat="server" Width="85px" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr15_td03" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtDIFF_SRY15" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr15_td04" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUST_SRY15" runat="server" Style="text-align: right;" Width="85px"
                                    onblur="synchro('lblSRY15__GCON','txtDIFF_SRY15','txtADJUST_SRY15','txtADJUSTED_SRY15')"
                                    CssClass="gears-GPeriodPositionOk_BeforeP_18_AfterP_0 gears-GNumber" />
                            </asp:TableCell>
                            <asp:TableCell ID="tblJISHO_tr15_td05" runat="server" Style="width: 90px;">
                                <asp:TextBox ID="txtADJUSTED_SRY15" runat="server" Style="text-align: right;" Width="85px"
                                    Enabled="False" class="gs-number" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:HiddenField ID="hdnUPD_USR" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <br style="clear: both" />
            <asp:Button ID="btnUpdate" runat="server" Text="　登録 / 更新　" />
            <div style="clear: both">
            </div>
        </asp:Panel>
        <!--検索条件-->
        <h3 class="ppp-switch-header">
            検索条件</h3>
        <asp:Panel ID="pnlGFilter" runat="server">
            <ui:unitItem ID="HIN_CD__2" CtlKind="TXT" runat="server" LabelText="品目コード" Operator="START_WITH" />
            <ui:unitItem ID="HIN_TXT__2" CtlKind="TXT" runat="server" LabelText="品目テキスト" Operator="LIKE" />
            <ui:unitItem ID="HIN_CD_OLD__2" CtlKind="TXT" runat="server" LabelText="旧品目コード" Operator="LIKE"
                AuthorizationAllow="AST_MDP01" RoleEvalAction="VISIBLE" />
            <ui:unitItem ID="HINKAISO_CD__2" CtlKind="TXT" runat="server" LabelText="品目階層" Operator="START_WITH"
                AuthorizationAllow="AST_MDP01" RoleEvalAction="VISIBLE" />
            <ui:unitItem ID="HING1_CD__2" CtlKind="DDL" runat="server" LabelText="品目グループ１" IsNeedAll="True"
                AuthorizationAllow="AST_PRM01" RoleEvalAction="VISIBLE" />
            <br style="clear: both" />
            <asp:Button ID="btnSearch" runat="server" Text="　検索　" />
        </asp:Panel>
    </asp:Panel>
    <br />
    <!--表示-->
    <asp:Label ID="lblSummaryDataHelp" runat="server" Text="　　単位：kg　※背景が黄色の行はスケジューラ対象外の品目です"
        Style="margin-left: 23px;" />
    <br />
    <asp:GridView ID="grvJisho" runat="server" AutoGenerateDeleteButton="False" AutoGenerateEditButton="False"
        AutoGenerateSelectButton="False" CssClass="ppp-table" HeaderStyle-CssClass="ppp-table-head"
        EnableViewState="True" DataSourceID="odsData" PageSize="20" AllowPaging="True"
        AutoGenerateColumns="False" RowStyle-CssClass="ppp-table-odd" DataKeyNames="SAKUTEI_YM,HIN_CD,PLNT_CD"
        HeaderStyle-Wrap="False" RowStyle-Wrap="False" Style="margin-left: 33px;">
        <AlternatingRowStyle CssClass="ppp-table-even" />
        <Columns>
            <asp:CommandField ShowSelectButton="True">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
            </asp:CommandField>
            <asp:BoundField DataField="SAKUTEI_YM" HeaderText="策定年月">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="品目コード">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="lblHIN_CD" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>'
                        ToolTip='<%# DataBinder.Eval(Container.DataItem, "HIN_TXT")%>' CssClass="tooltip-on" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ﾌﾟﾗﾝﾄ">
                <HeaderStyle Wrap="False" />
                <ItemStyle Wrap="False" />
                <ItemTemplate>
                    <asp:Label ID="lblPLNT_CD" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PLNT_CD")%>'
                        ToolTip='<%# DataBinder.Eval(Container.DataItem, "PLNT_TXT")%>' CssClass="tooltip-on" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SRY01" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY01" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY01" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY02" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY02" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY02" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY03" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY03" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY03" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY04" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY04" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY04" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY05" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY05" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY05" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY06" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY06" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY06" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY07" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY07" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY07" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY08" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY08" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY08" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY09" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY09" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY09" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY10" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY10" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY10" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY11" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY11" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY11" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY12" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY12" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY12" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY13" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY13" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY13" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY14" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY14" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY14" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SRY15" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="DIFF_SRY15" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" />
            </asp:BoundField>
            <asp:BoundField DataField="ADJUSTED_SRY15" DataFormatString="{0:N0}">
                <HeaderStyle Wrap="False" />
                <ItemStyle HorizontalAlign="Right" Wrap="False" Font-Bold="True" />
            </asp:BoundField>
            <asp:BoundField DataField="SCHEDULER_FLG" Visible="False" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsData" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount"
        TypeName="Gears.GDSTemplate" EnablePaging="True"></asp:ObjectDataSource>
    <asp:Label ID="lblCount" runat="server" Text="" CssClass="ppp-msg success" Style="margin-left: 33px;"></asp:Label>
    <br />
    <br />
    <asp:Panel ID="pnlJISHO_YM" runat="server">
        <asp:HiddenField ID="hdnJISHO_YM" runat="server" />
    </asp:Panel>
    <asp:HiddenField ID="hdnMode" runat="server" />
</asp:Content>
<asp:Content ID="clientFooter" ContentPlaceHolderID="pppFooter" runat="Server" ClientIDMode="Static">
    <!-- 品目検索用コントロール -->
    <hs:hinSearch ID="SEARCH_NSGT_1" runat="server" TgtControl=txtHIN_CD__KEY HinKbn=NISUGATA AutoPostBack=true />
    <asp:Panel ID="pnlForCodeSearch__PLNT" runat="server" class="ppp-box-dialog">
        <ui:unitItem ID="PLNT_CD__SEARCH" CtlKind="TXT" runat="server" LabelText="プラントコード"
            Operator="START_WITH" CssClass="ppp-code-selector" />
        <ui:unitItem ID="PLNT_TXT__SEARCH" CtlKind="TXT" runat="server" LabelText="プラントテキスト"
            Operator="LIKE" CssClass="ppp-code-selector" />
        <br style="clear: both;" />
        <br />
        <input id="btnPLNT_CD_SEARCH" type="button" value=" プラント検索 " onclick="codeSearchPlnt.getData('AST_V_JISHO_PLNT', 'PLNT_CD', 'PLNT_TXT','<%= Master.ConnectionName %>')" /><br />
        <select id="lstPlntBox" size="10" style="width: 400px;">
        </select>
    </asp:Panel>
</asp:Content>
