﻿Imports Gears
Imports System.Data

Partial Class _220_ast_SchedulerItemDashBoard
    Inherits GearsPage

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        'データソース設定
        registerMyControl(pnlKeyAttr, New AstMaster.AST_M_HIN(GPageMediator.ConnectionName))
        registerMyControl(pnlSearchPanel)
        registerMyControl(grvPowder, New AstMaster.AST_M_HIN(GPageMediator.ConnectionName))
        registerMyControl(grvNisugata, New AstMaster.AST_M_HIN(GPageMediator.ConnectionName))

        '検索用オブジェクトの設定
        SEARCH_HING.ConnectionName = GPageMediator.ConnectionName

        'イベントハンドラの登録
        AddHandler HIN_CD.getControl(Of TextBox).TextChanged, AddressOf Me.txtHIN_CD_TextChanged

        '非同期更新処理の登録
        'Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)
        'srmOnPage.RegisterAsyncPostBackControl(HIN_CD.getControl(Of TextBox))


    End Sub

    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)

        addRelation(HIN_CD.getControl, pnlKeyAttr)
        addRelation(hdnPARENT_HIN_CD, pnlSearchPanel)

        If Not IsPostBack Then
            setHeader(getQueryString("hin_cd"))
        Else
            setHeader(HIN_CD.getValue)
        End If
        setContent(getSelectedPelet)

    End Sub

    Protected Sub txtHIN_CD_TextChanged(sender As Object, e As System.EventArgs)
        setContent(getSelectedPelet)
        'udpKeyAttr.Update()
    End Sub

    'キー項目を取得し、セットする
    Public Sub setHeader(ByVal hin As String)
        If String.IsNullOrEmpty(hin) Then '値がない場合、処理しない
            Exit Sub
        Else
            '起点コントロールに値をセット
            HIN_CD.setValue(hin)
        End If

        'セットされた品目の値を取得
        executeBehavior(HIN_CD.getControl)
        grvPeletList.DataBind()

        '荷姿品の場合、ソート順メンテを非表示
        'If AstMaster.HIN_KBN.GetHinKbn(HIN_KBN.getValue) = AstMaster.HIN_KBN_LIST.NISUGATA Then
        '    btnSortMnt.Visible = False
        'End If

        initParameter()

    End Sub

    Private Sub initParameter()
        hdnHIN_CD__SEARCH.Value = ""
        hdnHING_CD__SEARCH.Value = ""
        hdnPARENT_HIN_CD__SEARCH.Value = ""
    End Sub

    Protected Sub odsPelet_ObjectCreating(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsPelet.ObjectCreating
        e.ObjectInstance = New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
    End Sub

    Protected Sub odsPelet_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsPelet.Selecting
        Dim dto As New GearsDTO(ActionType.SEL)
        If String.IsNullOrEmpty(hdnJYUGO_GRP_CD.Value) Then
            dto.addFilter(SqlBuilder.newFilter("PARENT_HIN_CD").eq(Nothing))
        Else
            dto.addFilter(SqlBuilder.newFilter("PARENT_HIN_CD").eq(hdnJYUGO_GRP_CD.Value))
        End If
        dto.addFilter(SqlBuilder.newFilter("HIN_KBN").eq(AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.PELET)))
        dto.addSelection(SqlBuilder.newSelect("HIN_CD").ASC.isNoSelect)

        e.InputParameters("data") = dto
    End Sub

    Protected Sub grvPeletList_PageIndexChanged(sender As Object, e As System.EventArgs) Handles grvPeletList.PageIndexChanged
        adjustPageIndex(grvPeletList, grvPeletDetail)
    End Sub

    Protected Sub grvPeletList_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grvPeletList.SelectedIndexChanged
        setContent(getSelectedPelet)

        'スタイル設定
        If grvPeletList.SelectedIndex > -1 AndAlso Not grvPeletList.SelectedRow Is Nothing Then
            grvPeletList.SelectedRow.CssClass = "ppp-table-strong"
            Dim rowCount As Integer = adjustPageIndex(grvPeletList, grvPeletDetail)
            grvPeletDetail.SelectedIndex = rowCount
            grvPeletDetail.SelectedRow.CssClass = grvPeletList.SelectedRow.CssClass
        End If

    End Sub

    Protected Sub grvPeletDetail_PageIndexChanged(sender As Object, e As System.EventArgs) Handles grvPeletDetail.PageIndexChanged
        adjustPageIndex(grvPeletDetail, grvPeletList)

    End Sub
    Protected Sub grvPeletDetail_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grvPeletDetail.SelectedIndexChanged

    End Sub

    Private Function adjustPageIndex(ByRef grvSelectedTable As GridView, ByRef grvTargetTable As GridView) As Integer
        '選択側のコンテンツが表示されるように、片方のページインデックスを調整しカラーリングする
        Dim indexCount As Integer = IIf(grvSelectedTable.SelectedIndex < 0, 0, grvSelectedTable.SelectedIndex)

        If grvSelectedTable.PageIndex > 0 Then
            indexCount += grvSelectedTable.PageSize * grvSelectedTable.PageIndex
        End If

        Dim targetPage As Integer = indexCount \ grvTargetTable.PageSize '商
        If grvTargetTable.PageIndex <> targetPage Then
            grvTargetTable.PageIndex = targetPage
            grvTargetTable.DataBind() '変更後のページをロード
        End If

        Return indexCount Mod grvTargetTable.PageSize 'ページ内の行カウントを返却

    End Function

    Private Sub setContent(ByVal pelet As String)
        'パラメーター初期化
        initParameter()

        '重合Ｇ
        hdnHIN_CD__SEARCH.Value = NVL(hdnJYUGO_GRP_CD.Value)
        getMyControl(grvPowder.ID).init(makeSendMessage(pnlSearchPanel))
        hdnHIN_CD__SEARCH.Value = ""

        'ペレット詳細
        grvPeletDetail.DataBind()

        '荷姿一覧
        hdnPARENT_HIN_CD__SEARCH.Value = NVL(pelet)
        Dim dto As GearsDTO = makeSendMessage(pnlSearchPanel)
        dto.addSelection(SqlBuilder.newSelect("HIN_CD").ASC.isNoSelect)
        getMyControl(grvNisugata.ID).init(dto)

        '生産系列一覧
        makeSSNTable(hdnJYUGO_GRP_CD.Value, pelet)

    End Sub

    Private Sub makeSSNTable(ByVal jyugoGrp As String, ByVal pelet As String)

        If String.IsNullOrEmpty(jyugoGrp) Then
            Exit Sub
        End If

        '銘柄-荷姿の装置情報を取得
        Dim hin As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
        Dim hinInfo As New AstMaster.AST_M_HIN_SOUCHI(GPageMediator.ConnectionName)
        Dim hdto As New GearsDTO(ActionType.SEL)
        Dim orGroup As New SqlFilterGroup("OR")
        hdto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(jyugoGrp).inGroup(orGroup))
        hdto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(pelet).inGroup(orGroup))
        hdto.addFilter(SqlBuilder.newFilter("PARENT_HIN_CD").eq(pelet).inGroup(orGroup))
        hin.setOrder(hdto)
        Dim hinInfoTable As DataTable = hinInfo.gSelect(hdto)

        '代表モデル区分を集約
        Dim modelList As New List(Of String)
        If Not hinInfoTable Is Nothing Then
            Dim query = From row As DataRow In hinInfoTable
                        Select row("MDL_KBN")
            For Each mdl As String In query
                modelList.Add(mdl)
            Next
        End If

        '装置の情報を取得
        Dim souchi As New AstMaster.SOUCHI_CD(GPageMediator.ConnectionName)
        Dim sdto As New GearsDTO(ActionType.SEL)
        For Each mdl As String In modelList
            sdto.addFilter(SqlBuilder.newFilter("MDL_KBN").eq(mdl))
        Next
        Dim souchiTable As DataTable = souchi.gSelect(sdto)

        '最終形のデータテーブルを作成
        Dim ssnTable As New DataTable
        Dim souchiIndex As New Dictionary(Of String, Integer)

        ssnTable.Columns.Add(New DataColumn("品目区分", GetType(System.String)))
        ssnTable.Columns.Add(New DataColumn("品目コード", GetType(System.String)))
        ssnTable.Columns.Add(New DataColumn("品目テキスト", GetType(System.String)))
        ssnTable.Columns.Add(New DataColumn("モデル", GetType(System.String)))
        ssnTable.Columns.Add(New DataColumn("最小補充", GetType(System.String)))
        ssnTable.Columns.Add(New DataColumn("増分補充", GetType(System.String)))
        'ssnTable.Columns.Add(New DataColumn("しわ寄せ", GetType(System.String)))

        For Each dRow As DataRow In souchiTable.Rows
            ssnTable.Columns.Add(New DataColumn(dRow("SOUCHI_CD"), GetType(System.String)))
        Next
        ssnTable.PrimaryKey = New DataColumn() {ssnTable.Columns("品目コード")}

        'データをセット
        Dim nowHIN_CD As String = ""
        Dim nowRow As DataRow = Nothing
        For i As Integer = 0 To hinInfoTable.Rows.Count
            Dim dRow As DataRow = Nothing

            If i < hinInfoTable.Rows.Count Then
                dRow = hinInfoTable.Rows(i)
            End If

            If i = 0 Then '初回処理
                If hinInfoTable.Rows.Count > 0 Then
                    nowRow = ssnTable.NewRow
                    nowHIN_CD = dRow("HIN_CD")
                Else
                    Exit For
                End If
            ElseIf i = hinInfoTable.Rows.Count Then '最終処理
                If Not nowRow Is Nothing Then
                    ssnTable.Rows.Add(nowRow)
                End If
                Exit For

            ElseIf nowHIN_CD <> dRow("HIN_CD") Then '品目コードでのブレイク
                ssnTable.Rows.Add(nowRow)
                nowHIN_CD = dRow("HIN_CD")
                nowRow = ssnTable.NewRow
            End If

            '値セット
            nowRow("品目区分") = dRow("HIN_KBN_TXT")
            nowRow("品目コード") = dRow("HIN_CD")
            nowRow("品目テキスト") = dRow("HIN_TXT")
            nowRow("モデル") = dRow("DHYO_MDL_KBN_TXT")
            nowRow("最小補充") = dRow("MIN_HOJU_SRY")
            nowRow("増分補充") = dRow("ADD_HOJU_SRY")

            If ssnTable.Columns.Contains(dRow("SOUCHI_CD")) Then
                If dRow("MK_FLG") = "0" Then
                    If dRow("DHYO_SOUCHI_FLG") = "1" Then
                        nowRow(dRow("SOUCHI_CD")) = "●"
                    Else
                        nowRow(dRow("SOUCHI_CD")) = "○"
                    End If

                End If
            End If

        Next

        'バインド
        grvSeisan.DataSource = ssnTable
        grvSeisan.DataBind()

    End Sub
    Private Function getSelectedPelet() As String
        Dim result As String = ""
        If Not grvPeletList Is Nothing AndAlso Not String.IsNullOrEmpty(grvPeletList.SelectedValue) Then
            result = grvPeletList.SelectedValue
        Else
            Select Case AstMaster.HIN_KBN.GetHinKbn(HIN_KBN.getValue)
                Case AstMaster.HIN_KBN_LIST.PELET
                    result = HIN_CD.getValue
                Case AstMaster.HIN_KBN_LIST.NISUGATA
                    result = hdnPARENT_HIN_CD.Value
            End Select
        End If
        Return result

    End Function
    Private Function NVL(ByVal value As String, Optional ByVal whenNull As String = "XXXXXXXXX") As String
        If String.IsNullOrEmpty(value) Then
            Return whenNull
        Else
            Return value
        End If

    End Function

End Class
