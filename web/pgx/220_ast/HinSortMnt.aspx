﻿<%@ Page Title="銘柄ソート順メンテナンス" Language="VB" MasterPageFile="~/220_ast/astMaster.master" AutoEventWireup="false" CodeFile="HinSortMnt.aspx.vb" Inherits="_220_ast_HinSortMnt" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ Register src="../parts/220_ast/SCItemSearch.ascx" tagname="sh" tagprefix="search" %>
<%@ MasterType VirtualPath="astMaster.master" %>


<asp:Content ID="Head" ContentPlaceHolderID="pppHead" Runat="Server" ClientIDMode=Static>
    <script type="text/javascript" src="lib/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="lib/FixedColumns.min.js"></script>

    <script language="javascript">
        $(function () {
            if ($("#grvHin").size() > 0) {
                $("#grvHin").dataTable({
                    "bJQueryUI": true,
                    "bPaginate": false,
                    "bSort": true,
                    "bProcessing":true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bAutoWidth": false,
                    "sScrollY": "400px",
                    "sScrollX": "630px",
                    "bScrollCollapse": true,
                    "bRetrieve": true,
                    "oLanguage": {
                        "sSearch": "検索：",
                        "sInfo": " _TOTAL_ 件",
                        "sZeroRecords": "条件に一致するデータはありません",
                        "sInfoEmpty": "データがありません",
                        "sInfoFiltered": " 総件数 _MAX_ 件"
                    },
                    "aoColumns": [
                        { "sWidth": "90px"},
                        { "sWidth": "150px"},
                        { "sWidth": "90px"},
                        { "sWidth": "150px" },
                        { "sWidth": "60px", "sSortDataType": "dom-text", "sType": "numeric"},
                        { "sWidth": "60px", "sSortDataType": "dom-text", "sType": "numeric"},
                        { "bVisible": false },
                        { "bVisible":false },
                        { "bVisible":false },
                        { "bVisible":false }
                    ]
                });
            }
        })
        /* Create an array with the values of all the input boxes in a column */
        $.fn.dataTableExt.afnSortData['dom-text'] = function (oSettings, iColumn) {
            var aData = [];
            $('td:eq(' + iColumn + ') input', oSettings.oApi._fnGetTrNodes(oSettings)).each(function () {
                aData.push(this.value);
            });
            return aData;
        }
        /* Create an array with the values of all the select options in a column */
        $.fn.dataTableExt.afnSortData['dom-select'] = function (oSettings, iColumn) {
            var aData = [];
            $('td:eq(' + iColumn + ') select', oSettings.oApi._fnGetTrNodes(oSettings)).each(function () {
                aData.push($(this).val());
            });
            return aData;
        }

        /* Create an array with the values of all the checkboxes in a column */
        $.fn.dataTableExt.afnSortData['dom-checkbox'] = function (oSettings, iColumn) {
            var aData = [];
            $('td:eq(' + iColumn + ') input', oSettings.oApi._fnGetTrNodes(oSettings)).each(function () {
                aData.push(this.checked == true ? "1" : "0");
            });
            return aData;
        }
        function fncCloseWindow() {
            window.close();
        }
    </script>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="pppPageTitle" Runat="Server" ClientIDMode=Static>
銘柄ソート順メンテナンス
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Label runat="server" ID="lblMsg" Text=""></asp:Label>
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="pppContent" Runat="Server" ClientIDMode=Static>

    <asp:Panel id="pnlHIN" runat="server" CssClass="ppp-include-combo-area" style="width:640px;margin:0px 0px 0px 15px">
         <asp:Panel id="pnlGFilter" runat="server"  Width="100%" > <%-- "GFILTER"をIDに含むこと。 --%>
            <asp:UpdatePanel id="udpHin" runat="server" UpdateMode=Conditional>
                <ContentTemplate>
                    <ui:unitItem ID="DHYO_MDL_KBN" CtlKind="DDL" runat="server" LabelText="モデル区分" />
                    <ui:unitItem ID="PARENT_HIN_CD" CtlKind="TXT" runat="server" LabelText="親重合Ｇコード" SearchAction="SEARCH_POWDER.openDialog()"　Operator="START_WITH" AutoPostBack=True />
                    <ui:unitItem ID="PARENT_HIN_TXT" CtlKind="TXT" runat="server" LabelText="親重合Ｇ名称" Operator="LIKE" Width="200"/>
                    <asp:HiddenField ID="hdnHIN_KBN" runat="server" Value="" />
                    <asp:HiddenField ID="hdnMK_FLG" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnEXHIN_KBN" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnSCHEDULER_FLG" runat="server" Value="1" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel runat="server"  style="width:570px">
            <div style="float:right">
                <asp:Button id="btnSearch" runat="server" Text="　検索　" />
            </div>
            <br style="clear:both">
        </asp:Panel>
        <br style="clear:both">
        <asp:Panel id="pnlData" runat="server" style="width:640px;">
            <asp:GridView id="grvHin" runat="server"
                CssClass="ppp-table" DataKeyNames="HIN_CD" 
                HeaderStyle-CssClass="ppp-table-head" 
                EnableViewState=true AllowPaging=false AutoGenerateColumns=False 
                AutoGenerateSelectButton="False" AutoGenerateEditButton="false" AutoGenerateDeleteButton="False"
                RowStyle-CssClass="odd" >
               <AlternatingRowStyle CssClass="even" />
            <Columns>
                    <asp:BoundField DataField="PARENT_HIN_CD" HeaderText="親重合Ｇコード">
                    </asp:BoundField>
                    <asp:BoundField DataField="PARENT_HIN_TXT" HeaderText="親重合Ｇ名称">
                    </asp:BoundField>
                    <asp:BoundField DataField="HIN_CD" HeaderText="品目コード">
                    </asp:BoundField>
                    <asp:BoundField DataField="HIN_TXT" HeaderText="品目テキスト">
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="ソート順" HeaderStyle-Width="60" HeaderStyle-HorizontalAlign="Center" >
                        <ItemTemplate> 
                            <asp:TextBox ID="txtHIN_SORT" runat="server" Text='<%# Eval("HIN_SORT") %>' CssClass="gears-GMatch_Pattern_^[0-9]{0,4}$" Width=55 style="text-align:right" >
                            </asp:TextBox> 
                        </ItemTemplate> 
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="生産順" HeaderStyle-Width="60" HeaderStyle-HorizontalAlign="Center" >
                        <ItemTemplate> 
                            <asp:TextBox ID="txtPRT_SSN_ORDER" runat="server" Text='<%# Eval("PRT_SSN_ORDER") %>' CssClass="gears-GNumeric gears-GByteLength_Length_2" Width=55 style="text-align:right" >
                            </asp:TextBox> 
                        </ItemTemplate> 
                    </asp:TemplateField>
                    <asp:BoundField DataField="UPD_YMD" HeaderText="UPD_YMD">
                    </asp:BoundField>
                    <asp:BoundField DataField="UPD_HMS" HeaderText="UPD_HMS">
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="PREV_HIN_SORT" HeaderStyle-Width="60" HeaderStyle-HorizontalAlign="Center" >
                        <ItemTemplate> 
                            <asp:TextBox ID="txtPREV_HIN_SORT" runat="server" Text='<%# Eval("HIN_SORT") %>' Visible="false" >
                            </asp:TextBox> 
                        </ItemTemplate> 
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PREV_PRT_SSN_ORDER" HeaderStyle-Width="60" HeaderStyle-HorizontalAlign="Center" >
                        <ItemTemplate>
                            <asp:TextBox ID="txtPREV_PRT_SSN_ORDER" runat="server" Text='<%# Eval("PRT_SSN_ORDER") %>' Visible="false" >
                            </asp:TextBox> 
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <br style="clear:both">
            <asp:Button ID="btnUpdate" runat="server" Text=" 保存 " />&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnClose" runat="server" Text="　閉じる　" />
        <br style="clear:both">
        <br style="clear:both">
	    <asp:Label ID="lblLog" runat="server" Text="" CssClass="ppp-msg success"></asp:Label>
    </asp:Panel>
</asp:Content>

<asp:Content ID="Footer" ContentPlaceHolderID="pppFooter" Runat="Server" ClientIDMode=Static>
    <search:sh ID="SEARCH_POWDER" runat="server" TgtControl=txtPARENT_HIN_CD HinKbn=POWDER  AutoPostBack=true />
    <asp:HiddenField ID="hdnUpdFunc" Value="" runat="server" />
</asp:Content>
