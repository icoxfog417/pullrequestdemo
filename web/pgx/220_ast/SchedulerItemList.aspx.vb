﻿Imports Gears

''' ----------------------------------------------------------------------------------------
''' <summary>
''' スケジューラ品目一覧
''' </summary>
''' <histry>
''' 2012/05/16  [TIS 久保]    Created
''' 2013/11/18  [TIS 坂本]    Excel2010対応と一括アップロード用に出力レイアウト修正
''' </histry>
''' ----------------------------------------------------------------------------------------
Partial Class _220_ast_SchedulerItemList
    Inherits GearsPage

    Private gvUtil As GridViewUtility

    'スイッチ用のプロパティ
    Private _isDisplayUpper As Boolean = True
    Public Property IsDisplayUpper() As Boolean
        Get
            Return _isDisplayUpper
        End Get
        Set(ByVal value As Boolean)
            _isDisplayUpper = value
        End Set
    End Property

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init

        registerMyControl(pnlGFILTER)

        'イベントハンドラの登録
        AddHandler VKORG.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlVKORG_SelectedIndexChanged
        AddHandler DHYO_MDL_KBN.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlMDL_KBN_SelectedIndexChanged

        '非同期更新処理の登録
        Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)
        srmOnPage.RegisterAsyncPostBackControl(VKORG.getControl(Of DropDownList))
        srmOnPage.RegisterAsyncPostBackControl(DHYO_MDL_KBN.getControl(Of DropDownList))

        gvUtil = New GridViewUtility(grvHin, lblState)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        setEscapesWhenSend(pnlGFILTER, Nothing, SORT_KIND.ControlId)

        addRelation(VKORG.getControl(Of DropDownList), HING1_CD.getControl(Of DropDownList))
        addRelation(DHYO_MDL_KBN.getControl(Of DropDownList), DHYO_SOUCHI_CD.getControl(Of DropDownList))

        'メッセージ消す用
        btnSearch.Attributes("onclick") = "return clearMsg();"
        btnListOut.Attributes("onclick") = "return clearMsg();"
        lblMsg.Text = ""
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        lblMsg.Text = ""
        grvHin.PageIndex = 0
        pnlFloat.Style.Remove("display") '表示開始
        IsDisplayUpper = False
    End Sub
    Protected Sub ddlVKORG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        executeBehavior(VKORG.getControl(Of DropDownList))
        udpHING1_CD.Update()
    End Sub
    Protected Sub ddlMDL_KBN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        executeBehavior(DHYO_MDL_KBN.getControl(Of DropDownList))
        udpSOUCHI_CD.Update()
    End Sub

    Protected Sub odsHin_ObjectCreating(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsHin.ObjectCreating
        e.ObjectInstance = New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
    End Sub

    Protected Sub odsHin_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsHin.Selecting
        e.InputParameters("data") = makeSelectingDto()
    End Sub

    Protected Sub grvHin_PageIndexChanged(sender As Object, e As System.EventArgs) Handles grvHin.PageIndexChanged
        IsDisplayUpper = False
    End Sub

    Protected Sub grvHin_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvHin.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'スタイルセット
            Dim hinKbn As String = DataBinder.Eval(e.Row.DataItem, "HIN_KBN").ToString()
            If Not String.IsNullOrEmpty(hinKbn) Then
                Select Case AstMaster.HIN_KBN.GetHinKbn(hinKbn)
                    Case AstMaster.HIN_KBN_LIST.POWDER 'パウダー
                        e.Row.CssClass = "ppp-table-strong"
                    Case AstMaster.HIN_KBN_LIST.PELET 'ペレット
                        e.Row.CssClass = "ppp-table-even"
                    Case Else '包装品/荷姿品
                        'なし
                End Select

            End If
        End If

    End Sub

    Protected Sub btnListOut_Click(sender As Object, e As System.EventArgs) Handles btnListOut.Click
        IsDisplayUpper = True
        Try
            Using scList As AstMaster.XlsTemplateSchedulerItem = New AstMaster.XlsTemplateSchedulerItem(GPageMediator.ConnectionName)
                Dim dtCount As Integer = scList.expandSchedulerItem(makeSelectingDto)
                If dtCount = 0 Then
                    lblMsg.Text = "データがありません"
                Else
                    scList.Write("ＳＣ品目一覧.xlsx")
                End If
            End Using
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try

    End Sub
    Private Function makeSelectingDto() As GearsDTO
        Dim ds As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
        Dim sendObj As GearsDTO = makeSendMessage(pnlGFILTER)
        ds.setOrder(SORT_KIND.getValue, sendObj)
        Return sendObj

    End Function

End Class
