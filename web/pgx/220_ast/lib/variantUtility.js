﻿//使用方法
/*
・前提条件
　1.jQuery・bgiframe・jQuery-uiのjavaScriptファイルを先行で読み込んでください(jQuery-uiはスタイルシートも)
    ※bgiframeはIE特有のドロップダウンリストが強制的に前面に出る現象の対策です。今後不要になる可能性があります
　2.ToolkitScriptManagerもしくはScriptManagerを使用し、VariantDataService.asmxのサービスが使用可能な状態にしてください
　3.保存対象とするコントロールは、inputタイプであり、id属性が設定されている必要があります

・クライアント側設定
　1.バリアントを保存するためのボタン・呼び出すためのボタンの2種類を作成します。
　　前者はonclickにvcollect(ユーザーID)・後者はvselect(ユーザーID)を設定することで使用可能になります。
    ※ASPのフォームタグ中では、postbackすると画面が更新されポップアップが消えてしまうため、"return vcollect(ユーザーID)"
    にするなどしてpostbackを防止する必要があります

・動作概要
　1.input属性を収集し、その設定値をXML化してDBに保存します(保存する際はBASE64エンコード化)。
　　※classに"v-unselect"が設定されているものは選択対象外としています。条件として保存したくないパラメータがあればこのクラスを設定
　　　してください
　2.適用時には保存した値を読み直し、各画面パラメータに設定しています。

・注意事項
　1.hidden・passwordは保存対象外となります。また、fileについてはフィールドへの読取/書込が不可能であるため
　　対象外としています。
　2.保存/呼出の画面はbody内に動的に書き込まれます。同じく動的な書込みを使用している場合は注意して下さい。
　3.id/クラス名として「v-」始まりの文字列を使用しています。画面上で「v-」で始まるid/クラス名を使用している場合は
　　重複がないかご確認ください。

*/

//定数
var indialog = "<div id=\"v-indialog\" title=\"選択条件保存\" style=\"font-size: 90%;\">"
indialog += "<table class=\"ui-widget\" >";
indialog += "<tr><td>営業員番号</td><td><input id=\"v-user\" type=\"text\" value=\"v-replaceUser\"></input></td></tr>";
indialog += "<tr><td>バリアント名</td><td><input id=\"v-variant\" type=\"text\"></input></td></tr>";
indialog += "<tr><td>公開範囲</td><td><select id=\"v-security\">";
indialog += "<option value=\"9\">自分のみ</option>";
indialog += "<option value=\"2\">営業G内公開</option>";
indialog += "<option value=\"1\">事業部内公開</option>";
indialog += "<option value=\"0\">全社公開</option>";
indialog += "</select></td></tr>";
indialog += "</table><br/><b id=\"v-emsg\" style=\"color:red;\"></b><b id=\"v-smsg\" style=\"color:blue;\"></b>";
indialog += "</div>";

var seldialog = "<div id=\"v-seldialog\" title=\"選択条件呼出\" style=\"font-size: 90%;\">"
seldialog += "営業員番号:<input id=\"v-user\" type=\"text\" value=\"v-replaceUser\"></input><input type=\"button\" onclick=\"makeSelTab()\" value=\"一覧取得\"><br><br>"
seldialog += "<div overflow-y:scroll; ><table class=\"ui-widget\" width=\"100%\">"
seldialog += "<thead id=\"v-selecttabh\" class=\"ui-widget-header\"></thead>"
seldialog += "<tbody id=\"v-selecttabb\" class=\"ui-widget-content\"></tbody></table></div>"
seldialog += "<br/><b id=\"v-emsg\" style=\"color:red;\"></b><b id=\"v-smsg\" style=\"color:blue;\"></b>"
seldialog += "</div>"


//バリアント呼出-------------------------------------------------------------------------------
function vcollect(puser,vwidth,vheight) {
    //呼出側のコールを不可能にする
    if ($("#v-seldialog").dialog("isOpen")) {
        $("#v-seldialog").dialog("close");
    }
    //既に開いている場合閉じる
    if ($("#v-indialog").dialog("isOpen")) {
        $("#v-indialog").dialog("close");
    }

    var dwidth = 400;
    var dheight = 300;
    //幅の指定がある場合
    if (arguments.length > 1) {
        dwidth = vwidth;
    }
    //高さの指定がある場合
    if (arguments.length > 2) {
        dheight = vheight;
    }

    var fname = getFileName();

    var xml = "";
    $(":input:not(.v-unselect)").each(
                function () {
                    if ($(this).attr("id") !== undefined ) {
                        var ptype = $(this).attr("type").toUpperCase();
                        if ($(this).get(0).tagName == "TEXTAREA") {
                            ptype = "TEXT";
                        } else if ($(this).get(0).tagName == "SELECT") {
                            ptype = "SELECT";
                        }

                        switch (ptype) {
                            case "TEXT":
                            case "SELECT":
                                xml += makeTagStr($(this).attr("id"), ptype, $(this).val())
                                break;
                            case "CHECKBOX":
                            case "RADIO":
                                xml += makeTagStr($(this).attr("id"), ptype, $(this).attr("checked"))
                                break;
                            default: //password・hiddenなどは対象外.fileはそもそも読取/書込みがムリなので対象外
                                break;

                        }


                    }
                }
            );

    xml = "<VARIANT>" + xml + "</VARIANT>";

    indialog = indialog.replace("v-replaceUser", puser);
    $("body").append(indialog);
    var dlg = $("#v-indialog").bgiframe();
    dlg.dialog({
        width: dwidth,
        height: dheight,
        close: function () { $("#v-indialog").remove(); },
        buttons: {
            "キャンセル": function () {
                $(this).dialog("close");
                $("#v-indialog").remove();
            },
            "登録": function () {
                if ($("#v-user").val() != "" && $("#v-variant").val() != "") {
                    clearMsg();
                    //サービス呼出
                    VariantDataService.createVariant($("#v-user").val(),
                                                             $("#v-variant").val(),
                                                             $("#v-security").val(),
                                                             fname,
                                                             xml,
                                                             function (result, cx, name) {
                                                                 readMsg(result);
                                                             },
                                                             function (ext, cx, name) { $("#v-emsg").text("更新に失敗しました"); }
                                                             )
                } else {
                    $("#v-emsg").text("全ての項目を入力してください");
                }
            }
        }
    });

}
function makeTagStr(id, type, value) {
    return "<item id=\"" + id + "\" type=\"" + type + "\">" + value + "</item>";

}

//バリアント表示-------------------------------------------------------------------------------
function vselect(puser,vwidth,vheight) {
    //保存側のコールを不可能にする
    if ($("#v-indialog").dialog("isOpen")) {
        $("#v-indialog").dialog("close");
    }
    //既に開いている場合閉じる
    if ($("#v-seldialog").dialog("isOpen")) {
        $("#v-seldialog").dialog("close");
    }

    var dwidth = 500;
    var dheight = 400;
    //幅の指定がある場合
    if (arguments.length > 1) {
        dwidth = vwidth;
    }
    //高さの指定がある場合
    if (arguments.length > 2) {
        dheight = vheight;
    }

    seldialog = seldialog.replace("v-replaceUser", puser);
    $("body").append(seldialog);
    var dlg = $("#v-seldialog");
    dlg.bgiframe();
    dlg.dialog({
        width: dwidth,
        height: dheight,
        close: function(){ $("#v-seldialog").remove(); },
        buttons: {
            "キャンセル": function () {
                $(this).dialog("close");
                $("#v-seldialog").remove();
            }
        }
    })
}

function makeSelTab() {
    var tableStr = ""
    var fname = getFileName();

    if ($("#v-user").val() != "") {
        clearMsg();
        VariantDataService.readVariant($("#v-user").val(), fname,
                                                function (result, cx, name) {
                                                    writeTable(result);
                                                },
                                                function (ext, cx, name) {
                                                    $("#v-emsg").text("更新に失敗しました");
                                                }
                                                );
    }

}

function writeTable(result) {
    var tablehead = "<tr>"
    tablehead += "<td>バリアント名</td>"
    tablehead += "<td>登録ユーザー</td>"
    tablehead += "<td>登録日</td>"
    tablehead += "<td>呼出</td>"
    tablehead += "<td>削除</td></tr>"

    var tableBody = ""
    //xmlの読み取り
    $(result).find("VARIANT").each(function () {
        var line = ""
        line += "<tr>";
        line += "<td >" + $(this).attr("name") + "</td>";
        line += "<td >" + $(this).attr("uname") + "</td>";
        line += "<td >" + $(this).attr("cdate") + "</td>";
        line += "<td ><input type=\"button\" value=\"呼出\" onclick=\"callVariant('" + $(this).attr("key") + "')\"></td>";
        line += "<td ><input type=\"button\" value=\"削除\" onclick=\"deleteVariant('" + $(this).attr("key") + "')\"></td>";
        line += "</tr>";
        tableBody += line;
    });

    $("#v-selecttabh").empty();
    $("#v-selecttabb").empty();

    $("#v-selecttabh").append(tablehead);
    $("#v-selecttabb").append(tableBody);

}

//バリアント呼出/適用--------------------------------------------------------------------------
function callVariant(key) {
    if ($("#v-user").val() != "") {
        clearMsg();
        VariantDataService.defreezeSelection(key,
                                                    function (result, cx, name) { applyVariant(result) },
                                                    function (ext, cx, name) { $("#v-emsg").text("呼出に失敗しました"); }
                                                    )
    }

}

function applyVariant(result) {
    $(result).find("item").each(function () {
        var id = $(this).attr("id");
        var ptype = $(this).attr("type");
        var value = $(this).text();

        switch (ptype) {
            case "TEXT":
                $("#" + id).val(value)
                break;
            case "SELECT":
                if (value == "null") {
                    $("#" + id).val(null);
                } else {
                    var selArray = value.split(",");
                    $("#" + id).val(selArray);
                }
                break;
            case "CHECKBOX":
            case "RADIO":
                if (value == "true") {
                    $("#" + id).attr("checked", true);
                } else {
                    $("#" + id).attr("checked", false);
                }
                break;
            default: //password・hiddenなどは対象外.fileはそもそも読取/書込みがムリなので対象外
                break;

        }
    });

}

//バリアント削除--------------------------------------------------------------------------

function deleteVariant(key) {
    if ($("#v-user").val() != "") {
        clearMsg();
        VariantDataService.deleteVariant(key, $("#v-user").val(),
                                                    function (result, cx, name) {
                                                        makeSelTab();
                                                        readMsg(result);

                                                    },
                                                    function (ext, cx, name) { $("#v-emsg").text("削除に失敗しました"); }
                                                );
    }
}

//汎用関数-------------------------------------------------------------------------------
function getFileName() {
    var fname = location.pathname;
    fname = fname.substring(fname.lastIndexOf('/') + 1, fname.length);
    return fname;
}


function readMsg(result) {
    if ($(result).find("RESULT").attr("STATUS") == "S") {
        $("#v-smsg").text($(result).find("RESULT").text());
    } else {
        $("#v-emsg").text($(result).find("RESULT").text());
    }

}

function clearMsg() {
    $("#v-emsg").text("");
    $("#v-smsg").text("");
}

