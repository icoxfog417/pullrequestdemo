﻿
Partial Class _220_ast_astSwitchItemMaster
    Inherits System.Web.UI.MasterPage

    '継承元プロパティのオーバーライド
    Public Property DisplayMenu() As Boolean
        Get
            Return Master.DisplayMenu
        End Get
        Set(ByVal value As Boolean)
            Master.DisplayMenu = value
        End Set
    End Property

    Public Property LoginUser() As String
        Get
            Return Master.LoginUser
        End Get
        Set(ByVal value As String)
            Master.LoginUser = value
        End Set
    End Property

    Public Property ConnectionName() As String
        Get
            Return Master.ConnectionName
        End Get
        Set(ByVal value As String)
            Master.ConnectionName = value
        End Set
    End Property

    Public Property DsNameSpace() As String
        Get
            Return Master.DsNameSpace
        End Get
        Set(ByVal value As String)
            Master.DsNameSpace = value
        End Set
    End Property

    Public ReadOnly Property ColDefineTable() As String
        Get
            Return Master.ColDefineTable
        End Get
    End Property

    Public ReadOnly Property isPRM01 As Boolean
        Get
            Return Master.isPRM01
        End Get
    End Property
    Public ReadOnly Property isMDP01 As Boolean
        Get
            Return Master.isMDP01
        End Get
    End Property
    Public ReadOnly Property isMCI_ELA As Boolean
        Get
            Return Master.isMCI_ELA
        End Get
    End Property
    Public ReadOnly Property isMCI_SWP As Boolean
        Get
            Return Master.isMCI_SWP
        End Get
    End Property

    'スイッチ用のプロパティ
    Private _isDisplayUpper As Boolean = True
    Public Property IsDisplayUpper() As Boolean
        Get
            Return _isDisplayUpper
        End Get
        Set(ByVal value As Boolean)
            _isDisplayUpper = value
        End Set
    End Property

End Class

