﻿//コード検索を行うためのクラス jQuery uiのダイアログを使用してコード検索を実施する
var CodeSearcher = function (dialogF, selectL, targetA) {

    //コンストラクタ(メンバ変数に値をセット)
    this.dialogField = dialogF;
    this.selectList = selectL;
    this.targetArea = targetA;
    this.selectedCallback = null;
    this.additionalControl = new Array();

    //メソッド
    //選択エリアの値を初期化
    this.clearSelection = function () {
        //$("#" + this.selectList).empty(); IE6でうまく動作しないため、直接JavaScript対応
        document.getElementById(this.selectList).options.length = 0;

    }

    //ダイアログオープン
    this.openDialog = function () {
        $("#" + this.dialogField).dialog("open");
    }

    //ダイアログ設定
    this.setDialogOption = function (option) {
        $("#" + this.dialogField).dialog(option);
    }

    //値設定先の変更
    this.setTarget = function (target) {
        this.targetArea = target;
    }

    //選択後のコールバック(セットしたコントロール名を引数に取る)
    this.setSelectedCallback = function (callback) {
        this.selectedCallback = callback;
    }

    //ターゲットエリア以外のコントロール読み込み
    this.setAdditionalControl = function (item) {
        if (item instanceof Array) {
            this.additionalControl = item
        } else {
            this.additionalControl = new Array(item);
        }
    }

    //ダイアログ設定処理(bgiframeを使用)
    this.initDialog = function (titleVal, widthVal, heightVal, delimVal) {
        var title = "";
        var width = 600;
        var height = 500;
        var delimi = "\n";

        //初期値設定
        for (var i = 0; i < arguments.length; i++) {
            switch (i) {
                case 0:
                    title = arguments[i];
                    break;
                case 1:
                    width = arguments[i];
                    break;
                case 2:
                    height = arguments[i];
                    break;
                case 3:
                    delimi = arguments[i];
                    break;
            }
        }

        //ダイアログ設定
        var me = this;
        var dialog = $("#" + this.dialogField); //IE6対応
        dialog.bgiframe();
        dialog.dialog({
            title: title,
            autoOpen: false,
            width: width,
            height: height,
            buttons: {
                "選択": function () {
                    me.transferSelection(delimi);
                    $(this).dialog("close");
                    if (me.selectedCallback != null) {
                        me.selectedCallback(me.targetArea);
                    }

                },
                "閉じる": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                me.clearSelection();
            }

        })

        //フォント設定(等幅フォントにしないとスペースで大きさを調整できない)
        $("#" + this.selectList).css("font-family", "ＭＳ ゴシック")

    }

    //選択したリストから、ターゲットのテキストエリアに値を移す(deliminatorで区切り記号を指定可能)
    this.transferSelection = function (delim) {
        //区切り記号を設定
        var deliminator = "\n"
        if (arguments.length > 0) {
            deliminator = delim;
        }

        //値を伝送->リストのフォームコントロールから値を取得し、テキストエリアに値をセット
        var selection = $("#" + this.selectList).val();
        var text = ""
        if (selection != null) {
            if (selection instanceof Array) {
                for (var i = 0; i < selection.length; i++) {
                    if (i == selection.length - 1) {
                        text += selection[i]; //最後は末尾にdeliminatorをつけない
                    } else {
                        text += selection[i] + delim;
                    }
                }
            } else {
                text = selection;
            }
            $("#" + this.targetArea).val(text);
        }

    }

    //コード値及びテキストを指定されたテーブルから取得する　※サービスを使用する
    this.getData = function (table, code, text, con) {
        //var testXml = "<SELECT TARGET=\"CPD_M_MEIGARA\" SCHEMA=\"SCM\"><COL FILTER=\"BGPB00\" OPERATOR=\"LIKE\" ORDER=\"ASC\" GROUP=\"TRUE\" AS=\"HING\" >HING_CD</COL><COL GROUP=\"TRUE\" >HING_TXT</COL><COL FILTER=\"0\" NOSELECT=\"TRUE\" >MK_FLG</COL></SELECT>"
        //コード分の25バイトを確保
        var padding = "|------------------------"
        var xml = ""
        var selection = ""
        var listTarget = this.selectList;
        var constr = ""
        if (arguments.length > 3) {
            constr = " CONSTR=\"" + con + "\""
        }

        var self = this;
        $("#" + this.dialogField).find(".ppp-code-selector").each(function () {
            //dialog内のコントロールを検索し、サービスに伝送するためのデータを作成する
            selection += self.makeColumnXml($(this))
        })
        //追加分のコントロール読み込み
        for (var i = 0; i < this.additionalControl.length; i++) {
            selection += this.makeColumnXml($("#" + this.additionalControl[i]))
        }

        xml = "<SELECT TARGET=\"" + table + "\" " + constr + " >" + selection + "</SELECT>"
        //alert(xml);
        DBDataService.getDBData(xml,
                                    function (result, cx, name) {
                                        $("#" + listTarget).empty();

                                        $(result).find("ROW").each(function () {
                                            var codeVal = $(this).find("COL[NAME=\"" + code + "\"]").text();

                                            var paddingCount = padding.length - (codeVal + padding).substring(0, padding.length).lastIndexOf("|");
                                            var paddingVal = ""
                                            for (var i = 0; i < paddingCount; i++) {
                                                paddingVal += "&nbsp;";
                                            }

                                            var textVal = codeVal + paddingVal + $(this).find("COL[NAME=\"" + text + "\"]").text();
                                            $("#" + listTarget).append("<option value=\"" + codeVal + "\">" + textVal + "</option>")

                                        })
                                        //alert(test);
                                    },
                                    function (ext, cx, name) { alert(ext.get_message()); })

    }

    this.makeColumnXml = function (jqObj) {
        var operand = ""
        var colxml = ""
        if (jqObj !== undefined && jqObj != null) {
            if (jqObj.attr("Operator") !== undefined) {
                operand = "OPERATOR=\"" + jqObj.attr("Operator") + "\" "
            }
            var endIndex = jqObj.attr("id").indexOf("__")
            if (endIndex < 0) {
                endIndex = jqObj.attr("id").length;
            }
            var val = jqObj.val();
            if (jqObj.css("text-transform") == "uppercase") {
                val = val.toUpperCase();
            } else if (jqObj.css("text-transform") == "lowercase") {
                val = val.toLowerCase();
            }

            var colxml = "<COL FILTER=\"" + val + "\" " + operand + " ORDER=\"ASC\">" + jqObj.attr("id").substring(3, endIndex) + "</COL>";
        }
        return colxml

    }


}