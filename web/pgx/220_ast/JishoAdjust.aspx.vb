﻿Imports Gears
Imports System.Data

Partial Class _220_ast_JishoAdjust
    Inherits GearsPage

#Region "クラス変数"

    Private targetYMTableName As SqlDataSource = SqlBuilder.newDataSource("AST_V_JISHO_YM")
    Private hinTableName As SqlDataSource = SqlBuilder.newDataSource("AST_V_HIN")
    Private plntTableName As SqlDataSource = SqlBuilder.newDataSource("AST_V_JISHO_PLNT")

    Private jishoDataSource As AstMaster.AST_D_JISHO_ADJUST = Nothing
    Private targetYMDataSource As GDSTemplate = Nothing
    Private hinDataSource As GDSTemplate = Nothing
    Private plntDatasource As GDSTemplate = Nothing

#End Region

#Region "イベント"

#Region "ページ"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        jishoDataSource = New AstMaster.AST_D_JISHO_ADJUST(GPageMediator.ConnectionName)
        targetYMDataSource = New GDSTemplate(GPageMediator.ConnectionName, targetYMTableName)
        hinDataSource = New GDSTemplate(GPageMediator.ConnectionName, hinTableName)
        plntDatasource = New GDSTemplate(GPageMediator.ConnectionName, plntTableName)

        jishoDataSource.addLockCheckCol("UPD_YMD", LockType.UDATESTR)
        jishoDataSource.addLockCheckCol("UPD_HMS", LockType.UTIMESTR)

        'モデル検証クラスの設定
        Dim jishoValidator As New JishoValidator(GPageMediator.ConnectionName)
        jishoDataSource.ModelValidator = jishoValidator

        'フィルタ用のコントロールを登録
        registerMyControl(pnlGFilter)

        '一覧表示用のコントロール/データソースを登録
        registerMyControl(grvJisho) 'ページング処理GridViewは手動でデータソースとの紐付けが必要のため、ここでは書かない

        '編集用のコントロール/データソースを登録
        registerMyControl(pnlGFORM, jishoDataSource)

        '年月保持用のコントロール/データソースを登録
        registerMyControl(pnlJISHO_YM, targetYMDataSource)

        registerMyControl(pnlKeyItem)
        registerMyControl(pnlHin, hinDataSource)
        registerMyControl(pnlPlnt, plntDatasource)

        'updatePanel用にイベントハンドラの登録
        AddHandler HIN_CD__KEY.getControl(Of TextBox).TextChanged, AddressOf Me.txtHIN_CD__KEY_TextChanged
        AddHandler PLNT_CD__KEY.getControl(Of TextBox).TextChanged, AddressOf Me.txtPLNT_CD__KEY_TextChanged

        'updatePanel更新用のコントロールのプロパティ設定
        HIN_CD__KEY.getControl(Of TextBox).AutoPostBack = True
        PLNT_CD__KEY.getControl(Of TextBox).AutoPostBack = True

        '品目検索ユーザコントロールにConnectionNameを設定
        SEARCH_NSGT_1.ConnectionName = GPageMediator.ConnectionName


    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ここで行うこと
        '①：関連を定義（A→B：Aが変更されたらBに変更内容を通知し、Bを変更する）
        '　　（executeBehaviorを利用する為に必要）
        '②キー項目の設定
        ' ※②についてはコントロールそのもののIDに[__KEY]を追加すればよいようになったため、ここでは設定不要になった

        ' ◆関連定義◆ --------------------------------------------------------------------

        '検索条件パネルの内容で、品目一覧テーブルを表示
        '（こう書けばパネル内の項目すべてが対象[=検索条件に使用される]になる）
        addRelation(pnlGFilter, grvJisho)

        '品目一覧テーブル内の行の「選択」ボタンクリックで、編集パネルにその品目のデータを表示
        addRelation(grvJisho, pnlGFORM)

        '画面リフレッシュ用：キー値(年月＋品目コード＋プラントコード)で編集パネルにそのキー値のデータを表示
        addRelation(pnlKeyItem, pnlGFORM)

        '画面リフレッシュ用：品目コード値で、品目コード＋品目名称のパネルにデータを表示
        addRelation(HIN_CD__KEY.getControl, pnlHin)

        '画面リフレッシュ用：プラントコード値で、プラントコード＋プラント名称のパネルにデータを表示
        addRelation(PLNT_CD__KEY.getControl, pnlPlnt)


        '編集パネルの内容を編集して「更新」すると、自身の編集パネルの内容を更新
        '（明示的に書く必要あり、但し単一コントロール→単一コントロールの関連は効かない。パネルなど内部にコントロールを含む場合のみ有効）
        addRelation(pnlGFORM, pnlGFORM)

        '以下、上でFrom-To関連を定義したコントロールのうち、To側に通知する必要のないコントロールを除外、
        'もしくはFrom側に入っていない固定値などを追加で通知したい場合に記述

        'このコントロールは通知対象からはずしたい、という場合このように宣言
        '（この場合、発行されるupdate文のSETの対象にしないよう宣言が必要）
        setEscapesWhenSend(pnlGFORM, pnlGFORM, {HIN_TXT__GCON.ControlId, PLNT_TXT__GCON.ControlId _
                                                , lblSRY01__GCON.ID, lblSRY02__GCON.ID, lblSRY03__GCON.ID, lblSRY04__GCON.ID, lblSRY05__GCON.ID _
                                                , lblSRY06__GCON.ID, lblSRY07__GCON.ID, lblSRY08__GCON.ID, lblSRY09__GCON.ID, lblSRY10__GCON.ID _
                                                , lblSRY11__GCON.ID, lblSRY12__GCON.ID, lblSRY13__GCON.ID, lblSRY14__GCON.ID, lblSRY15__GCON.ID _
                                                , txtDIFF_SRY01.ID, txtDIFF_SRY02.ID, txtDIFF_SRY03.ID, txtDIFF_SRY04.ID, txtDIFF_SRY05.ID _
                                                , txtDIFF_SRY06.ID, txtDIFF_SRY07.ID, txtDIFF_SRY08.ID, txtDIFF_SRY09.ID, txtDIFF_SRY10.ID _
                                                , txtDIFF_SRY11.ID, txtDIFF_SRY12.ID, txtDIFF_SRY13.ID, txtDIFF_SRY14.ID, txtDIFF_SRY15.ID _
                                                , txtADJUSTED_SRY01.ID, txtADJUSTED_SRY02.ID, txtADJUSTED_SRY03.ID, txtADJUSTED_SRY04.ID, txtADJUSTED_SRY05.ID _
                                                , txtADJUSTED_SRY06.ID, txtADJUSTED_SRY07.ID, txtADJUSTED_SRY08.ID, txtADJUSTED_SRY09.ID, txtADJUSTED_SRY10.ID _
                                                , txtADJUSTED_SRY11.ID, txtADJUSTED_SRY12.ID, txtADJUSTED_SRY13.ID, txtADJUSTED_SRY14.ID, txtADJUSTED_SRY15.ID})

        'キー項目の設定
        'getMyControl(HIN_CD__1.ControlId).setAskey() -- コントロールそのもののIDで定義づけできるため不要（例）HIN_CD__KEY


        'メッセージ等画面項目の初期化
        lblMsg.Text = ""
        lblMsg.CssClass = ""
        HIN_TXT__GCON.getControl.CssClass = ""
        PLNT_TXT__GCON.getControl.CssClass = ""

        'テーブルデータのロード
        If Not IsPostBack Then

            '検索条件パネルをオープン状態に
            hdnMode.Value = "S"

            'ロード時に起点の年月を取得、表示する
            addRelation(pnlJISHO_YM, pnlJISHO_YM)
            executeBehavior(pnlJISHO_YM)
            setYMtoTblJISHO()

            '検索フォームの値でグリッドビューを表示
            grvJisho.PageIndex = 0 'ページ選択初期化
            grvJisho.DataBind()

        End If

    End Sub


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If grvJisho.Rows.Count = 0 Then
            lblSummaryDataHelp.Visible = False
        Else
            lblSummaryDataHelp.Visible = True
        End If


        If String.IsNullOrWhiteSpace(hdnSAKUTEI_YM__KEY.Value) Then
            hdnSAKUTEI_YM__KEY.Value = hdnJISHO_YM.Value
        End If

    End Sub


#End Region

#Region "ボタン"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        '検索条件パネルをオープン状態に
        hdnMode.Value = "S"

        lblCount.Text = String.Empty

        'フィルタの値で一覧を更新する(ページング対応用)　→ DataBindを実行するとSelectingのイベントが発生する
        grvJisho.PageIndex = 0 'ページ選択初期化
        grvJisho.DataBind()

    End Sub


    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        '編集フォームパネルをオープン状態に
        hdnMode.Value = "U"

        '更新者を設定
        hdnUPD_USR.Value = Master.LoginUser

        'データ登録/更新
        Dim bolResult As Boolean = executeBehavior(pnlGFORM, ActionType.SAVE)

        'ログ出力
        getLogMsgDescription(bolResult, lblMsg)

        If bolResult And getLogCount() = 0 Then '結果がTrueなのにログありは、警告の場合

            '検索フォームの値でグリッドビューを表示
            grvJisho.PageIndex = 0 'ページ選択初期化
            grvJisho.DataBind()

            '編集フォームにキー値でデータロード
            executeBehavior(pnlKeyItem)
            formatFormData()

        End If

    End Sub


#End Region

#Region "グリッドビュー"

    Protected Sub grvJisho_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvJisho.SelectedIndexChanged

        '編集フォームパネルをオープン状態に
        hdnMode.Value = "U"

        '選択行の値を編集フォームに値ロード
        executeBehavior(grvJisho)
        formatFormData()
    End Sub

    Protected Sub grvJisho_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvJisho.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Then
            '開始月でテーブルのヘッダのテキストを変更
            Dim dt As DateTime
            If DateTime.TryParseExact(hdnJISHO_YM.Value, "yyyyMM", Nothing, Globalization.DateTimeStyles.None, dt) Then
                For i As Integer = 0 To 14
                    e.Row.Cells(i * 3 + 4).Text = "'" & dt.AddMonths(i).ToString("yy/MM") & "<br />計画値"
                    e.Row.Cells(i * 3 + 5).Text = "<br />増減"
                    e.Row.Cells(i * 3 + 6).Text = "<br />調整後"
                Next
            End If
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim dr As DataRow = CType(drv.Row, DataRow)

            'スケジューラ対象外の行は色を変更
            If dr.IsNull("SCHEDULER_FLG") OrElse dr("SCHEDULER_FLG") = "0" Then
                e.Row.BackColor = Drawing.Color.Yellow
                e.Row.ForeColor = Drawing.Color.Red
            End If

            '数値がマイナスとなる場合、赤色にする
            Dim columnList As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String)

            columnList.Add(4, "SRY01")
            columnList.Add(5, "DIFF_SRY01")
            columnList.Add(6, "ADJUSTED_SRY01")
            columnList.Add(7, "SRY02")
            columnList.Add(8, "DIFF_SRY02")
            columnList.Add(9, "ADJUSTED_SRY02")
            columnList.Add(10, "SRY03")
            columnList.Add(11, "DIFF_SRY03")
            columnList.Add(12, "ADJUSTED_SRY03")
            columnList.Add(13, "SRY04")
            columnList.Add(14, "DIFF_SRY04")
            columnList.Add(15, "ADJUSTED_SRY04")
            columnList.Add(16, "SRY05")
            columnList.Add(17, "DIFF_SRY05")
            columnList.Add(18, "ADJUSTED_SRY05")
            columnList.Add(19, "SRY06")
            columnList.Add(20, "DIFF_SRY06")
            columnList.Add(21, "ADJUSTED_SRY06")
            columnList.Add(22, "SRY07")
            columnList.Add(23, "DIFF_SRY07")
            columnList.Add(24, "ADJUSTED_SRY07")
            columnList.Add(25, "SRY08")
            columnList.Add(26, "DIFF_SRY08")
            columnList.Add(27, "ADJUSTED_SRY08")
            columnList.Add(28, "SRY09")
            columnList.Add(29, "DIFF_SRY09")
            columnList.Add(30, "ADJUSTED_SRY09")
            columnList.Add(31, "SRY10")
            columnList.Add(32, "DIFF_SRY10")
            columnList.Add(33, "ADJUSTED_SRY10")
            columnList.Add(34, "SRY11")
            columnList.Add(35, "DIFF_SRY11")
            columnList.Add(36, "ADJUSTED_SRY11")
            columnList.Add(37, "SRY12")
            columnList.Add(38, "DIFF_SRY12")
            columnList.Add(39, "ADJUSTED_SRY12")
            columnList.Add(40, "SRY13")
            columnList.Add(41, "DIFF_SRY13")
            columnList.Add(42, "ADJUSTED_SRY13")
            columnList.Add(43, "SRY14")
            columnList.Add(44, "DIFF_SRY14")
            columnList.Add(45, "ADJUSTED_SRY14")
            columnList.Add(46, "SRY15")
            columnList.Add(47, "DIFF_SRY15")
            columnList.Add(48, "ADJUSTED_SRY15")

            For Each item As KeyValuePair(Of Integer, String) In columnList
                If Not dr.IsNull(item.Value) AndAlso dr(item.Value) < 0 Then
                    e.Row.Cells(item.Key).ForeColor = Drawing.Color.Red
                Else
                    e.Row.Cells(item.Key).ForeColor = Drawing.Color.Black
                End If
            Next

        End If



    End Sub

    Protected Sub odsData_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsData.ObjectCreating
        e.ObjectInstance = New AstMaster.AST_D_JISHO_ADJUST(GPageMediator.ConnectionName)
    End Sub

    Protected Sub odsData_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsData.Selecting
        Dim sendObj As GearsDTO = makeSendMessage(pnlGFilter, grvJisho)

        'ソート順を定義
        sendObj.addSelection(SqlBuilder.newSelect("HIN_CD").ASC.isNoSelect)
        sendObj.addSelection(SqlBuilder.newSelect("PLNT_CD").ASC.isNoSelect)

        e.InputParameters("data") = sendObj

    End Sub

    Protected Sub odsData_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles odsData.Selected

        If TypeOf e.ReturnValue Is Integer Then 'データセットとカウントの取得が行われる
            lblCount.Text = "抽出結果：" + e.ReturnValue.ToString + " 件" + lblCount.Text
        ElseIf TypeOf e.ReturnValue Is DataTable Then
            Dim startPoint As Integer = grvJisho.PageIndex * grvJisho.PageSize
            Dim endPoint As Integer = startPoint + CType(e.ReturnValue, DataTable).Rows.Count

            If endPoint > 0 Then
                lblCount.Text = " ( " + (startPoint + 1).ToString + " ～ " + endPoint.ToString + " ) "
            End If

        End If

    End Sub

#End Region

#Region "アップデートパネル"

    Protected Sub txtHIN_CD__KEY_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        updatePanelLoad()
    End Sub

    Protected Sub txtPLNT_CD__KEY_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        updatePanelLoad()
    End Sub

    Protected Sub updatePanelLoad()
        Dim errflg As String = "0"

        If ExistsNisugata(HIN_CD__KEY) Then
            HIN_TXT__GCON.getControl.CssClass = ""
            executeBehavior(HIN_CD__KEY.getControl)
        Else
            getLogMsgDescription(False, HIN_TXT__GCON.getControl)
            Log.Clear()
            errflg = "1"
        End If

        If ExistsPlant(PLNT_CD__KEY) Then
            PLNT_TXT__GCON.getControl.CssClass = ""
            executeBehavior(PLNT_CD__KEY.getControl)
        Else
            getLogMsgDescription(False, PLNT_TXT__GCON.getControl)
            Log.Clear()
            errflg = "1"
        End If

        If errflg = "1" Then
            clearFormData()
        Else
            loadUpdatePanelJishoData()
        End If

        udpJisho.Update()

    End Sub

    Private Function ExistsNisugata(ByVal control As UnitItem) As Boolean
        Dim ds As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
        Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)
        dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(control.getValue()))
        dto.addFilter(SqlBuilder.newFilter("HIN_KBN").eq("3"))

        If control.getValue = "" Then
            Log.Add(HIN_CD__KEY.ID, New GearsDataValidationException("品目コード欄が空白です"))
            Return False
        ElseIf ds.gSelectCount(dto) = 0 Then
            Log.Add(HIN_CD__KEY.ID, New GearsDataValidationException("品目コード=" + control.getValue() + "の荷姿は存在しません"))
            Return False
        End If
        Return True
    End Function

    Private Function ExistsPlant(ByVal control As UnitItem) As Boolean
        Dim ds As New GDSTemplate(GPageMediator.ConnectionName, SqlBuilder.newDataSource("AST_V_PLNT"))
        Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)
        dto.addFilter(SqlBuilder.newFilter("PLNT_CD").eq(control.getValue()))

        If control.getValue = "" Then
            Log.Add(PLNT_CD__KEY.ID, New GearsDataValidationException("プラントコード欄が空白です"))
            Return False
        ElseIf ds.gSelectCount(dto) = 0 Then
            Log.Add(PLNT_CD__KEY.ID, New GearsDataValidationException("プラントコード=" + control.getValue() + "は存在しません"))
            Return False
        End If
        Return True
    End Function

    Protected Sub clearFormData()

        Dim tbl As Table
        tbl = TryCast(udpJisho.FindControl("tblJISHO"), Table)

        For Each row As TableRow In tbl.Controls
            For Each cell As TableCell In row.Controls
                For Each childctl As System.Web.UI.Control In cell.Controls

                    '配下のテキストボックスはすべて空
                    If TypeOf childctl Is TextBox Then
                        CType(childctl, TextBox).Text = Nothing
                    End If

                    '数量のラベルはすべて空
                    For i As Integer = 1 To 15
                        If childctl.ID = "lblSRY" & Right("0" & i.ToString(), 2) & "__GCON" Then
                            DirectCast(childctl, Label).Text = Nothing

                        End If

                    Next
                Next
            Next
        Next


    End Sub

    Protected Sub loadUpdatePanelJishoData()

        executeBehavior(pnlKeyItem)
        formatFormData()

        Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)
        If jishoDataSource.gSelectCount(GPageMediator.makeSendMessage(pnlKeyItem, pnlGFORM, dto)) = 0 Then
            clearFormData()
        End If

    End Sub

#End Region

#Region "その他関数"

    Protected Sub formatFormData()

        labalNumberFormat({lblSRY01__GCON, lblSRY02__GCON, _
                           lblSRY03__GCON, lblSRY04__GCON, _
                           lblSRY05__GCON, lblSRY06__GCON, _
                           lblSRY07__GCON, lblSRY08__GCON, _
                           lblSRY09__GCON, lblSRY10__GCON, _
                           lblSRY11__GCON, lblSRY12__GCON, _
                           lblSRY13__GCON, lblSRY14__GCON, _
                           lblSRY15__GCON})

    End Sub

    Protected Sub labalNumberFormat(ByRef lbls As Label())

        Dim d As Decimal
        For Each lbl As Label In lbls
            If String.IsNullOrWhiteSpace(lbl.Text) = False AndAlso Int32.TryParse(lbl.Text, d) Then

                If d < 0 Then
                    lbl.ForeColor = Drawing.Color.Red
                Else
                    lbl.ForeColor = Drawing.Color.Black
                End If

                lbl.Text = String.Format("{0:#,0}", d)

            End If
        Next

    End Sub

    Protected Sub setYMtoTblJISHO()
        Dim dt As DateTime
        If DateTime.TryParseExact(hdnJISHO_YM.Value, "yyyyMM", Nothing, Globalization.DateTimeStyles.None, dt) Then

            Dim tbl As Table
            tbl = TryCast(udpJisho.FindControl("tblJISHO"), Table)

            For Each row As TableRow In tbl.Controls
                For Each cell As TableCell In row.Controls
                    For Each childctl As System.Web.UI.Control In cell.Controls
                        '年月のラベルに値を設定
                        For i As Integer = 1 To 15
                            If childctl.ID = "lblJISHO_YM" & Right("0" & i.ToString(), 2) Then
                                DirectCast(childctl, Label).Text = dt.AddMonths(i - 1).ToString("yyyy/MM")
                            End If
                        Next
                    Next
                Next
            Next
        End If
    End Sub

#End Region

#End Region


End Class
