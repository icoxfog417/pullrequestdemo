﻿Imports Gears
Imports System.Data
Imports System.String


Partial Class _220_ast_GmzAdjust
    Inherits GearsPage

#Region "クラス変数"

    Private gmzDataSource As AstMaster.AST_D_GTMTZAIKO_ADJUST = Nothing
    Private gmzDetailDataSource As AstMaster.AST_V_GTMTZAIKO = Nothing

#End Region

#Region "イベント"

#Region "ページ"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        gmzDataSource = New AstMaster.AST_D_GTMTZAIKO_ADJUST(GPageMediator.ConnectionName)
        gmzDetailDataSource = New AstMaster.AST_V_GTMTZAIKO(GPageMediator.ConnectionName)

        gmzDataSource.addLockCheckCol("UPD_YMD", LockType.UDATESTR)
        gmzDataSource.addLockCheckCol("UPD_HMS", LockType.UTIMESTR)

        'モデル検証クラスの設定
        Dim gmzValidator As New GmzValidator(GPageMediator.ConnectionName)
        gmzDataSource.ModelValidator = gmzValidator

        'フィルタ用のコントロールを登録
        registerMyControl(pnlGFilter)

        '一覧表示用のコントロール/データソースを登録
        registerMyControl(grvGmzSummary) 'ページング処理GridViewは手動でデータソースとの紐付けが必要のため、ここでは書かない
        registerMyControl(grvGmzDetail, gmzDetailDataSource) 'ページング不要GridViewはここでコントロールとデータソースを紐付け

        '編集用のコントロール/データソースを登録
        registerMyControl(pnlGFORM, gmzDataSource)

        registerMyControl(pnlKeyItem)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ここで行うこと
        '①：関連を定義（A→B：Aが変更されたらBに変更内容を通知し、Bを変更する）
        '　　（executeBehaviorを利用する為に必要）
        '②キー項目の設定
        ' ※②についてはコントロールそのもののIDに[__KEY]を追加すればよいようになったため、ここでは設定不要になった

        ' ◆関連定義◆ --------------------------------------------------------------------

        '検索条件パネルの内容で、品目一覧テーブルを表示
        '（こう書けばパネル内の項目すべてが対象[=検索条件に使用される]になる）
        addRelation(pnlGFilter, grvGmzSummary)

        '品目一覧テーブル内の行の「選択」ボタンクリックで、編集パネルにそのキー値のデータを表示・ロット内訳詳細テーブルを表示
        addRelation(grvGmzSummary, pnlGFORM)
        addRelation(grvGmzSummary, grvGmzDetail)

        '画面リフレッシュ用：キー値で編集パネルにそのキー値のデータを表示・ロット内訳詳細テーブルを表示
        addRelation(pnlKeyItem, pnlGFORM)
        addRelation(pnlKeyItem, grvGmzDetail)

        '編集パネルの内容を編集して「更新」すると、自身の編集パネルの内容を更新
        '（明示的に書く必要あり）
        addRelation(pnlGFORM, pnlGFORM)

        '以下、上でFrom-To関連を定義したコントロールのうち、To側に通知する必要のないコントロールを除外、
        'もしくはFrom側に入っていない固定値などを追加で通知したい場合に記述

        'このコントロールは通知対象からはずしたい、という場合このように宣言
        '（この場合、発行されるupdate文のSETの対象にしないよう宣言が必要）
        setEscapesWhenSend(pnlGFORM, pnlGFORM, {HIN_TXT__GCON.ControlId _
                                        , lblQRANK_A_SRY__GCON.ID, lblQRANK_B_SRY__GCON.ID _
                                        , lblQRANK_C_SRY__GCON.ID, lblQRANK_D_SRY__GCON.ID _
                                        , lblQRANK_Z_SRY__GCON.ID _
                                        , lblSCPTAISHO_SRY__GCON.ID, lblSCPTAISHOGAI_SRY__GCON.ID _
                                        , lblTOTAL_SRY__GCON.ID, lblADJUSTED_TOTAL_SRY__GCON.ID})

        'キー項目の設定
        'getMyControl(HIN_CD__1.ControlId).setAskey() -- コントロールそのもののIDで定義づけできるため不要（例）HIN_CD__KEY

        'メッセージの初期化
        lblMsg.Text = ""
        lblMsg.CssClass = ""

        'テーブルデータのロード()
        If Not IsPostBack Then

            '検索条件パネルをオープン状態に
            hdnMode.Value = "S"

            '検索フォームの値でグリッドビューを表示
            grvGmzSummary.PageIndex = 0 'ページ選択初期化
            grvGmzSummary.DataBind()

        End If


    End Sub


    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        If grvGmzSummary.Rows.Count = 0 Then
            lblSummaryDataHelp.Visible = False
        Else
            lblSummaryDataHelp.Visible = True
        End If
        If grvGmzDetail.Rows.Count = 0 Then
            lblDetailDataHelp.Visible = False
        Else
            lblDetailDataHelp.Visible = True
        End If
    End Sub


#End Region

#Region "ボタン"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        '検索条件パネルをオープン状態に
        hdnMode.Value = "S"

        lblCount.Text = String.Empty

        'フィルタの値で一覧を更新する(ページング対応用)　→ DataBindを実行するとSelectingのイベントが発生する
        grvGmzSummary.PageIndex = 0 'ページ選択初期化
        grvGmzSummary.DataBind()


    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        '編集フォームパネルをオープン状態に
        hdnMode.Value = "U"

        '更新者を設定
        hdnUPD_USR.Value = Master.LoginUser

        'データ登録/更新
        Dim bolResult As Boolean = executeBehavior(pnlGFORM, ActionType.SAVE)

        'ログ出力
        getLogMsgDescription(bolResult, lblMsg)

        If bolResult And getLogCount() = 0 Then '結果がTrueなのにログありは、警告の場合

            '検索フォームの値でグリッドビューを表示
            grvGmzSummary.PageIndex = 0 'ページ選択初期化
            grvGmzSummary.DataBind()

            executeBehavior(pnlKeyItem)
            formatFormData()

        End If

    End Sub

#End Region

#Region "グリッドビュー"

    Protected Sub grvGmzSummary_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvGmzSummary.SelectedIndexChanged

        '編集フォームパネルをオープン状態に
        hdnMode.Value = "U"

        '選択行の値を編集フォームに値ロード
        executeBehavior(grvGmzSummary)
        formatFormData()

        '編集フォームのキー値を退避(アップロード後のリフレッシュ用)
        hdnHIN_CD.Value = HIN_CD__KEY__GCON.getValue
        hdnYM.Value = lblYM__KEY__GCON.Text

    End Sub


    Protected Sub grvGmzSummary_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvGmzSummary.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim dr As DataRow = CType(drv.Row, DataRow)

            'スケジューラ対象外の行は色を変更
            If dr.IsNull("SCHEDULER_FLG") OrElse dr("SCHEDULER_FLG") = "0" Then
                e.Row.BackColor = Drawing.Color.LightGray
                e.Row.ForeColor = Drawing.Color.Black
            End If

            '数値がマイナスとなる場合、赤色にする
            Dim columnList As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String)

            columnList.Add(4, "QRANK_A_SRY")
            columnList.Add(5, "QRANK_B_SRY")
            columnList.Add(6, "QRANK_C_SRY")
            columnList.Add(7, "QRANK_D_SRY")
            columnList.Add(8, "QRANK_Z_SRY")
            columnList.Add(9, "SCPTAISHO_SRY")
            columnList.Add(10, "SCPTAISHOGAI_SRY")
            columnList.Add(11, "TOTAL_SRY")
            columnList.Add(12, "ADJUST_SRY")
            columnList.Add(13, "ADJUSTED_TOTAL_SRY")

            For Each item As KeyValuePair(Of Integer, String) In columnList
                If Not dr.IsNull(item.Value) AndAlso dr(item.Value) < 0 Then
                    e.Row.Cells(item.Key).ForeColor = Drawing.Color.Red
                Else
                    e.Row.Cells(item.Key).ForeColor = Drawing.Color.Black
                End If
            Next

        End If

    End Sub

    'PRM日次在庫の修正が終わるまで表示しない
    '<asp:TemplateField HeaderText="日次在庫">
    '    <ItemTemplate>
    '        <asp:HyperLink ID="lnkTo" runat="server" Target="newPPP" NavigateUrl='<%# "../212_inventorymanagement/zaiko_list.aspx?hin=" & DataBinder.Eval(Container.DataItem, "HIN_CD") %>'
    '            Text="⇒リンク" CssClass="tooltip-on" />
    '    </ItemTemplate>
    '</asp:TemplateField>
    'Protected Sub grvGmzDetail_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvGmzDetail.RowCreated

    '    If Not User.IsInRole("AST_PRM01") Then

    '        If e.Row.RowType = DataControlRowType.DataRow _
    '          OrElse e.Row.RowType = DataControlRowType.Header Then

    '            'PRMロール以外の場合、ポータル日次在庫へのリンクを非表示に
    '            e.Row.Cells(14).Visible = False

    '        End If
    '    End If

    'End Sub

    Protected Sub grvGmzDetail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvGmzDetail.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim drv As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim dr As DataRow = CType(drv.Row, DataRow)

            Dim columnList As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String)

            columnList.Add(6, "RIYOKA_SRY")
            columnList.Add(7, "TENSOCHU_SRY")
            columnList.Add(8, "KENSACHU_SRY")
            columnList.Add(9, "HIRIKA_SRY")
            columnList.Add(10, "HORYU_SRY")
            columnList.Add(11, "HENPIN_SRY")
            columnList.Add(12, "TOKUSHU_SRY")
            columnList.Add(13, "TOTAL_SRY")

            '数値がマイナスとなる場合、赤色にする
            For Each item As KeyValuePair(Of Integer, String) In columnList
                If Not dr.IsNull(item.Value) AndAlso dr(item.Value) < 0 Then
                    e.Row.Cells(item.Key).ForeColor = Drawing.Color.Red
                Else
                    e.Row.Cells(item.Key).ForeColor = Drawing.Color.Black
                End If
            Next

        End If

    End Sub

    Protected Sub odsData_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsData.ObjectCreating
        e.ObjectInstance = New AstMaster.AST_D_GTMTZAIKO_ADJUST(GPageMediator.ConnectionName)
    End Sub

    Protected Sub odsData_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsData.Selecting
        Dim sendObj As GearsDTO = makeSendMessage(pnlGFilter, grvGmzSummary)

        'ソート順を定義
        sendObj.addSelection(SqlBuilder.newSelect("HIN_CD").ASC.isNoSelect)

        e.InputParameters("data") = sendObj
    End Sub

    Protected Sub odsData_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs) Handles odsData.Selected
        If TypeOf e.ReturnValue Is Integer Then 'データセットとカウントの取得が行われる
            lblCount.Text = "抽出結果：" + e.ReturnValue.ToString + " 件" + lblCount.Text
        ElseIf TypeOf e.ReturnValue Is DataTable Then
            Dim startPoint As Integer = grvGmzSummary.PageIndex * grvGmzSummary.PageSize
            Dim endPoint As Integer = startPoint + CType(e.ReturnValue, DataTable).Rows.Count

            If endPoint > 0 Then
                lblCount.Text = " ( " + (startPoint + 1).ToString + " ～ " + endPoint.ToString + " ) "
            End If

        End If

    End Sub

#End Region

#Region "その他関数"

    Protected Sub formatFormData()

        labalNumberFormat({lblQRANK_A_SRY__GCON, lblQRANK_B_SRY__GCON, _
                           lblQRANK_C_SRY__GCON, lblQRANK_D_SRY__GCON, _
                           lblQRANK_Z_SRY__GCON, _
                           lblSCPTAISHO_SRY__GCON, lblSCPTAISHOGAI_SRY__GCON, _
                           lblTOTAL_SRY__GCON, lblADJUSTED_TOTAL_SRY__GCON})

    End Sub

    Protected Sub labalNumberFormat(ByRef lbls As Label())

        Dim d As Decimal
        For Each lbl As Label In lbls
            If String.IsNullOrWhiteSpace(lbl.Text) = False AndAlso Decimal.TryParse(lbl.Text, d) Then

                If d < 0 Then
                    lbl.ForeColor = Drawing.Color.Red
                Else
                    lbl.ForeColor = Drawing.Color.Black
                End If

                lbl.Text = String.Format("{0:#,0}", d)

            End If
        Next

    End Sub


#End Region

#End Region

End Class
