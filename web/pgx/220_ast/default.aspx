﻿<%@ Page Language="VB" MasterPageFile="astMaster.master" AutoEventWireup="true" CodeFile="Default.aspx.vb" Inherits="_220_ast_Default" %>

<asp:Content id="clientHead" ContentPlaceHolderID="pppHead" Runat="Server" ClientIDMode=Static>
    <title>Ｓ-Ｎａｖｉ＋　メニュー画面</title>
    <script language="javascript">
        $(function () {
            setPPPMenuAction();
        })
        
    </script>
</asp:Content>

<asp:Content id="clientCenter" ContentPlaceHolderID="pppContent" Runat="Server" ClientIDMode=Static>
    <div align="center">
    <br/>
    <span class="hi" style="font-size:14pt;font-weight:bold;">Ｓ－Ｎａｖｉ　Ｐｌｕｓ　メニュー</span>
    <!-- ▼各機能へのリンク -->
    <div id="ppp-menu" class="ppp-menu" >
		<div id="ppp-menu-list">
            <ul class="ppp-menu-list">
				<li class="ppp-menu-item">
					<a href="SchedulerItemList.aspx" class="ppp-link-item">マスタメンテナンス</a>
				</li>
                <li class="ppp-menu-item">
					<a href="UploadHinMst.aspx" class="ppp-link-item">ＥＸＣＥＬ一括メンテナンス</a>
				</li>
				<li class="ppp-menu-item">
					<a href="HanyoMstMnt.aspx" class="ppp-link-item">汎用テーブル参照</a>
				</li>
				<li class="ppp-menu-item">
                    <a href="GmzAdjust.aspx" class="ppp-link-item">月末在庫意思入れ</a>
                </li>
				<li class="ppp-menu-item">
                    <a href="JishoAdjust.aspx" class="ppp-link-item">自消計画意思入れ</a>
                </li>
				<li class="ppp-menu-item">
                    <a href="CsvOutput.aspx" class="ppp-link-item">ＣＳＶファイル出力</a>
                </li>
				<li class="ppp-menu-item">
                    <a href="AlertList.aspx" class="ppp-link-item">アラート一覧</a>
                </li>
			</ul>           
            <div style="border:0px;">
                <span style="color:#0af;align:center;">Ast_Helpメール問合せ</span><br/>
                <iframe src="notesmail_ast_help.html" frameborder="no" scrolling="no" marginwidth="0" marginheight="0" height="70px" width="70px" title="aaa">
                </iframe>
            </div>

		</div>
		<div id="ppp-menu-content">
            <div class="ppp-menu-content-item default" >
                <span class="ppp-caption">メニューを選択してください</span><br/><br/>
                Astplannerに受け渡すマスタのメンテナンス、データの補正、及び、ＣＳＶファイル出力を行います<br/><br/>
                
                <div style="background-color:#eef4ff;padding:5px 15px 0px 15px;width:400px;border: outset 1px #aaccaa;line-height:14px;">
                <span class="ppp-label"><b>Ｓ－Ｎａｖｉ Ｐｌｕｓ マニュアル</b></span>
				<ol>
				<li class="ppp-label">マスタメンテナンス</li>
					<ol>
					<li class="ppp-label">スケジューラ品目一覧　
						<a href="help/SchedulerItemList.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
						<a href="help/SchedulerItemList.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
					<li class="ppp-label">スケジューラー品目ダッシュボード　
						<a href="help/SchedulerItemDashBoard.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
						<a href="help/SchedulerItemDashBoard.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
					<li class="ppp-label">銘柄メンテナンス　
						<a href="help/MeigaraMnt.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
						<a href="help/MeigaraMnt.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
					<li class="ppp-label">荷姿詳細メンテ画面　
						<a href="help/NsgtHinMnt.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
						<a href="help/NsgtHinMnt.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
					<li class="ppp-label">ダミー品目メンテナンス　
						<a href="help/DummyHinMnt.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
						<a href="help/DummyHinMnt.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
					</ol>
				<li class="ppp-label">ＥＸＣＥＬ一括メンテナンス
					<a href="help/UploadHinMst.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
					<a href="help/UploadHinMst.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
				<li class="ppp-label">汎用テーブル参照画面　
					<a href="help/HanyoMstMnt.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
					<a href="help/HanyoMstMnt.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
				<li class="ppp-label">月末在庫数量意思入れ　
					<a href="help/GmzAdjust.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
					<a href="help/GmzAdjust.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
				<li class="ppp-label">自消計画数量意思入れ　
					<a href="help/JishoAdjust.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
					<a href="help/JishoAdjust.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
				<li class="ppp-label">ＣＳＶファイル出力　
					<a href="help/CsvOutput.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
					<a href="help/CsvOutput.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
				<li class="ppp-label">アラート一覧　
					<a href="help/AlertList.aspx-help.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
					<a href="help/AlertList.aspx-help.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
				<li class="ppp-label">関連資料</li>
					<ol>
					<li class="ppp-label">システム概要図　
						<a href="help/S-Navi_gaiyo.htm" target="_blank"><img src="images/monitor.png" title="画面" style="vertical-align:bottom;"></a>
						<a href="help/S-Navi_gaiyo.ppt" target="_blank"><img src="images/document-powerpoint.png" title="Excel" style="vertical-align:bottom;"></a></li>
					<li class="ppp-label">S-Navi Plus ＣＳＶ出力定義書　
						<a href="help/S-NaviPlusCSV_def.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
					<li class="ppp-label">Astplanner側 データ定義書　
						<a href="help/ysl_data_def.xls" target="_blank"><img src="images/document-excel.png" title="Excel" style="vertical-align:bottom;"></a></li>
					<li class="ppp-label">Astplanner機能仕様書　
						<a href="help/ysl_ast_kino.pdf" target="_blank"><img src="images/document-pdf.png" title="PDF" style="vertical-align:bottom;"></a></li>
					</ol>
				</ol>
				</div>

            </div>
            
            <div class="ppp-menu-content-item">
                <span class="ppp-caption">スケジューラ品目一覧</span><br/>
					一覧形式での品目検索、Excel出力を行います<br/><br/>
                <span class="ppp-caption">スケジューラー品目ダッシュボード</span><br/>
					指定された銘柄に関わる情報をダッシュボード形式で照会します<br/><br/>
                <span class="ppp-caption">銘柄メンテナンス</span><br/>
					重合グループ／銘柄のマスタメンテナンスを行います<br/><br/>
                <span class="ppp-caption">荷姿詳細メンテ画面</span><br/>
					荷姿品のマスタメンテナンスを行います<br/><br/>
                <span class="ppp-caption">ダミー品目メンテナンス</span><br/>
					SD品、TR-SD品のマスタメンテナンスを行います<br/><br/>
            </div>
            <div class="ppp-menu-content-item">
                <span class="ppp-caption">ＥＸＣＥＬ一括メンテナンス</span><br/><br/>
                スケジューラ品目のＥＸＣＥＬ一括アップロードを行います
            </div>
            <div class="ppp-menu-content-item">
                <span class="ppp-caption">汎用テーブル参照</span><br/><br/>
                汎用テーブルの登録内容を照会します
            </div>
			<div class="ppp-menu-content-item">
                <span class="ppp-caption">月末在庫意思入れ</span><br/><br/>
                月締め後の月末在庫数量について、数量の調整を行います
            </div>
			<div class="ppp-menu-content-item">
                <span class="ppp-caption">自消計画意思入れ</span><br/><br/>
                月次計画（ＳＣ実行計画）の自消計画数量について、数量の調整を行います
            </div>
			<div class="ppp-menu-content-item">
                <span class="ppp-caption">ＣＳＶファイル出力</span><br/><br/>
                Astplannerが必要とするＣＳＶファイル（マスタ、トランザクションデータ）を出力します
            </div>
	    <div class="ppp-menu-content-item">
                <span class="ppp-caption">アラート一覧</span><br/><br/>
                マスタ整備が整っていない品目を照会します
            </div>
        </div>
	</div>
    </div>
    <asp:HiddenField ID="hdLoginUser" runat="server" ClientIDMode=Static />
</asp:Content>
