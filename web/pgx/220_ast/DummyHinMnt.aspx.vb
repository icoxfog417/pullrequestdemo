﻿Imports Gears

Partial Class _220_ast_DummyHinMnt
    Inherits GearsPage

    Private gvUtil As GridViewUtility

    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)

        Dim scHingDS As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
        scHingDS.addLockCheckCol("UPD_YMD", LockType.UDATESTR) '楽観的ロックに使用する列とタイプを指定。
        scHingDS.addLockCheckCol("UPD_HMS", LockType.UTIMESTR)
        scHingDS.addLockCheckCol("UPD_USR", LockType.USER)

        registerMyControl(pnlMeigara__GFORM, scHingDS)
        registerMyControl(pnlDUMMY__GFilter)
        registerMyControl(grvDummy, New AstMaster.AST_M_HIN(GPageMediator.ConnectionName))

        'モデル検証クラスの設定
        Dim hingValidator As New AstMasterHinmokuValidator(Master.ConnectionName)
        scHingDS.ModelValidator = hingValidator

        'イベントハンドラの登録
        AddHandler HIN_CD__KEY.getControl(Of TextBox).TextChanged, AddressOf Me.txtHIN_CD__KEY_TextChanged
        AddHandler HIN_KBN.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlHIN_KBN_SelectedIndexChanged
        AddHandler EXHIN_KBN.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlEXHIN_KBN_SelectedIndexChanged
        AddHandler DHYO_MDL_KBN.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlDHYO_MDL_KBN_SelectedIndexChanged

        '非同期更新処理の登録
        Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)
        srmOnPage.RegisterAsyncPostBackControl(DHYO_MDL_KBN.getControl(Of DropDownList))
        srmOnPage.RegisterAsyncPostBackControl(HIN_CD__KEY.getControl(Of TextBox))

        'ユーザーコントロールの初期化
        ucHIN_SOUCHI.ConnectionName = Master.ConnectionName
        SEARCH_PARENT.ConnectionName = Master.ConnectionName

        gvUtil = New GridViewUtility(grvDummy, lblState)

    End Sub

    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)

        '初期化処理
        lblMsg.Text = ""
        resetEscapesWhenSend(pnlMeigara__GFORM, pnlMeigara__GFORM)

        'リレーション定義
        addRelation(HIN_CD__KEY.getControl, pnlMeigara__GFORM)
        addRelation(pnlMeigara__GFORM, pnlMeigara__GFORM)
        addRelation(grvDummy, pnlMeigara__GFORM)
        addRelation(DHYO_MDL_KBN.getControl, DHYO_SOUCHI_CD.getControl)

        '不要コントロール
        setEscapesWhenSend(pnlMeigara__GFORM, pnlMeigara__GFORM, pnlHinSouchi.ID)

        Dim hin As String = HIN_CD__KEY.getValue
        If Not IsPostBack AndAlso Not String.IsNullOrEmpty(getQueryString("hin_cd")) Then
            hin = getQueryString("hin_cd")
        End If

        If Not IsPostBack And Not String.IsNullOrEmpty(hin) Then
            HIN_CD__KEY.setValue(hin)
            refresh()
            changeHinKbn()
            changeExHinKbn()
        ElseIf IsReload Then
            'refresh()
        End If

    End Sub
    Private Sub refresh()
        executeBehavior(HIN_CD__KEY.getControl)
        refreshSouchiControl()
    End Sub

    Protected Sub grvDummy_PageIndexChanged(sender As Object, e As System.EventArgs) Handles grvDummy.PageIndexChanged
        Master.IsDisplayUpper = False

    End Sub

    Protected Sub grvDummy_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles grvDummy.SelectedIndexChanged
        executeBehavior(grvDummy)
        refreshSouchiControl()
        Master.IsDisplayUpper = True
    End Sub

    Protected Sub odsDummy_ObjectCreating(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsDummy.ObjectCreating
        e.ObjectInstance = New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
    End Sub
    Protected Sub odsDummy_Selecting(sender As Object, e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsDummy.Selecting

        If String.IsNullOrEmpty(EXHIN_KBN__SEARCH.getValue) Then
            e.InputParameters("data") = addDummyRestriction(makeSendMessage(pnlDUMMY__GFilter))
        Else
            e.InputParameters("data") = makeSendMessage(pnlDUMMY__GFilter)
        End If

    End Sub

    Protected Sub txtHIN_CD__KEY_TextChanged(sender As Object, e As System.EventArgs)
        refreshSouchiControl()
    End Sub

    Protected Sub ddlHIN_KBN_SelectedIndexChanged(sender As Object, e As System.EventArgs)
        changeHinKbn()
        refreshSouchiControl()
    End Sub
    Protected Sub ddlEXHIN_KBN_SelectedIndexChanged(sender As Object, e As System.EventArgs)
        changeExHinKbn()
    End Sub
    Protected Sub ddlDHYO_MDL_KBN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        executeBehavior(DHYO_MDL_KBN.getControl(Of DropDownList))
        refreshSouchiControl()

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        getLogMsgDescription(executeBehavior(pnlMeigara__GFORM, ActionType.SAVE), lblMsg)
        refreshSouchiControl()
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        grvDummy.DataBind()
        Master.IsDisplayUpper = False

    End Sub

    Private Sub changeHinKbn()
        '品目区分リストの設定
        Dim dto As New GearsDTO(ActionType.SEL)
        dto.addFilter(SqlBuilder.newFilter("HIN_KBN").eq(AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER)))
        dto.addFilter(SqlBuilder.newFilter("HIN_KBN").eq(AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.PELET)))
        getMyControl(HIN_KBN.ControlId).reload(dto)

        Dim peletOnly As Boolean = False
        If AstMaster.HIN_KBN.GetHinKbn(HIN_KBN.getValue) = AstMaster.HIN_KBN_LIST.PELET Then
            peletOnly = True
        End If
        switchControl(PARENT_HIN_CD.getControl, peletOnly)

    End Sub

    Private Sub changeExHinKbn()
        '選択リストの初期化
        getMyControl(EXHIN_KBN.ControlId).reload(addDummyRestriction)
        getMyControl(EXHIN_KBN__SEARCH.ControlId).reload(addDummyRestriction)

    End Sub
    Private Sub switchControl(ByRef control As WebControl, ByVal enable As Boolean, Optional ByVal inVisible As Boolean = False)
        If Not inVisible Then
            control.Enabled = enable
        Else

            If enable Then
                control.Style.Remove("display")
            Else
                control.Style.Add("display", "none")
            End If

        End If

    End Sub

    Private Function addDummyRestriction(Optional ByVal dto As GearsDTO = Nothing) As GearsDTO
        Dim nDto As GearsDTO = Nothing
        Dim addDefault As Boolean = True

        If dto Is Nothing Then
            nDto = New GearsDTO(ActionType.SEL)
        Else
            nDto = New GearsDTO(dto)
        End If

        nDto.addFilter(SqlBuilder.newFilter("EXHIN_KBN").eq("0").nots)

        Return nDto

    End Function

    Private Sub refreshSouchiControl()
        ucHIN_SOUCHI.Update(HIN_CD__KEY.getValue, HIN_KBN.getValue, DHYO_MDL_KBN.getValue)
        udpSOUCHI_CD.Update()
    End Sub


End Class
