﻿<%@ Page Title="Ｓ-Ｎａｖｉ＋　アラート一覧" Language="VB" MasterPageFile="~/220_ast/astMaster.master" AutoEventWireup="false" CodeFile="AlertList.aspx.vb" Inherits="_220_ast_AlertList" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ MasterType VirtualPath="astMaster.master" %>

<asp:Content ID="SHeader" ContentPlaceHolderID="pppHead" Runat="Server" ClientIDMode=Static>
    <script>
        $(function(){
            //adjustCombobox();
            //adjustComboboxが終わるまで待ってから処理

            $(".ppp-switch-area").hide(); //画面ゆれ(処理が終わるまで上が出たり消えたりする)を防ぐために、一旦隠す
            <%If IsDisplayUpper Then %>
                makePPPSwitchArea(0);
            <%Else %>
                makePPPSwitchArea(-1);
            <%End IF %>
            $(".ppp-switch-area").show();

            $(".link-field").click(function(){ 
                window.open($(this).attr("href"),$(this).attr("target")).focus();
             })
        })
    </script>

    <script type="text/javascript" language="javascript">

        // アラートメール送信処理
        function SendAlert() {
            var lblMSG = document.getElementById("lblAlert");
            var modellist = document.getElementById("ddlALERT_MDL_KBN");
            var model = modellist.options[modellist.selectedIndex];
            var conn = document.getElementById("lblConn");
            var yosan_obj = document.getElementById("ChkYOSAN");
            var yosan_chk = "N"
            if (yosan_obj.checked) {yosan_chk = "Y"};

            lblMSG.innerHTML = "送信準備中、しばらくお待ち下さい ．．．";
            SCAlert.SendAlert(conn.value, model.value, yosan_chk, OkCallback, NgCallback);
        }

        // 送信終了の画面ポップアップ表示
        function OkCallback(result) {
            var lblMSG = document.getElementById("lblAlert");
            lblMSG.innerHTML = "";
            alert(result);
        }
        function NgCallback(result) {
            var lblMSG = document.getElementById("lblAlert");
            lblMSG.innerHTML = "";
            alert("エラーが発生しました！　：" + result);
        }
    </script>

     <style>
       .liquid-list-area
      {
       min-width:750px;
       max-width:1250px;
       width: expression(this.width > 1024 ? this.width : '1150px'); /* IE用 */
      }
      
      .liquid-list-area .lq-float
      {
        padding:0px 0px 10px 0px;
        overflow:auto;   
      }
     </style>
</asp:Content>

<asp:Content ID="SPreDeclare" ContentPlaceHolderID="pppPreDeclare" Runat="Server" ClientIDMode=Static>
    <asp:ScriptManagerProxy runat="server">
            <Services>
                <asp:ServiceReference path="~/service/220_ast/SCAlert.asmx" />
            </Services>  
    </asp:ScriptManagerProxy>

</asp:Content>

<asp:Content ID="SPageTitle" ContentPlaceHolderID="pppPageTitle" Runat="Server" ClientIDMode=Static>
&nbsp&nbspアラート一覧
</asp:Content>
<asp:Content ID="SHeaderContent" ContentPlaceHolderID="pppHeader" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="pppSContent" ContentPlaceHolderID="pppContent" Runat="Server" ClientIDMode=Static>
    <asp:Panel id="SwitchingArea" class="ppp-switch-area" runat="server">
    <h3 class="ppp-switch-header">
        検索条件
    </h3>
    <asp:Panel id="UpperPanel" runat="server" CssClass="ppp-include-combo-area" >
       <asp:Panel id="pnlGFILTER" runat="server" Width=970>
            <ui:unitItem ID="ALERT_MDL_KBN" CtlKind="DDL" runat="server" LabelText="代表モデル区分" /> 
            <br />
            <asp:CheckBox ID="ChkYOSAN" runat="server" Text="予算データチェック" class="hi" />
            <asp:HiddenField ID="HdnViewName" runat="server" Value="AST_V_ALERTLIST" />
            <span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
            <asp:Button id="btnSearch" runat="server" Text="　　検　　索　　" />
            <span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
            <input id="btnSendAlert" type="button" value="　メール送信　" onclick="SendAlert()" />
            <span>&nbsp&nbsp&nbsp&nbsp</span>
            <asp:Label runat="server" ID="lblAlert" Text="" class="hi" ></asp:Label>
        </asp:Panel>
        
    </asp:Panel>
  </asp:Panel>

    <div class="liquid-list-area" style="margin:10px 0px 0px 30px;"> 
    <asp:Panel ID="pnlFloat" runat="server" CssClass="lq-float" Style="display:none"> <%-- 初回非表示 --%>
    <asp:GridView id="grvAlert" runat="server" CssClass="ppp-table" HeaderStyle-CssClass=ppp-table-head 
        EnableViewState=False DataSourceID=odsAlert
        PageSize=200 AllowPaging=True AutoGenerateColumns=False>
            <Columns>

                <asp:TemplateField HeaderText="品目コード">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkTo" runat="server" Target="scEdit" 
                            NavigateUrl=<%# AstMaster.Common.AstMasterNavigation.makeLink(Container) %>
                            Text=<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>
                            ToolTip=<%# DataBinder.Eval(Container.DataItem, "HIN_TXT")%>
                         CssClass="link-field" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="HIN_TXT" HeaderText="品目テキスト">
                </asp:BoundField>
                <asp:BoundField DataField="PARENT_HIN_CD" HeaderText="親銘柄">
                </asp:BoundField>
                <asp:BoundField DataField="PARENT_HIN_TXT" HeaderText="親銘柄名称">
                </asp:BoundField>
                <asp:BoundField DataField="ERR_KBN" HeaderText="エラー区分">
                </asp:BoundField>
                <asp:BoundField DataField="ERR_MSG" HeaderText="エラー内容">
                </asp:BoundField>
            </Columns>
    </asp:GridView>
    <asp:ObjectDataSource id="odsAlert" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount" typename="AstMaster.AST_V_ALERTLIST" EnablePaging=True >
    </asp:ObjectDataSource>
    <asp:Label runat="server" ID="lblState" CssClass="ppp-msg success"></asp:Label>
    <br/><br/>
    <asp:Label runat="server" CssClass="lw">&nbsp&nbsp※エラー区分：&nbsp&nbsp【I】新規自動登録品目、【M】マスタチェックエラー、【D】データチェックエラー、【Y】予算データチェックエラー</asp:Label>
    <br/><br/><br/><br/>

    </asp:Panel>
    </div>
    <asp:Label runat="server" ID="lblMsg" Text=""></asp:Label>
    <asp:HiddenField ID="lblConn" runat="server" />
</asp:Content>


