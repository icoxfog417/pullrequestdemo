﻿Imports Gears
''' ------------------------------------------------------------------------------
''' Project     : 220_ast
''' Class       : _220_ast_AlertList
''' 
''' -------------------------------------------------------------------------------
''' <summary>
''' アラート一覧画面
''' </summary>
''' <remarks>
''' </remarks>
''' <hisotry>
''' </hisotry>
''' -------------------------------------------------------------------------------
Partial Class _220_ast_AlertList
    Inherits GearsPage

    Private gvUtil As GridViewUtility
    Private strPreHinCd As String
    Private cellHinCd As TableCell
    Private cellHinTxt As TableCell
    Private cellParentHinCd As TableCell
    Private cellParentHinTxt As TableCell

    'スイッチ用のプロパティ
    Private _isDisplayUpper As Boolean = True
    Public Property IsDisplayUpper() As Boolean
        Get
            Return _isDisplayUpper
        End Get
        Set(ByVal value As Boolean)
            _isDisplayUpper = value
        End Set
    End Property


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        lblConn.Value = Master.ConnectionName   'javascriptからの参照のためセット(アラート送信時用)

        registerMyControl(pnlGFILTER)

        '非同期更新処理の登録
        Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)

        gvUtil = New GridViewUtility(grvAlert, lblState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        If ChkYOSAN.Checked = False Then
            HdnViewName.Value = "AST_V_ALERTLIST" '相関、実績・月次チェック、新規品目
        Else
            HdnViewName.Value = "AST_V_ALERTLIST_Y" '上記＋予算データチェック
        End If

        grvAlert.PageIndex = 0
        pnlFloat.Style.Remove("display") '表示開始
        IsDisplayUpper = False

    End Sub

    Protected Sub odsAlert_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsAlert.ObjectCreating
        e.ObjectInstance = New AstMaster.AST_V_ALERTLIST(GPageMediator.ConnectionName, HdnViewName.Value)
    End Sub

    Protected Sub odsAlert_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsAlert.Selecting
        Dim ds As New AstMaster.AST_V_ALERTLIST(GPageMediator.ConnectionName, HdnViewName.Value)
        Dim dto As GearsDTO = makeSendMessage(pnlGFILTER)
        dto.CommandTimeout = 180    '2012/07/31 ADD
        dto.addSelection(SqlBuilder.newSelect("HIN_CD").ASC.isNoSelect)

        e.InputParameters("data") = dto
    End Sub

    Protected Sub grvAlert_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvAlert.PageIndexChanged
        IsDisplayUpper = False
    End Sub

    Protected Sub grvAlert_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvAlert.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'スタイルセット
            Dim hinKbn As String = DataBinder.Eval(e.Row.DataItem, "HIN_KBN").ToString()
            If Not String.IsNullOrEmpty(hinKbn) Then
                Select Case AstMaster.HIN_KBN.GetHinKbn(hinKbn)
                    Case AstMaster.HIN_KBN_LIST.POWDER 'パウダー
                        e.Row.CssClass = "ppp-table-strong"
                    Case AstMaster.HIN_KBN_LIST.PELET 'ペレット
                        e.Row.CssClass = "ppp-table-even"
                    Case Else '包装品/荷姿品
                        'なし
                End Select
            End If
            'セル結合
            Dim strHinCd As String = DataBinder.Eval(e.Row.DataItem, "HIN_CD").ToString()
            If strHinCd = strPreHinCd Then
                If Not IsNothing(cellHinCd) Then
                    For i = 0 To 3
                        e.Row.Cells(i).Visible = False
                    Next
                    If (cellHinCd.RowSpan = 0) Then
                        cellHinCd.RowSpan = 2
                        cellHinTxt.RowSpan = 2
                        cellParentHinCd.RowSpan = 2
                        cellParentHinTxt.RowSpan = 2
                    Else
                        cellHinCd.RowSpan = cellHinCd.RowSpan + 1
                        cellHinTxt.RowSpan = cellHinTxt.RowSpan + 1
                        cellParentHinCd.RowSpan = cellParentHinCd.RowSpan + 1
                        cellParentHinTxt.RowSpan = cellParentHinTxt.RowSpan + 1
                    End If
                Else
                    cellHinCd = e.Row.Cells(0)
                    cellHinTxt = e.Row.Cells(1)
                    cellParentHinCd = e.Row.Cells(2)
                    cellParentHinTxt = e.Row.Cells(3)
                End If
            Else
                cellHinCd = e.Row.Cells(0)
                cellHinTxt = e.Row.Cells(1)
                cellParentHinCd = e.Row.Cells(2)
                cellParentHinTxt = e.Row.Cells(3)
            End If
            strPreHinCd = strHinCd

        End If

    End Sub

End Class
