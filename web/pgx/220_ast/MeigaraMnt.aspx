﻿<%@ Page Title="Ｓ-Ｎａｖｉ＋　銘柄メンテナンス" Language="VB" MasterPageFile="~/220_ast/astMaster.master" AutoEventWireup="false" CodeFile="MeigaraMnt.aspx.vb" Inherits="_220_ast_MeigaraMnt" %>
<%@ Register src="../parts/UnitItem.ascx" tagname="unitItem" tagprefix="ui" %>
<%@ Register src="../parts/220_ast/SCItemSearch.ascx" tagname="sh" tagprefix="search" %>
<%@ Register src="../parts/220_ast/SCItemHinSouchi.ascx" tagname="hinSouchi" tagprefix="hs" %>

<%@ MasterType VirtualPath="astMaster.master" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="pppHead" Runat="Server" ClientIDMode=Static>
    <link href="css/jquery.simple-color-picker.css" rel="stylesheet" type="text/css" />
    <script src="./lib/jquery.simple-color-picker.js" type="text/javascript" ></script>
    <script>
        function calcAZZ() {
            var nsum = 0.0
            $("#pnlDetailParams .azz-parameter").each(function () {
                if (!isNaN($(this).val())) {
                    nsum += parseFloat($(this).val());
                }
            })
            var azz = Math.round(nsum / 30  * 100) / 100; //月数変換
            $("#lblAZZ_CALC").text(azz);

        }
        function callParent() {
            if ($("#" + '<%= HIN_KBN.ControlId %>').val() != '<%= AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER) %>') {
                SEARCH_PARENT.openDialog()
            }
        }
        function callSortDialog() {
            //重合Ｇの取得
            var result = ""
            if ($("#" + '<%= HIN_KBN.ControlId %>').val() != '<%= AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER) %>') {
                result = $("#txtPARENT_HIN_CD").val()
            } else {
                result = $("#txtHIN_CD__KEY").val()
            }

            //編集ウィンドウのオープン
            var sortWindow = window.open('HinSortMnt.aspx?upd_func=updSort&parent_hin_cd=' + escape(result), 'scSort').focus();
        }

        function jumpToDashBoard() {
            window.open("SchedulerItemDashBoard.aspx?hin_cd=" + escape($("#txtHIN_CD__KEY").val()), 'scDetail').focus();
        
        }

        function updSort() {
            __doPostBack('<%= udpSORT_CD.ClientID %>', '');
        }

        $(function () {
            $(".invoke-hin-switch").each(function () {
                $(this).change(function (event) {
                    if (event.currentTarget.value != "") {
                        if (window.confirm("現在の入力値を、銘柄 " + event.currentTarget.value + " へコピーしますか？\n（キャンセルの場合、変更後の品目へ遷移します）。")) {
                            __doPostBack($(this).attr("id"), '');
                        } else {
                            location.href = "MeigaraMnt.aspx?hin_cd=" + escape($(this).val());
                        }
                    }
                });
            })

            //ダッシュボードへのジャンプボタン作成
            $("#btnSearchCodeHIN_CD__KEY").attr("title", "ダッシュボードへ");
            $("#btnSearchCodeHIN_CD__KEY").css({"font-size":"10px"});
            $("#btnSearchCodeHIN_CD__KEY").button({
                icons: { primary: "ui-icon-circle-arrow-e" },
                text: false
            });

        })

    </script>
</asp:Content>
<asp:Content ID="ContentPreDeclare" ContentPlaceHolderID="pppPreDeclare" Runat="Server" ClientIDMode=Static>
    <%-- 
    <asp:ScriptManagerProxy runat="server">
            <Services>
                <asp:ServiceReference Path="~/service/000_common/DBDataService.asmx" />
            </Services>  
    </asp:ScriptManagerProxy>
    --%>
</asp:Content>
<asp:Content ID="ContentPageTitle" ContentPlaceHolderID="pppPageTitle" Runat="Server" ClientIDMode=Static>
    銘柄メンテナンス
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label runat="server" ID="lblMsg" Text=""></asp:Label>

</asp:Content>
<asp:Content ID="ContentHeader" ContentPlaceHolderID="pppHeader" Runat="Server" ClientIDMode=Static>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="pppContent" Runat="Server" ClientIDMode=Static>

  <asp:Panel id="pnlMeigara__GFORM" runat="server" style="margin-left:15px">
    <asp:Panel id="pnlTop" runat="server">
        <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">基本</h3>
        <ui:unitItem ID="HIN_CD__KEY" CtlKind="TXT" runat="server" LabelText="品目コード" CssClass="invoke-hin-switch gears-GRequired gears-GByteLengthBetween_MinLength_0_Length_18" SearchAction="jumpToDashBoard()" />

        <asp:UpdatePanel id="udpDefault__HIN_TXT" runat="server" UpdateMode=Conditional>
            <ContentTemplate>
            <ui:unitItem ID="HIN_TXT" CtlKind="TXT" runat="server" LabelText="品目テキスト" Width="280" CssClass="gears-GRequired gears-GByteLengthBetween_Length_40"/>
            </ContentTemplate>
        </asp:UpdatePanel>

        <ui:unitItem ID="SCHEDULER_FLG" CtlKind="CHB" runat="server" LabelText="SC対象フラグ" Width="80" />

        <asp:UpdatePanel id="udpDefault__R3_FLG" runat="server" UpdateMode=Conditional>
            <ContentTemplate>
                <ui:unitItem ID="R3_FLG" CtlKind="CHB" runat="server" LabelText="SAP存在フラグ" Width="80" IsEditable=False />
            </ContentTemplate>
        </asp:UpdatePanel>

        <ui:unitItem ID="MK_FLG" CtlKind="RBL" runat="server" LabelText="無効フラグ"  />

        <br style="clear:both">
        <ui:unitItem ID="HIN_KBN" CtlKind="DDL" runat="server" LabelText="品目区分" Width=100 AutoPostBack=True/>
        <ui:unitItem ID="EXHIN_KBN" CtlKind="DDL" runat="server" LabelText="例外品目区分" IsEditable=False  Width=100/>
        <ui:unitItem ID="PARENT_HIN_CD" CtlKind="TXT" runat="server" LabelText="親銘柄/重合Ｇ" SearchAction="callParent()"/>
        <ui:unitItem ID="INFL_HIN_CD" CtlKind="TXT" runat="server" LabelText="しわ寄せ品目" SearchAction="SEARCH_CHILD.openDialog()"/>

        <asp:UpdatePanel id="udpDefault__VKORG" runat="server" UpdateMode=Conditional>
            <ContentTemplate>
            <ui:unitItem ID="VKORG" CtlKind="DDL" runat="server" LabelText="販売組織"  Width=100/>
            </ContentTemplate>
        </asp:UpdatePanel>

        <br style="clear:both">

        <ui:unitItem ID="KAKUGAI_KBN" CtlKind="DDL" runat="server" LabelText="格外銘柄区分" Width="100" />
        <ui:unitItem ID="IKOHIN_FLG" CtlKind="DDL" runat="server" LabelText="移行品銘柄" Width="100" />
        <ui:unitItem ID="PIM_OUT_CD" CtlKind="TXT" runat="server" LabelText="PIM-Aid 品目コード" CssClass="gears-GByteLengthBetween_Length_18" />

        <asp:UpdatePanel id="udpDefault__SCP_OUT_CD" runat="server" UpdateMode=Conditional >
            <ContentTemplate>
            <ui:unitItem ID="SCP_OUT_CD" CtlKind="TXT" runat="server" LabelText="SAP 品目コード" CssClass="gears-GByteLengthBetween_Length_18"/>
            </ContentTemplate>
        </asp:UpdatePanel>

        <br style="clear:both">
        <hr class="dot" width="99%" />

	    <ui:unitItem ID="DISP_COLOR" CtlKind="TXT" runat="server" LabelText="色" Width="45" CssClass="cpicker gears-GByteLength_Length_7"/>
	    <ui:unitItem ID="CMNT" CtlKind="TXT" runat="server" LabelText="コメント" Width="335" CssClass="gears-GByteLengthBetween_Length_100"/>
        <asp:UpdatePanel id="udpSORT_CD" runat="server" UpdateMode=Conditional>
            <ContentTemplate>
                <ui:unitItem ID="PRT_SSN_ORDER__GCON" CtlKind="LBL" runat="server" LabelText="銘柄生産順"　Width="60" />
                <ui:unitItem ID="HIN_SORT__GCON" CtlKind="LBL" runat="server" LabelText="銘柄ソート順"　Width="60" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <span>&nbsp&nbsp&nbsp</span>
        <asp:Button id="btnSortMnt" runat="server"  OnClientClick="callSortDialog(); return false;" UseSubmitBehavior=False Text="　ソート順編集　" />
    </asp:Panel>
    <br style="clear:both">
    <br style="clear:both">

    <asp:Panel id="pnlAttr" runat="server">
        <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">生産装置</h3>
        <ui:unitItem ID="DHYO_MDL_KBN" CtlKind="DDL" runat="server" Width="120" LabelText="代表モデル区分" AutoPostBack=True />
        <asp:UpdatePanel id="udpSOUCHI_CD" runat="server" UpdateMode=Conditional>
            <ContentTemplate>
                <ui:unitItem ID="DHYO_SOUCHI_CD" CtlKind="DDL" runat="server" Width="120" LabelText="代表装置" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <span width="10px">&nbsp</span>

        <asp:Panel id="pnlHinSouchi" runat="server" >
            <hs:hinSouchi ID="ucHIN_SOUCHI" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <br style="clear:both">

    <asp:Panel id="pnlScp" runat="server">
        <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">数量パラメータ</h3>

        <asp:Panel id="pnlParam" runat="server">
	        <ui:unitItem ID="MIN_HOJU_SRY" CtlKind="TXT" runat="server" LabelText="最小補充生産量" CssClass="gears-GRequired gears-GNumeric gears-GPeriodPositionOk_BeforeP_4_AfterP_2 gears-GComp_CompType_>_TgtValue_0"/>
	        <ui:unitItem ID="ADD_HOJU_SRY" CtlKind="TXT" runat="server" LabelText="増分補充生産量" CssClass="gears-GRequired gears-GNumeric gears-GPeriodPositionOk_BeforeP_4_AfterP_2 gears-GComp_CompType_>_TgtValue_0"/>
            <div style="float:left;width:180px;" >&nbsp;</div>
	        <ui:unitItem ID="JISHO_DRP_FLG" CtlKind="DDL" runat="server" LabelText="自消引落" Width="120" />
	        <ui:unitItem ID="JISHO_DRP_NSU" CtlKind="TXT" runat="server" LabelText="自消引落日数" Width="60" CssClass="gears-GNumber gears-GByteLengthBetween_Length_2"/>
            <ui:unitItem ID="SSN_CYC_NSU" CtlKind="TXT" runat="server" LabelText="生産サイクル日数" Width="90" CssClass="gears-GNumber gears-GNumber gears-GByteLengthBetween_Length_3 "/>
            <br style="clear:both">

           	<hr class="dot" width="99%" />
            
            <asp:Panel id="pnlDetailParams" runat="server">
                <ui:unitItem ID="CYC_HOSEI_NSU" CtlKind="TXT" runat="server" LabelText="サイクル長周期補正日数" CssClass="azz-parameter gears-GNumber gears-GByteLengthBetween_Length_3"/>
                <ui:unitItem ID="HBAI_HDZK_NSU" CtlKind="TXT" runat="server" LabelText="販売変動在庫日数"  CssClass="azz-parameter gears-GNumber  gears-GByteLengthBetween_Length_3"/>
                <ui:unitItem ID="KNTLDT_HS_NSU" CtlKind="TXT" runat="server" LabelText="検定リードタイム在庫日数" CssClass="azz-parameter gears-GNumber gears-GByteLengthBetween_Length_3" />
                <ui:unitItem ID="KPRISK_ZK1_NSU" CtlKind="TXT" runat="server" LabelText="欠品リスク在庫１日数" CssClass="azz-parameter gears-GNumber gears-GByteLengthBetween_Length_3" />
                <ui:unitItem ID="KPRISK_ZK2_NSU" CtlKind="TXT" runat="server" LabelText="欠品リスク在庫２日数" CssClass="azz-parameter gears-GNumber gears-GByteLengthBetween_Length_3"/>
	            <br style="clear:both">
                <ui:unitItem ID="HSRSK_ZK_NSU" CtlKind="TXT" runat="server" LabelText="品質リスク在庫日数" CssClass="azz-parameter gears-GNumber gears-GByteLengthBetween_Length_3"/>
                <ui:unitItem ID="OTHER_ZK_NSU" CtlKind="TXT" runat="server" LabelText="その他在庫補正日数" CssClass="azz-parameter gears-GNumber gears-GByteLengthBetween_Length_3"/>
                <ui:unitItem ID="AZZ_CALC" CtlKind="LBL" runat="server" LabelText="安全在庫率(＊ボタンで計算)" SearchAction="calcAZZ()" IsEditable=False Width="145" Height="25"
                     style="background-color:lavender; text-align:right;vertical-align:text-top;"/>
                <ui:unitItem ID="AZZK_CMT" CtlKind="TXT" runat="server" LabelText="安全在庫コメント" Width="330" CssClass="gears-GByteLengthBetween_Length_256" />
            </asp:Panel>
            <br style="clear:both">
        </asp:Panel>
        <br style="clear:both">
        <br style="clear:both">

        <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">帳票・データ活用</h3>
        <asp:Panel id="pnlAttrDock" runat="server">
            <ui:unitItem ID="DOK_ZOK1" CtlKind="DDL" runat="server" LabelText="独自属性１" />
            <ui:unitItem ID="DOK_ZOK2" CtlKind="DDL" runat="server" LabelText="独自属性２" />
            <ui:unitItem ID="DOK_ZOK3" CtlKind="DDL" runat="server" LabelText="独自属性３" />
            <ui:unitItem ID="DOK_ZOK4" CtlKind="DDL" runat="server" LabelText="独自属性４" />
            <ui:unitItem ID="DOK_ZOK5" CtlKind="TXT" runat="server" LabelText="独自属性５" />
        </asp:Panel>
        <br style="clear:both">

        <asp:Panel id="pnlForForm" runat="server">
            <ui:unitItem ID="JUSHI_KBN" CtlKind="DDL" runat="server" LabelText="樹脂区分" Width="150"/>
            <ui:unitItem ID="SHOKUBAI_KBN" CtlKind="DDL" runat="server" LabelText="触媒区分" Width="150"/>
            <br style="clear:both">

            <ui:unitItem ID="JUKYU_FLG" CtlKind="CHB" runat="server" LabelText="需給対象外フラグ" Width="80"/>
            <ui:unitItem ID="HIN_ZOKS_FLG" CtlKind="DDL" runat="server" LabelText="品目属性フラグ" Width="120"/>
            <ui:unitItem ID="JUKYU_HIN_TXT" CtlKind="TXT" runat="server" LabelText="営業品目テキスト" CssClass="gears-GByteLengthBetween_Length_20" />
            <ui:unitItem ID="NSGT_TNI_NUM" CtlKind="TXT" runat="server" LabelText="荷姿単位" Width=100 CssClass="gears-GNumeric gears-GNumeric gears-GPeriodPositionOk_BeforeP_5_AfterP_3" />
            <ui:unitItem ID="PALT_TNI_NUM" CtlKind="TXT" runat="server" LabelText="パレット単位" Width=100 CssClass="gears-GNumeric gears-GNumeric gears-GPeriodPositionOk_BeforeP_5_AfterP_3"  />

        </asp:Panel>
        <br style="clear:both">

        <br style="clear:both">


        <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">管理項目　<small class="lw">※ Astplanner非連携</small></h3>
        <asp:Panel id="pnlAttrArea" runat="server">
            <ui:unitItem ID="HBAN_KBN" CtlKind="DDL" runat="server" LabelText="廃番区分"  Width="120"  />
            <ui:unitItem ID="JYAKUSHO_KBN" CtlKind="DDL" runat="server" LabelText="弱小銘柄区分" Width="120" />
            <ui:unitItem ID="PWD_BSH_FLG" CtlKind="CHB" runat="server" LabelText="ﾊﾟｳﾀﾞｰ抜出銘柄" Width="110" />
            <ui:unitItem ID="MIN_SSN_SRY" CtlKind="TXT" runat="server"  LabelText="最低基準生産量" Width="100" CssClass="gears-GNumber gears-GByteLengthBetween_Length_15 "/>
	        <ui:unitItem ID="SCP_PLANOM" CtlKind="TXT" runat="server" LabelText="計画単位" Width=100 CssClass="gears-GByteLengthBetween_Length_3" />
            <br style="clear:both">

            <ui:unitItem ID="SSNKNR_CMT" CtlKind="TXT" runat="server" LabelText="生産管理コメント" Width="830"  CssClass="gears-GByteLengthBetween_Length_256" />
            <br style="clear:both">

        </asp:Panel>
        <br style="clear:both">

        <asp:Panel id="pnlComment" runat="server">
            <ui:unitItem ID="SSN_SYK_FLG" CtlKind="DDL" runat="server" LabelText="生産制約種別" />
            <ui:unitItem ID="SSN_SYK_CMT" CtlKind="TXT" runat="server" LabelText="生産制約コメント" Width="650" CssClass="gears-GByteLengthBetween_Length_256"/>
            <br style="clear:both">
            <ui:unitItem ID="HBAI_SYK_FLG" CtlKind="CHB" runat="server" LabelText="販売制約"  Width="50"/>
            <ui:unitItem ID="HBAI_SYK_CMT" CtlKind="TXT" runat="server" LabelText="販売制約コメント" Width="750" CssClass="gears-GByteLengthBetween_Length_256"/>
            <br style="clear:both">
            <ui:unitItem ID="TSUMEK_KINSI_FLG" CtlKind="CHB" runat="server" LabelText="詰替禁止"  Width="50"/>
            <ui:unitItem ID="TSUMEK_KINSI_CMT" CtlKind="TXT" runat="server" LabelText="詰替禁止制約コメント" Width="750" CssClass="gears-GByteLengthBetween_Length_256"/>
            <br style="clear:both">
            <ui:unitItem ID="CHUI_FLG" CtlKind="CHB" runat="server" LabelText="注意銘柄"  Width="50"/>
            <ui:unitItem ID="CHUI_CMT" CtlKind="TXT" runat="server" LabelText="注意銘柄コメント" Width="750" CssClass="gears-GByteLengthBetween_Length_256"/>
        </asp:Panel>
        <br style="clear:both">

    </asp:Panel>
    <br style="clear:both">
    <hr class="dot" width="99%" />

    <asp:Button id="btnSave" runat="server" Text="　保存　" />
    <br style="clear:both">
      
    <br/><br/><br/><br/>

  </asp:Panel>



</asp:Content>

<asp:Content ID="ContentFooter" ContentPlaceHolderID="pppFooter" Runat="Server" ClientIDMode=Static>
    <asp:UpdatePanel id="udpSearchObj" runat="server" UpdateMode=Conditional>
        <ContentTemplate>
            <asp:HiddenField id="hdnPARENT_HIN_CD" runat="server" />
            <search:sh ID="SEARCH_PARENT" runat="server" TgtControl=txtPARENT_HIN_CD HinKbn=POWDER />
            <search:sh ID="SEARCH_CHILD" runat="server" TgtControl=txtINFL_HIN_CD Height=300 IsNoSelect=True RestrictionControls=hdnPARENT_HIN_CD/>
        </ContentTemplate>
    </asp:UpdatePanel>
     
</asp:Content>
<asp:Content ID="EndDeclare" ContentPlaceHolderID="pppEndDeclare" Runat="Server" ClientIDMode=Static>
</asp:Content>

