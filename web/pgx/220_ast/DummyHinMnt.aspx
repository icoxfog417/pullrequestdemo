﻿<%@ Page Title="Ｓ-Ｎａｖｉ＋　ダミー品メンテナンス" Language="VB" MasterPageFile="~/220_ast/astSwitchItemMaster.master" AutoEventWireup="false" CodeFile="DummyHinMnt.aspx.vb" Inherits="_220_ast_DummyHinMnt" %>
<%@ MasterType VirtualPath="astSwitchItemMaster.master" %>
<%@ Register Src="../parts/UnitItem.ascx" TagName="unitItem" TagPrefix="ui" %>
<%@ Register src="../parts/220_ast/SCItemSearch.ascx" tagname="sh" tagprefix="search" %>
<%@ Register src="../parts/220_ast/SCItemHinSouchi.ascx" tagname="hinSouchi" tagprefix="hs" %>

<asp:Content ID="SHeader" ContentPlaceHolderID="pppSHeader" Runat="Server" ClientIDMode=Static>
    <link href="css/jquery.simple-color-picker.css" rel="stylesheet" type="text/css" />
    <script src="./lib/jquery.simple-color-picker.js" type="text/javascript" ></script>
    <script>
        function callParent() {
            if ($("#" + '<%= HIN_KBN.ControlId %>').val() != '<%= AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.POWDER) %>') {
                SEARCH_PARENT.openDialog()
            }
        }
    </script>

</asp:Content>

<asp:Content ID="SPreDeclare" ContentPlaceHolderID="pppSPreDeclare" Runat="Server" ClientIDMode=Static> 
</asp:Content>

<asp:Content ID="SPageTitle" ContentPlaceHolderID="pppSPageTitle" Runat="Server" ClientIDMode=Static>
ダミー品目メンテナンス
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Label runat="server" ID="lblMsg" Text=""></asp:Label>
</asp:Content>

<asp:Content ID="SHeaderContent" ContentPlaceHolderID="pppSHeaderContent" Runat="Server" ClientIDMode=Static>
</asp:Content>

<asp:Content ID="UpperAreaTitle" ContentPlaceHolderID="pppUpperAreaTitle" Runat="Server" ClientIDMode=Static>
マスタメンテナンス
</asp:Content>
<asp:Content ID="UpperPanel" ContentPlaceHolderID="pppUpperPanel" Runat="Server" ClientIDMode=Static>
    <asp:Panel id="pnlMeigara__GFORM" runat="server">

        <asp:Panel id="pnlCommon" runat="server">
            <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">基本</h3>
            <ui:unitItem ID="HIN_CD__KEY" CtlKind="TXT" runat="server" LabelText="品目コード" AutoPostBack=True CssClass="gears-GRequired gears-GByteLengthBetween_MinLength_0_Length_18" />
            <ui:unitItem ID="HIN_TXT" CtlKind="TXT" runat="server" LabelText="品目テキスト" Width="280" CssClass="gears-GByteLengthBetween_MinLength_0_Length_40" />
			<asp:HiddenField ID="hdnHING_CD" Value="" runat="server" />
            <ui:unitItem ID="SCHEDULER_FLG" CtlKind="CHB" runat="server" LabelText="ＳＣ対象" Width="60" />
            <ui:unitItem ID="MK_FLG" CtlKind="RBL" runat="server" LabelText="無効フラグ"  />

			<br style="clear:both">
            <ui:unitItem ID="HIN_KBN" CtlKind="DDL" runat="server" LabelText="品目区分" Width=100 AutoPostBack=True/>
            <ui:unitItem ID="EXHIN_KBN" CtlKind="DDL" runat="server" LabelText="例外品目区分" Width=100 AutoPostBack=True/>
            <ui:unitItem ID="PARENT_HIN_CD" CtlKind="TXT" runat="server" LabelText="親銘柄/重合Ｇ" SearchAction="callParent()" CssClass="gears-GByteLengthBetween_MinLength_0_Length_18" />
            <ui:unitItem ID="PIM_OUT_CD" CtlKind="TXT" runat="server" LabelText="PIM-Aid 品目コード" CssClass="gears-GByteLengthBetween_Length_18" />
            <ui:unitItem ID="SCP_OUT_CD" CtlKind="TXT" runat="server" LabelText="SAP 品目コード" CssClass="gears-GByteLengthBetween_Length_18"/>
            <br style="clear:both">

   		    <ui:unitItem ID="DISP_COLOR" CtlKind="TXT" runat="server" LabelText="色" Width="35" CssClass="cpicker gears-GByteLength_Length_7" />
		    <ui:unitItem ID="CMNT" CtlKind="TXT" runat="server" LabelText="コメント" Width="395" CssClass="gears-GByteLengthBetween_MinLength_0_Length_100" />

            <br style="clear:both">
            <ui:unitItem ID="SSNKNR_CMT" CtlKind="TXT" runat="server" LabelText="生産管理コメント" Width="460" CssClass="gears-GByteLengthBetween_MinLength_0_Length_256" />
            <br style="clear:both">

        </asp:Panel>

        <asp:Panel id="pnlSSN" runat="server">
            <ui:unitItem ID="VKORG" CtlKind="DDL" runat="server" LabelText="販売組織"  Width=100/>
            <ui:unitItem ID="DHYO_MDL_KBN" CtlKind="DDL" runat="server" LabelText="代表モデル区分" AutoPostBack=True />
            <asp:UpdatePanel id="udpSOUCHI_CD" runat="server" UpdateMode=Conditional>
                <ContentTemplate>
                    <ui:unitItem ID="DHYO_SOUCHI_CD" CtlKind="DDL" runat="server" LabelText="代表装置"  />
                </ContentTemplate>
            </asp:UpdatePanel>

            <br style="clear:both">
            <asp:Panel id="pnlHinSouchi" runat="server" >
                <h3 class="ui-state-default ui-corner-all ppp-box" style="font-size:15px">生産装置</h3>
                <hs:hinSouchi ID="ucHIN_SOUCHI" runat="server" />
            </asp:Panel>

        </asp:Panel>

    </asp:Panel>
    <br/>
    <asp:Button id="btnSave" runat="server" Text="　保存　" />


</asp:Content>

<asp:Content ID="LowerAreaTitle" ContentPlaceHolderID="pppLowerAreaTitle" Runat="Server" ClientIDMode=Static>
検索エリア
</asp:Content>
<asp:Content ID="LowerPanel" ContentPlaceHolderID="pppLowerPanel" Runat="Server" ClientIDMode=Static>

    <asp:Panel id="pnlDUMMY__GFilter" runat="server">
        <ui:unitItem ID="DHYO_MDL_KBN__SEARCH" CtlKind="DDL" runat="server" LabelText="代表モデル区分" IsNeedAll=True />
        <ui:unitItem ID="HIN_CD__SEARCH" CtlKind="TXT" runat="server" LabelText="品目コード" Operator="START_WITH" />
        <ui:unitItem ID="HIN_TXT__SEARCH" CtlKind="TXT" runat="server" LabelText="品目テキスト" Operator="LIKE"/>
        <ui:unitItem ID="HIN_KBN__SEARCH" CtlKind="DDL" runat="server" LabelText="品目区分" IsNeedAll=True />
        <ui:unitItem ID="EXHIN_KBN__SEARCH" CtlKind="DDL" runat="server" LabelText="例外品目区分" IsNeedAll=True/>
        <br style="clear:both">
        <asp:Button ID="btnSearch" runat="server" Text="　検索　" />

    </asp:Panel>
    <br/>
    <asp:Panel runat="server">
        <asp:GridView id="grvDummy" runat="server" CssClass="ppp-table" HeaderStyle-CssClass=ppp-table-head 
        EnableViewState=False DataSourceID=odsDummy AutoGenerateSelectButton=true DataKeyNames="HIN_CD"
        PageSize=20 AllowPaging=True AutoGenerateColumns=False>
            <Columns>
                <asp:BoundField DataField="HIN_KBN_TXT" HeaderText="品目区分" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="EXHIN_KBN_TXT" HeaderText="例外品目区分" ItemStyle-Width="100">
                </asp:BoundField>

                <asp:TemplateField HeaderText="品目コード">
                    <ItemTemplate>
                        <asp:Label ID="lblHinmoku" runat="server" 
                            Text=<%# DataBinder.Eval(Container.DataItem, "HIN_CD")%>
                            ToolTip=<%# DataBinder.Eval(Container.DataItem, "HIN_TXT")%>
                        />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ＳＣ対象" ItemStyle-HorizontalAlign=Center ItemStyle-Width="60">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSCHEDULER_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "SCHEDULER_FLG")="1",True,False) %>  Enabled=False/>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="DHYO_MDL_KBN_TXT" HeaderText="代表モデル" ItemStyle-Width="80">
                </asp:BoundField>
                <asp:BoundField DataField="SPART" HeaderText="製品部門" ItemStyle-Width="60">
                </asp:BoundField>
                <asp:BoundField DataField="DHYO_SOUCHI_CD" HeaderText="代表生産装置" ItemStyle-Width="100">
                </asp:BoundField>

                <asp:TemplateField HeaderText="無効フラグ" ItemStyle-Width="80" ItemStyle-HorizontalAlign=Center>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkMK_FLG" runat="server" Checked=<%# IIF(DataBinder.Eval(Container.DataItem, "MK_FLG")="1",True,False) %> Enabled=False/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
    </asp:GridView>
    <asp:ObjectDataSource id="odsDummy" runat="server" SelectMethod="gSelectPageBy" SelectCountMethod="gSelectCount" typename="AstMaster.AST_M_HIN" EnablePaging=True >
    </asp:ObjectDataSource>
    <asp:Label runat="server" ID="lblState" CssClass="ppp-msg success"></asp:Label>
    </asp:Panel>

</asp:Content>

<asp:Content ID="SFooter" ContentPlaceHolderID="pppSFooter" Runat="Server" ClientIDMode=Static>
    <asp:Label ID="lblEXHIN_KBN" Text="0" runat="server" Operator="NEQ" Style="display:none"/>
    <search:sh ID="SEARCH_PARENT" runat="server" TgtControl=txtPARENT_HIN_CD HinKbn=POWDER RestrictionControls="lblEXHIN_KBN"/>
</asp:Content>
<asp:Content ID="SEndDeclare" ContentPlaceHolderID="pppSEndDeclare" Runat="Server" ClientIDMode=Static>
</asp:Content>

