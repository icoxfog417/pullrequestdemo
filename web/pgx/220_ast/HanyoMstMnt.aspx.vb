﻿Imports Gears

''' ------------------------------------------------------------------------------
''' Project     : 220_ast
''' Class       : _220_ast_HanyoMstMnt
''' 
''' -------------------------------------------------------------------------------
''' <summary>
''' 汎用テーブル参照画面
''' </summary>
''' <remarks>
''' </remarks>
''' <hisotry>
'''  [K.Takahashi] 2012/06/05 Created
''' </hisotry>
''' -------------------------------------------------------------------------------
Partial Class _220_ast_HanyoMstMnt
    Inherits GearsPage

#Region "イベント"

#Region "ページ"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' OnInit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        Dim scHanyoDS As New AstMaster.AST_M_HANYO(GPageMediator.ConnectionName)

        registerMyControl(pnlHanyo__GFORM, scHanyoDS)
        registerMyControl(pnlHanyo__GFilter)
        registerMyControl(grvHanyo, New AstMaster.AST_M_HANYO(GPageMediator.ConnectionName))

        'イベントハンドラの登録
        AddHandler HANYO_ID.getControl(Of DropDownList).SelectedIndexChanged, AddressOf Me.ddlHANYO_ID_SelectedIndexChanged

        Dim gvUtil As New GridViewUtility(grvHanyo, lblState)
        'gvUtil.addHideColumn("KEY4")
        'gvUtil.addHideColumn("KEY5")


    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' OnLoad
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        '初期化処理
        resetEscapesWhenSend(pnlHanyo__GFORM, pnlHanyo__GFORM)

        'リレーション定義
        addRelation(pnlHanyo__GFORM, pnlHanyo__GFORM)
        addRelation(grvHanyo, pnlHanyo__GFORM)

        If Not IsPostBack Then
            refresh()
            Master.IsDisplayUpper = False
        End If

    End Sub

#End Region

#Region "検索ボタン"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' btnSearch_Click
    ''' </summary>
    ''' <remarks>
    ''' 検索ボタンがクリックされた際の処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        grvHanyo.DataBind()
        Master.IsDisplayUpper = False

    End Sub
#End Region

#Region "ドロップダウンリスト"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' OnInit
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub ddlHANYO_ID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        grvHanyo.DataBind()
        Master.IsDisplayUpper = False
    End Sub
#End Region

#Region "GridView"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' grvHanyo_PageIndexChanged
    ''' </summary>
    ''' <remarks>
    ''' ページボタンが変更された際の処理
    ''' </remarks>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub grvHanyo_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvHanyo.PageIndexChanged
        Master.IsDisplayUpper = False
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' grvHanyo_PageIndexChanged
    ''' </summary>
    ''' <remarks>
    ''' 行選択された際の処理
    ''' </remarks>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub grvHanyo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvHanyo.SelectedIndexChanged
        executeBehavior(grvHanyo)
        Master.IsDisplayUpper = True
    End Sub
#End Region

#Region "DataSource"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' odsHanyo_ObjectCreating
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender"></param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub odsHanyo_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsHanyo.ObjectCreating
        e.ObjectInstance = New AstMaster.AST_M_HANYO(GPageMediator.ConnectionName)
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' odsHanyo_Selecting
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub odsHanyo_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsHanyo.Selecting
        e.InputParameters("data") = makeSendMessage(pnlHanyo__GFilter)
    End Sub

#End Region


#End Region

#Region "その他関数"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' refresh
    ''' </summary>
    ''' <remarks>
    ''' 画面の再表示
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/06/05 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub refresh()
        executeBehavior(pnlHanyo__GFilter)
        'フィルタの値で一覧を更新する(ページング対応用)　→ DataBindを実行するとSelectingのイベントが発生する
        grvHanyo.DataBind()
    End Sub

#End Region

End Class

