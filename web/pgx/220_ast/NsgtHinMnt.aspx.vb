﻿Imports Gears




''' ------------------------------------------------------------------------------
''' Project     : 220_ast
''' Class       : _220_ast_NsgtHinMnt
''' 
''' -------------------------------------------------------------------------------
''' <summary>
''' 荷姿詳細メンテ画面
''' </summary>
''' <remarks>
''' </remarks>
''' <hisotry>
'''  [K.Takahashi] 2012/04/26 Created
''' </hisotry>
''' -------------------------------------------------------------------------------

Partial Class _220_ast_NsgtHinMnt
    Inherits GearsPage

#Region "変数"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 品目マスタDataset
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/26 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private dsHin As AST_M_HIN_NSGT = Nothing

#End Region

#Region "イベント"

#Region "ページ"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Init
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/26 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        dsHin = New AST_M_HIN_NSGT(GPageMediator.ConnectionName)

        dsHin.addLockCheckCol("UPD_YMD", LockType.UDATESTR) '楽観的ロックに使用する列とタイプを指定。
        dsHin.addLockCheckCol("UPD_HMS", LockType.UTIMESTR)
        dsHin.addLockCheckCol("UPD_USR", LockType.USER)
        'モデル検証クラスの設定
        Dim hingValidator As New AstMasterHinmokuValidator(Master.ConnectionName)
        dsHin.ModelValidator = hingValidator


        'フィルタ用のコントロールを登録　※忘れがちなので注意。Panel系のコントロールは自動的には登録されない
        registerMyControl(pnlGFilter)
        '一覧表示用のコントロール/データソースを登録
        registerMyControl(grvHin)
        '詳細表示用のコントロール/データソースを登録
        registerMyControl(pnlGFORM__HIN, dsHin)
        registerMyControl(pnlHIN_DSP, dsHin)
        registerMyControl(HIN_CD__KEY)
        registerMyControl(PARENT_HIN_CD__2)
        'イベントハンドラの登録
        AddHandler HIN_CD__KEY.getControl(Of TextBox).TextChanged, AddressOf Me.txtHIN_CD__KEY_TextChanged
        AddHandler PARENT_HIN_CD.getControl(Of TextBox).TextChanged, AddressOf Me.txtPARENT_HIN_CD_TextChanged

        '非同期更新処理の登録
        Dim srmOnPage As ScriptManager = AjaxControlToolkit.ToolkitScriptManager.GetCurrent(Me)
        srmOnPage.RegisterAsyncPostBackControl(HIN_CD__KEY.getControl(Of TextBox))
        srmOnPage.RegisterAsyncPostBackControl(PARENT_HIN_CD.getControl(Of TextBox))

        '各ユーザコントロールにConnectionNameを設定
        ucHIN_SOUCHI.ConnectionName = GPageMediator.ConnectionName
        SEARCH_PELET_1.ConnectionName = GPageMediator.ConnectionName
        SEARCH_PELET_2.ConnectionName = GPageMediator.ConnectionName
        SEARCH_NSGT_1.ConnectionName = GPageMediator.ConnectionName
        SEARCH_NSGT_2.ConnectionName = GPageMediator.ConnectionName
        '検索対象を通常品のみとする。
        SEARCH_PELET_1.RestrictionControls = hdnEXHIN_KBN.ID
        SEARCH_PELET_2.RestrictionControls = hdnEXHIN_KBN.ID

    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_Load
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/26 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        ''選択項目→一覧
        addRelation(pnlGFilter, grvHin)

        ''一覧表上での選択→詳細
        addRelation(grvHin, pnlGFORM__HIN)

        '詳細の変更実施
        addRelation(pnlGFORM__HIN, pnlGFORM__HIN)
        '詳細→一覧
        addRelation(pnlGFORM__HIN, grvHin)
        '品目コード→品目装置一覧
        addRelation(HIN_CD__KEY, pnlHIN_DSP)
        'このコントロールは更新対象からはずしたい、という場合このように宣言
        setEscapesWhenSend(pnlGFORM__HIN, pnlGFORM__HIN,
                           New String() {HIN_KBN_TXT__GCON.ControlId,
                                         VKORG_TXT__GCON.ControlId,
                                         DHYO_MDL_KBN_TXT__GCON.ControlId,
                                         DHYO_SOUCHI_CD__GCON.ControlId,
                                         EXHIN_KBN_TXT__GCON.ControlId,
                                         PARENT_HIN_TXT__GCON.ControlId,
                                         IKOHIN_FLG.ControlId, "hdnHING_CD", "hdnDHYO_MDL_KBN"})
        If Not IsPostBack Then
            '引数の設定
            If Not String.IsNullOrEmpty(getQueryString("hin_cd")) Then
                HIN_CD__KEY.setValue(getQueryString("hin_cd"))
                executeBehavior(pnlGFORM__HIN)
                PARENT_HIN_CD__2.setValue(PARENT_HIN_CD.getValue)
            Else
                Master.IsDisplayUpper = False
            End If
            refresh()
        End If

    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Page_LoadComplete
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

        If IsPostBack Then
            'ポストバック時、ログ記録を終了する。開始～終了までの間に実行された処理のログを出力する
        End If

    End Sub


#End Region

#Region "ボタン"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 表示ボタンクリック処理
    ''' </summary>
    ''' <remarks>
    ''' フィルタの値でロード
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'フィルタの値でロード
        refresh()
        Master.IsDisplayUpper = False
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 登録ボタンクリック処理
    ''' </summary>
    ''' <remarks>
    ''' 登録処理
    ''' </remarks>
    ''' <param name="sender">Object</param>
    ''' <param name="e">System.EventArgs</param>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim blnRslt As Boolean = False
        If isValidateOk() AndAlso CheckData() Then
            '更新処理
            blnRslt = executeBehavior(pnlGFORM__HIN, ActionType.SAVE)
            If blnRslt AndAlso Log.Count = 0 Then
                SetParentInfo()
                refresh()
            End If
        End If
        getLogMsgDescription(blnRslt, lblMsg)
    End Sub

#End Region

#Region "GridView"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    '''　SelectedIndexChangedイベント
    ''' </summary>
    ''' <remarks>
    ''' 行が選択された時の処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/16 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub grvHin_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grvHin.SelectedIndexChanged

        executeBehavior(CType(sender, GridView))
        SetParentInfo()
        ucHIN_SOUCHI.Update(HIN_CD__KEY.getValue, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA), hdnDHYO_MDL_KBN.Value, PARENT_HIN_CD.getValue)
        lblMsg.Text = ""
        HIN_TXT.getControl.CssClass = ""

    End Sub

#End Region

#Region "ObjectDataSource"

#Region "odsHin"
    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' ObjectCreatingイベント
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub odsHin_ObjectCreating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceEventArgs) Handles odsHin.ObjectCreating
        e.ObjectInstance = dsHin
    End Sub


    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' Selectingイベント
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub odsHin_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsHin.Selecting
        Dim sendObj As GearsDTO = makeSendMessage(pnlGFilter)
        e.InputParameters("data") = sendObj
    End Sub

#End Region

#End Region

#Region "HIN_CD__KEY"
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' txtHIN_CD__KEY_TextChanged
    ''' </summary>
    ''' <remarks>
    ''' 品目コードが変更された際の処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/26 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub txtHIN_CD__KEY_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ExistsHinmoku(HIN_CD__KEY, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA)) Then
            executeBehavior(HIN_CD__KEY)
            SetParentInfo()
            ucHIN_SOUCHI.Update(HIN_CD__KEY.getValue, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA), hdnDHYO_MDL_KBN.Value, PARENT_HIN_CD.getValue)
            HIN_TXT.getControl.CssClass = ""
        Else
            Dim desc As KeyValuePair(Of String, String) = getLogMsgDescription(False)
            HIN_TXT.getControl.CssClass = desc.Key
            HIN_TXT.getControl(Of TextBox).Text = desc.Value
        End If
        udpHin.Update()
    End Sub

#End Region

#Region "PARENT_HIN_CD"
    ''' ------------------------------------------------------------------------------
    ''' <summary>
    ''' txtPARENT_HIN_CD_TextChanged
    ''' </summary>
    ''' <remarks>
    ''' 親銘柄コードが変更された際の処理
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/21 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Protected Sub txtPARENT_HIN_CD_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SetParentInfo()
        ucHIN_SOUCHI.Update(HIN_CD__KEY.getValue, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA), hdnDHYO_MDL_KBN.Value, PARENT_HIN_CD.getValue)
        udpHin.Update()
    End Sub

#End Region

#End Region

#Region "その他関数"

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' 再表示
    ''' </summary>
    ''' <remarks>
    ''' フィルタの値でロード
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub refresh()
        executeBehavior(pnlGFilter)
        'フィルタの値で一覧を更新する(ページング対応用)　→ DataBindを実行するとSelectingのイベントが発生する
        grvHin.DataBind()
        '品目装置メンテグリッドの表示
        ucHIN_SOUCHI.Update(HIN_CD__KEY.getValue, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA), hdnDHYO_MDL_KBN.Value, PARENT_HIN_CD.getValue)
    End Sub

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' CheckData
    ''' </summary>
    ''' <remarks>
    ''' 項目以外のチェック
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function CheckData() As Boolean

        '品目コードの存在チェックと生産装置のチェック
        If ExistsHinmoku(HIN_CD__KEY, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA)) AndAlso
            (String.IsNullOrEmpty(PARENT_HIN_CD.getValue) OrElse ExistsHinmoku(PARENT_HIN_CD, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.PELET))) Then
            Return True
        Else
            Return False
        End If

    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' ExistsHinmoku
    ''' </summary>
    ''' <remarks>
    ''' 品目マスタの存在チェック
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/04/20 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Function ExistsHinmoku(ByVal control As UnitItem, ByVal strHinKbn As String) As Boolean
        Dim ds As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
        Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)
        dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(control.getValue()))
        dto.addFilter(SqlBuilder.newFilter("HIN_KBN").eq(strHinKbn))
        dto.addFilter(SqlBuilder.newFilter("EXHIN_KBN").eq(hdnEXHIN_KBN.Value))
        If ds.gSelectCount(dto) = 0 Then
            Log.Add(HIN_CD__KEY.ID, New GearsDataValidationException("品目コード=" + control.getValue() + "は存在しません。"))
            Return False
        End If
        Return True
    End Function

    ''' -------------------------------------------------------------------------------
    ''' <summary>
    ''' SetParentInfo
    ''' </summary>
    ''' <remarks>
    ''' 親品目の情報を取得して画面項目に設定する。
    ''' </remarks>
    ''' <hisotry>
    '''  [K.Takahashi] 2012/05/21 Created
    ''' </hisotry>
    ''' -------------------------------------------------------------------------------
    Private Sub SetParentInfo()
        If Not String.IsNullOrEmpty(PARENT_HIN_CD.getValue) Then
            If ExistsHinmoku(PARENT_HIN_CD, AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.PELET)) Then
                Dim dsParent As New AstMaster.AST_M_HIN(GPageMediator.ConnectionName)
                Dim dto As GearsDTO = New GearsDTO(ActionType.SEL)
                dto.addFilter(SqlBuilder.newFilter("HIN_CD").eq(PARENT_HIN_CD.getValue))
                dto.addFilter(SqlBuilder.newFilter("MK_FLG").eq("0"))

                '親銘柄の情報を取得
                Dim dt As System.Data.DataTable = dsParent.gSelect(dto)
                If dt.Rows.Count > 0 Then
                    If Not IsDBNull(dt.Rows(0).Item("HIN_TXT")) Then
                        PARENT_HIN_TXT__GCON.setValue(dt.Rows(0).Item("HIN_TXT"))
                    End If
                    If Not IsDBNull(dt.Rows(0).Item("VKORG_TXT")) Then
                        VKORG_TXT__GCON.setValue(dt.Rows(0).Item("VKORG_TXT"))
                    End If
                    If Not IsDBNull(dt.Rows(0).Item("DHYO_MDL_KBN_TXT")) Then
                        DHYO_MDL_KBN_TXT__GCON.setValue(dt.Rows(0).Item("DHYO_MDL_KBN_TXT"))
                    End If
                    If Not IsDBNull(dt.Rows(0).Item("DHYO_SOUCHI_CD")) Then
                        DHYO_SOUCHI_CD__GCON.setValue(dt.Rows(0).Item("DHYO_SOUCHI_CD"))
                    End If
                    If Not IsDBNull(dt.Rows(0).Item("EXHIN_KBN_TXT")) Then
                        EXHIN_KBN_TXT__GCON.setValue(dt.Rows(0).Item("EXHIN_KBN_TXT"))
                    End If
                    If Not IsDBNull(dt.Rows(0).Item("IKOHIN_FLG")) Then
                        IKOHIN_FLG.setValue(dt.Rows(0).Item("IKOHIN_FLG"))
                    End If
                End If
                PARENT_HIN_TXT__GCON.getControl.CssClass = ""
            Else
                getLogMsgDescription(False, PARENT_HIN_TXT__GCON.getControl)
            End If
        Else
            PARENT_HIN_TXT__GCON.setValue(String.Empty)
            VKORG_TXT__GCON.setValue(String.Empty)
            DHYO_MDL_KBN_TXT__GCON.setValue(String.Empty)
            DHYO_SOUCHI_CD__GCON.setValue(String.Empty)
            EXHIN_KBN_TXT__GCON.setValue(String.Empty)
            IKOHIN_FLG.setValue(String.Empty)
        End If
    End Sub

#End Region

End Class

''' ------------------------------------------------------------------------------
''' Project     : 220_ast
''' Class       : AST_M_HIN_NSGT
''' 
''' -------------------------------------------------------------------------------
''' <summary>
''' 品目マスタ（荷姿品）
''' </summary>
''' <remarks>
''' </remarks>
''' <hisotry>
'''  [K.Takahashi] 2012/04/26 Created
''' </hisotry>
''' -------------------------------------------------------------------------------
Public Class AST_M_HIN_NSGT
    Inherits AstMaster.AST_M_HIN

    Public Sub New(ByVal conStr As String)
        MyBase.New(conStr)
    End Sub

    Public Overrides Function makeSqlBuilder(ByRef data As Gears.GearsDTO) As SqlBuilder
        Dim sqlb As SqlBuilder = MyBase.makeSqlBuilder(data)

        sqlb.addFilter(SqlBuilder.newFilter("HIN_KBN").eq(AstMaster.HIN_KBN.GetHinKbn(AstMaster.HIN_KBN_LIST.NISUGATA))) '荷姿品
        sqlb.addSelection(SqlBuilder.newSelect("HIN_CD").ASC.isNoSelect)

        Return sqlb

    End Function

    Public Overrides Sub gSave(ByRef sqlb As Gears.SqlBuilder)

        If sqlb.getSelection("NSGT_TNI_NUM") IsNot Nothing AndAlso String.IsNullOrWhiteSpace(sqlb.getSelection("NSGT_TNI_NUM").Value) Then
            sqlb.getSelection("NSGT_TNI_NUM").setValue(Nothing)
        End If
        If sqlb.getSelection("PALT_TNI_NUM") IsNot Nothing AndAlso String.IsNullOrWhiteSpace(sqlb.getSelection("PALT_TNI_NUM").Value) Then
            sqlb.getSelection("PALT_TNI_NUM").setValue(Nothing)
        End If
        MyBase.gSave(sqlb)

    End Sub

End Class
